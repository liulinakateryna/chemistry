﻿using Chemistry.Models.Models;
using Chemistry.ViewModels.ViewModels;
using System;
using System.Diagnostics;
using System.Linq;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.UserControls
{
    public sealed partial class SelectedAtomsControl : UserControl
    {
        public SelectedAtomsControl()
        {
            this.InitializeComponent();
        }

        private void ElementDragOver(object sender, DragEventArgs e)
            => e.AcceptedOperation = DataPackageOperation.Copy;

        private async void DropChemicalElement(object sender, DragEventArgs e)
        {
            if (e.DataView?.Properties?.Any(x => x.Key == "atom") == true)
            {
                try
                {
                    var deferral = e.GetDeferral();

                    var atom = e.DataView.Properties.FirstOrDefault(x => x.Key == "atom").Value as Atom;
                    if (atom != null)
                    {
                        var resources = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;
                        resources?.ChemicalsViewModel.SelectAtomCommand.Execute(atom);
                    }

                    deferral.Complete();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
            else
            {
                e.AcceptedOperation = Windows.ApplicationModel.DataTransfer.DataPackageOperation.None;
            }
        }
    }
}