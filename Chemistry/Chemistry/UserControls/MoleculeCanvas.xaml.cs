﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using Chemistry.ViewModels.ViewModels;
using System.Collections.ObjectModel;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Chemistry.UserControls
{
    public sealed partial class MoleculeCanvas : UserControl
    {
        public static readonly DependencyProperty MoleculeProperty = DependencyProperty.Register(
            nameof(Molecule),
            typeof(ObservableCollection<GeometryUnit>),
            typeof(MoleculeCanvas),
            new PropertyMetadata(null, new PropertyChangedCallback(OnMoleculeChanged))
        );

        public ObservableCollection<GeometryUnit> Molecule
        {
            get => (ObservableCollection<GeometryUnit>)GetValue(MoleculeCanvas.MoleculeProperty);
            set => SetValue(MoleculeCanvas.MoleculeProperty, value);
        }

        private static void OnMoleculeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }

        public MoleculeCanvas()
        {
            this.InitializeComponent();
            var viewModelLocator = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;

            if (viewModelLocator != null)
            {
                viewModelLocator.MoleculeGeometryViewModel.OnClearing += this.OnClearing;
                viewModelLocator.MoleculeGeometryViewModel.OnDrawing += this.OnDrawing;
            }
        }

        private void OnDrawing()
        {
            this.Canvas.Children.Clear();

            foreach (GeometryUnit geometry in this.Molecule)
            {
                switch (geometry.GeometryType)
                {
                    case GeometryType.Atom:
                        this.DrawAtom(geometry);
                        break;

                    case GeometryType.Link:
                        this.DrawLink(geometry);
                        break;
                }
            }
        }

        private void OnClearing()
        {
            this.Canvas.Children.Clear();
        }

        private void DrawAtom(GeometryUnit geometry)
        {
            var textBlock = new TextBlock()
            {
                Text = geometry.Atom.Abbreviation,
                FontFamily = new FontFamily("Consolas"),
                FontSize = 14,
                Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            var border = new Border()
            {
                Background = new SolidColorBrush(Color.FromArgb(255, 200, 0, 0)),
                Width = 32,
                Height = 32,
                CornerRadius = new CornerRadius(16),
                Child = textBlock,
            };

            Canvas.SetTop(border, geometry.From.Y);
            Canvas.SetLeft(border, geometry.From.X);

            this.Canvas.Children.Add(border);
        }

        private void DrawLink(GeometryUnit geometry)
        {
            for (int i = 0; i < geometry.LinkMultiplicity; i++)
            {
                int delta = ((i + 1) / 2) * 4;

                if (i % 2 == 1)
                    delta *= -1;

                var link = new Line()
                {
                    X1 = geometry.From.X,
                    Y1 = geometry.From.Y + delta,
                    X2 = geometry.To.X,
                    Y2 = geometry.To.Y + delta,
                    StrokeThickness = 1,
                    Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0))
                };

                this.Canvas.Children.Add(link);
            }
        }
    }
}