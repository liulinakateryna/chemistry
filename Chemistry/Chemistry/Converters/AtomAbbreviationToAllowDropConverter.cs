﻿using Chemistry.Core.Providers;
using Chemistry.Models.Models;
using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Chemistry.Converters
{
    public sealed class AtomAbbreviationToAllowDropConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var atomAbbreviation = value as string;
            Atom selectedAtom = AtomsProvider.Atoms.FirstOrDefault(atom => atom.Abbreviation == atomAbbreviation);
            
            if (selectedAtom == null)
                return false;

            bool isVisible = new ValancesProvider().GetValances(selectedAtom).Any();

            if (parameter == "not")
                isVisible = !isVisible;

            return isVisible ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}