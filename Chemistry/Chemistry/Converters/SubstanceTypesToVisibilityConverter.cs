﻿using Chemistry.Models.Enum;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Chemistry.Converters
{
    public sealed class SubstanceTypesToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var substanceTypes = value as ObservableCollection<SubstanceType>;

            if (parameter is string enumString)
            {
                if (Enum.TryParse(enumString, out SubstanceType substanceType))
                    return (substanceTypes.Contains(substanceType)) 
                        ? Visibility.Visible : Visibility.Collapsed;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}