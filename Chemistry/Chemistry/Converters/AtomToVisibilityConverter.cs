﻿using Chemistry.Models.Models;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Chemistry.Converters
{
    public sealed class AtomToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var atom = value as MoleculeComponent;

            bool isVisible = atom != null;

            if (parameter == "not")
                isVisible = !isVisible;

            return isVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}