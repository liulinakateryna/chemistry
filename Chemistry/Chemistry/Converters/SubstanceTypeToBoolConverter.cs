﻿using Chemistry.Models.Enum;
using System;
using Windows.UI.Xaml.Data;

namespace Chemistry.Converters
{
    public sealed class SubstanceTypeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var selectedSubstanceType = (SubstanceType)value;

            if (parameter is string enumString)
            {
                if (Enum.TryParse(enumString, out SubstanceType substanceType))
                    return selectedSubstanceType == substanceType;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}