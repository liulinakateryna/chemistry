﻿using System;
using Windows.UI.Xaml.Data;

namespace Chemistry.Converters
{
    public sealed class StringToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
           => (value as string) == (parameter as string);

        public object ConvertBack(object value, Type targetType, object parameter, string language)
            => throw new NotSupportedException();
    }
}