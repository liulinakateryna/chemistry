﻿using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Popups;
using Chemistry.UI.Core.PopUp;
using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Chemistry
{
    sealed partial class App : Application
    {
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        private void Initialize()
        {
            CultureProvider.ApplyCulture();
        }

        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            this.InitializeWindowsApplication(e);
            Frame rootFrame = Window.Current.Content as Frame;
            this.Initialize();

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }

            PopupContentProvider.Instance.ShowPopupContent(PopupNavigationSource.Menu);
        }

        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        private void InitializeWindowsApplication(IActivatedEventArgs args)
        {
            PopupContentProvider.Instance.AddMapping(PopupNavigationSource.AtomPopup, typeof(AtomPopup));
            PopupContentProvider.Instance.AddMapping(PopupNavigationSource.Chemicals, typeof(ChemicalsPopup));
            PopupContentProvider.Instance.AddMapping(PopupNavigationSource.CloseAnyPopup, null);
            PopupContentProvider.Instance.AddMapping(PopupNavigationSource.Menu, typeof(MenuPopup));
            PopupContentProvider.Instance.AddMapping(PopupNavigationSource.PeriodicTable, typeof(PeriodicTablePopup));
            PopupContentProvider.Instance.AddMapping(PopupNavigationSource.Settings, typeof(SettingsPopup));
        }
    }
}
