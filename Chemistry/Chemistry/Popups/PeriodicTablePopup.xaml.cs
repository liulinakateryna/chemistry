﻿using Chemistry.Models.Models;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Popups
{
    public sealed partial class PeriodicTablePopup : UserControl
    {
        public Atom atom = new Atom()
        {
            Abbreviation = "Sr",
            AtomicNumber = 112,
            IsFake = true,
            ElementType = Models.Enum.QuantumSubshellFromKind.S
        };

        public Atom atomP = new Atom()
        {
            Abbreviation = "Sr",
            AtomicNumber = 112,
            ElementType = Models.Enum.QuantumSubshellFromKind.P
        };

        public Atom atomD = new Atom()
        {
            Abbreviation = "O",
            AtomicNumber = 112,
            ElementType = Models.Enum.QuantumSubshellFromKind.D
        };

        public Atom atomF = new Atom()
        {
            Abbreviation = "S",
            AtomicNumber = 112,
            ElementType = Models.Enum.QuantumSubshellFromKind.F
        };

        public PeriodicTablePopup()
        {
            this.InitializeComponent();
        }
    }
}