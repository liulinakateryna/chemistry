﻿using Chemistry.Models.Models;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Popups
{
    public sealed partial class ChemicalsPopup : UserControl
    {
        public ChemicalsPopup()
        {
            this.InitializeComponent();
            this.Loaded += this.OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.ChangeNavigationViewToggleButtonStyle();
        }

        private void DragChemicalElement(UIElement sender, DragStartingEventArgs args)
        {
            args.Data.Properties.Add("atom", (sender as Control).DataContext as Atom);
        }

        private void ResizeMenu(object sender, RoutedEventArgs e)
        {
            this.ResizeMenu();
        }

        private void ResizeMenu()
        {
            this.RootSplitView.IsPaneOpen = !this.RootSplitView.IsPaneOpen;
            this.ChangeNavigationViewToggleButtonStyle();
        }

        private void ChangeNavigationViewToggleButtonStyle()
        {
            if (this.RootSplitView.IsPaneOpen)
                this.PaneSwitchButtonInner.Visibility = Visibility.Collapsed;
            else
                this.PaneSwitchButtonInner.Visibility = Visibility.Visible;
        }
    }
}
