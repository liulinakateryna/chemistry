﻿using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Popups
{
    class DismissablePopup : UserControl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty DismissCommandProperty =
            DependencyProperty.Register("DismissCommand",
            typeof(ICommand),
            typeof(DismissablePopup),
            new PropertyMetadata(null));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty DismissCommandParameterProperty = DependencyProperty.Register(
            "DismissCommandParameter",
            typeof(object),
            typeof(DismissablePopup),
            new PropertyMetadata(null));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty IsDismissEnabledProperty = DependencyProperty.Register(
            "IsDismissEnabled",
            typeof(bool),
            typeof(DismissablePopup),
            new PropertyMetadata(true));

        public bool IsDismissEnabled
        {
            get { return (bool)GetValue(IsDismissEnabledProperty); }
            set { SetValue(IsDismissEnabledProperty, value); }
        }

        public ICommand DismissCommand
        {
            get
            {
                return (ICommand)this.GetValue(DismissCommandProperty);
            }
            set
            {
                this.SetValue(DismissCommandProperty, value);
            }
        }

        public object DismissCommandParameter
        {
            get
            {
                return this.GetValue(DismissCommandParameterProperty);
            }
            set
            {
                this.SetValue(DismissCommandParameterProperty, value);
            }
        }
    }
}