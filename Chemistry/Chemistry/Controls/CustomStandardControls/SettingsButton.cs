﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.CustomStandardControls
{
    [TemplatePart(Type = typeof(SettingsButton)),
        TemplateVisualState(GroupName = SelectionVisualGroup, Name = Selected),
        TemplateVisualState(GroupName = SelectionVisualGroup, Name = Unselected)]
    public sealed class SettingsButton : Button
    {
        private const string SelectionVisualGroup = "SelectionVisualGroup";
        private const string Selected = "Selected";
        private const string Unselected = "Unselected";

        public SettingsButton()
        {
            this.DefaultStyleKey = typeof(SettingsButton);
        }

        public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
           nameof(IsSelected),
           typeof(bool),
           typeof(SettingsButton),
           new PropertyMetadata(false, OnSelectionChanged));

        public bool IsSelected
        {
            get => (bool)GetValue(SettingsButton.IsSelectedProperty);
            set => SetValue(SettingsButton.IsSelectedProperty, value);
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        private static void OnSelectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SettingsButton control = d as SettingsButton;

            if (control != null)
                control.UpdateVisualStates();
        }

        private void UpdateVisualStates()
        {
            if (this.IsSelected)
                VisualStateManager.GoToState(this, SettingsButton.Selected, false);
            else
                VisualStateManager.GoToState(this, SettingsButton.Unselected, false);
        }
    }
}