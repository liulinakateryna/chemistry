﻿using Chemistry.Models.Models;
using Chemistry.Resources;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.ChemicalElementControl
{
    [TemplatePart(Type = typeof(ChemicalElementControl)), 
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = S), 
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = P), 
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = D), 
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = F), 
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = Fake),
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = Link)]
    public sealed class ChemicalElementControl : Control
    {
        private const string ChemicalElementTypeStateGroup = "ChemicalElementTypeStateGroup";
        private const string S = "S";
        private const string P = "P";
        private const string D = "D";
        private const string F = "F";
        private const string Fake = "Fake";
        private const string Link = "Link";

        public ChemicalElementControl()
        {
            this.DefaultStyleKey = typeof(ChemicalElementControl);
        }

        public static readonly DependencyProperty AbbreviationProperty = DependencyProperty.Register(
            nameof(Abbreviation),
            typeof(string),
            typeof(ChemicalElementControl),
            null);

        public static readonly DependencyProperty ArProperty = DependencyProperty.Register(
           nameof(Ar),
           typeof(string),
           typeof(ChemicalElementControl),
           null);

        public static readonly DependencyProperty AtomicNumberProperty = DependencyProperty.Register(
           nameof(AtomicNumber),
           typeof(string),
           typeof(ChemicalElementControl),
           null);

        public static readonly DependencyProperty AtomProperty = DependencyProperty.Register(
           nameof(Atom),
           typeof(Atom),
           typeof(ChemicalElementControl),
           null);

        public static readonly DependencyProperty FullNameProperty = DependencyProperty.Register(
           nameof(FullName),
           typeof(string),
           typeof(ChemicalElementControl),
           null);

        public static readonly DependencyProperty LinkTextProperty = DependencyProperty.Register(
           nameof(LinkText),
           typeof(string),
           typeof(ChemicalElementControl),
           null);

        public string Abbreviation
        {
            get => (string)GetValue(ChemicalElementControl.AbbreviationProperty);
            set => SetValue(ChemicalElementControl.AbbreviationProperty, value);
        }

        public string AtomicNumber
        {
            get => (string)GetValue(ChemicalElementControl.AtomicNumberProperty);
            set => SetValue(ChemicalElementControl.AtomicNumberProperty, value);
        }

        public Atom Atom
        {
            get => (Atom)GetValue(ChemicalElementControl.AtomProperty);
            set 
            {
                if (value != null)
                {
                    SetValue(ChemicalElementControl.AtomProperty, value);

                    this.UpdateAtomProperties();

                    this.UpdateVisualStates();
                }
            }
        }

        public string Ar
        {
            get => (string)GetValue(ChemicalElementControl.ArProperty);
            set => SetValue(ChemicalElementControl.ArProperty, value);
        }

        public string FullName
        {
            get => (string)GetValue(ChemicalElementControl.FullNameProperty);
            set => SetValue(ChemicalElementControl.FullNameProperty, value);
        }

        public string LinkText
        {
            get => (string)GetValue(ChemicalElementControl.LinkTextProperty);
            set => SetValue(ChemicalElementControl.LinkTextProperty, value);
        }

        protected override void OnApplyTemplate()
        {
            this.UpdateAtomProperties();
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        private void UpdateAtomProperties()
        {
            Atom atom = this.Atom;

            if (atom != null)
            {
                SetValue(ChemicalElementControl.AbbreviationProperty, atom.Abbreviation);
                SetValue(ChemicalElementControl.ArProperty, String.Format("{0:0.00}", atom.Ar));
                SetValue(ChemicalElementControl.AtomicNumberProperty, atom.AtomicNumber.ToString());

                if (!string.IsNullOrEmpty(atom.Abbreviation))
                    SetValue(ChemicalElementControl.FullNameProperty, PageResources.GetText(atom.Abbreviation));

                if (atom.IsLink)
                {
                    SetValue(ChemicalElementControl.LinkTextProperty, atom.LinkText);
                    SetValue(ChemicalElementControl.FullNameProperty, atom.LinkText);
                }
            }
        }

        private void UpdateVisualStates()
        {
            string state = this.Atom?.ElementType.ToString();

            if (this.Atom?.IsFake == true)
                VisualStateManager.GoToState(this, ChemicalElementControl.Fake, false);
            else if (this.Atom?.IsLink == true)
                VisualStateManager.GoToState(this, ChemicalElementControl.Link, false);
            else if (!string.IsNullOrEmpty(state))
                VisualStateManager.GoToState(this, state, false);
        }
    }
}