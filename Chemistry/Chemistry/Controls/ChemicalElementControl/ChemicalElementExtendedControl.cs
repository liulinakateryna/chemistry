﻿using Chemistry.Core.Providers;
using Chemistry.Models.Models;
using Chemistry.Resources;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.ChemicalElementControl
{
    [TemplatePart(Type = typeof(ChemicalElementExtendedControl)),
        TemplateVisualState(GroupName = ChemicalElementStateGroup, Name = Metal),
        TemplateVisualState(GroupName = ChemicalElementStateGroup, Name = NonMetal),
        TemplateVisualState(GroupName = ChemicalElementStateGroup, Name = Inert)]
    public sealed class ChemicalElementExtendedControl : Control
    {
        private const string ChemicalElementStateGroup = "ChemicalElementStateGroup";
        private const string Metal = "Metal";
        private const string NonMetal = "NonMetal";
        private const string Inert = "Inert";

        public ChemicalElementExtendedControl()
        {
            this.DefaultStyleKey = typeof(ChemicalElementExtendedControl);
        }

        public static readonly DependencyProperty AbbreviationProperty = DependencyProperty.Register(
            nameof(Abbreviation),
            typeof(string),
            typeof(ChemicalElementExtendedControl),
            null);

        public static readonly DependencyProperty AtomProperty = DependencyProperty.Register(
           nameof(Atom),
           typeof(Atom),
           typeof(ChemicalElementExtendedControl),
           null);

        public static readonly DependencyProperty FullNameProperty = DependencyProperty.Register(
           nameof(FullName),
           typeof(string),
           typeof(ChemicalElementExtendedControl),
           null);

        public string Abbreviation
        {
            get => (string)GetValue(ChemicalElementExtendedControl.AbbreviationProperty);
            set => SetValue(ChemicalElementExtendedControl.AbbreviationProperty, value);
        }

        public Atom Atom
        {
            get => (Atom)GetValue(ChemicalElementExtendedControl.AtomProperty);
            set
            {
                if (value != null)
                {
                    SetValue(ChemicalElementExtendedControl.AtomProperty, value);

                    this.UpdateAtomProperties();

                    this.UpdateVisualStates();
                }
            }
        }

        public string FullName
        {
            get => (string)GetValue(ChemicalElementExtendedControl.FullNameProperty);
            set => SetValue(ChemicalElementExtendedControl.FullNameProperty, value);
        }

        protected override void OnApplyTemplate()
        {
            this.UpdateAtomProperties();
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        private void UpdateAtomProperties()
        {
            Atom atom = this.Atom;

            if (atom != null)
            {
                SetValue(ChemicalElementExtendedControl.AbbreviationProperty, atom.Abbreviation);

                if (!string.IsNullOrEmpty(atom.Abbreviation))
                    SetValue(ChemicalElementExtendedControl.FullNameProperty, PageResources.GetText(atom.Abbreviation));
            }
        }

        private void UpdateVisualStates()
        {
            if (this.Atom == null)
                return;

            var atomProvider = new AtomBehaviourProvider();

            if (atomProvider.IsMetal(this.Atom))
                VisualStateManager.GoToState(this, ChemicalElementExtendedControl.Metal, false);
            else if (atomProvider.IsNonMetal(this.Atom))
                VisualStateManager.GoToState(this, ChemicalElementExtendedControl.NonMetal, false);
            else
                VisualStateManager.GoToState(this, ChemicalElementExtendedControl.Inert, false);
        }
    }
}