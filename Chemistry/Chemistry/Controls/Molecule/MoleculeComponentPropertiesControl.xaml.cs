﻿using Chemistry.Models.Enum;
using Chemistry.ViewModels.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.Molecule
{
    public sealed partial class MoleculeComponentPropertiesControl : UserControl
    {
        public MoleculeComponentPropertiesControl()
        {
            this.InitializeComponent();

            var resources = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;
            if (resources != null)
            { 
                resources.AtomPropertiesViewModel.SelectionChanged += this.SelectionChanged;
                this.SelectionChanged(resources.AtomPropertiesViewModel.SelectedSubstanceType);
            }
        }

        private void SelectionChanged(SubstanceType substanceType)
        {
            switch (substanceType)
            {
                case SubstanceType.Acid:
                    this.Acid.IsChecked = true;
                    return;

                case SubstanceType.Alkali:
                    this.Alkali.IsChecked = true;
                    return;

                case SubstanceType.Oxide:
                    this.Oxide.IsChecked = true;
                    return;

                case SubstanceType.Salt:
                    this.Salt.IsChecked = true;
                    return;

                case SubstanceType.Simple:
                    this.Simple.IsChecked = true;
                    return;
            }
        }

        private void ChangeSelection(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;
            var param = radioButton?.CommandParameter;

            if (param != null)
            {
                var resources = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;

                var selectedSubstanceType = (SubstanceType)param;
                resources.AtomPropertiesViewModel.SelectedSubstanceType = selectedSubstanceType;
            }
        }
    }
}