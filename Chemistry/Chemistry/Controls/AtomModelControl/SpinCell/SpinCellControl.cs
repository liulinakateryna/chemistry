﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl.SpinCell
{
    [TemplatePart(Type = typeof(SpinCellControl)),
        TemplateVisualState(GroupName = NumberOfElectronsVisualGroup, Name = One),
        TemplateVisualState(GroupName = NumberOfElectronsVisualGroup, Name = Two),
        TemplateVisualState(GroupName = NumberOfElectronsVisualGroup, Name = Zero)]
    public sealed class SpinCellControl : Control
    {
        private const string NumberOfElectronsVisualGroup = "NumberOfElectrons";
        private const string One = "One";
        private const string Two = "Two";
        private const string Zero = "Zero";

        public static readonly DependencyProperty NumberOfElectronsProperty = DependencyProperty.Register(
            nameof(NumberOfElectrons),
            typeof(int),
            typeof(SpinCellControl),
            null);

        public int NumberOfElectrons
        {
            get => (int)GetValue(SpinCellControl.NumberOfElectronsProperty);
            set
            {
                SetValue(SpinCellControl.NumberOfElectronsProperty, value);
                this.UpdateVisualStates();
            }
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.UpdateVisualStates();
        }

        public SpinCellControl()
        {
            this.DefaultStyleKey = typeof(SpinCellControl);
        }

        private void UpdateVisualStates()
        {
            switch (this.NumberOfElectrons)
            {
                case 0:
                    VisualStateManager.GoToState(this, SpinCellControl.Zero, false);
                    break;

                case 1:
                    VisualStateManager.GoToState(this, SpinCellControl.One, false);
                    return;

                case 2:
                    VisualStateManager.GoToState(this, SpinCellControl.Two, false);
                    return;
            }
        }
    }
}