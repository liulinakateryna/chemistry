﻿using Chemistry.Models.Enum;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl.SpinCell
{
    [TemplatePart(Type = typeof(QuantumSubshellCellControl)),
        TemplateVisualState(GroupName = QuantumSubshellKindStateGroup, Name = S),
        TemplateVisualState(GroupName = QuantumSubshellKindStateGroup, Name = P),
        TemplateVisualState(GroupName = QuantumSubshellKindStateGroup, Name = D),
        TemplateVisualState(GroupName = QuantumSubshellKindStateGroup, Name = F)]
    public sealed class QuantumSubshellCellControl : Control
    {
        private const string QuantumSubshellKindStateGroup = "QuantumSubshellKindStateGroup";
        private const string S = "S";
        private const string P = "P";
        private const string D = "D";
        private const string F = "F";

        private void DistributeElectrons()
        {
            int[] electronsDistribution = new int[7];

            int numberOfElectrons = this.NumberOfElectrons;
            int numberOfCells = 1 + this.QuantumSubshellKind * 2;

            for (int round = 0; round < 2; round++)
            {
                if (numberOfElectrons <= 0)
                    break;

                for (int i = 0; i < numberOfCells; i++)
                {
                    if (numberOfElectrons <= 0)
                        break;

                    electronsDistribution[i]++;
                    numberOfElectrons--;
                }
            }

            this.ElectronsCell1 = electronsDistribution[0];
            this.ElectronsCell2 = electronsDistribution[1];
            this.ElectronsCell3 = electronsDistribution[2];
            this.ElectronsCell4 = electronsDistribution[3];
            this.ElectronsCell5 = electronsDistribution[4];
            this.ElectronsCell6 = electronsDistribution[5];
            this.ElectronsCell7 = electronsDistribution[6];
        }

        public static readonly DependencyProperty ElectronsCell1Property = DependencyProperty.Register(
            nameof(ElectronsCell1),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty ElectronsCell2Property = DependencyProperty.Register(
            nameof(ElectronsCell2),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty ElectronsCell3Property = DependencyProperty.Register(
            nameof(ElectronsCell3),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty ElectronsCell4Property = DependencyProperty.Register(
            nameof(ElectronsCell4),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty ElectronsCell5Property = DependencyProperty.Register(
            nameof(ElectronsCell5),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty ElectronsCell6Property = DependencyProperty.Register(
            nameof(ElectronsCell6),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty ElectronsCell7Property = DependencyProperty.Register(
            nameof(ElectronsCell7),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty NumberOfElectronsProperty = DependencyProperty.Register(
            nameof(NumberOfElectrons),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public static readonly DependencyProperty QuantumSubshellKindProperty = DependencyProperty.Register(
            nameof(QuantumSubshellKind),
            typeof(int),
            typeof(QuantumSubshellCellControl),
            null);

        public int ElectronsCell1
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell1Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell1Property, value);
        }

        public int ElectronsCell2
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell2Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell2Property, value);
        }

        public int ElectronsCell3
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell3Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell3Property, value);
        }

        public int ElectronsCell4
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell4Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell4Property, value);
        }

        public int ElectronsCell5
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell5Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell5Property, value);
        }

        public int ElectronsCell6
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell6Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell6Property, value);
        }

        public int ElectronsCell7
        {
            get => (int)GetValue(QuantumSubshellCellControl.ElectronsCell7Property);
            set => SetValue(QuantumSubshellCellControl.ElectronsCell7Property, value);
        }

        public int NumberOfElectrons
        {
            get => (int)GetValue(QuantumSubshellCellControl.NumberOfElectronsProperty);
            set
            {
                SetValue(QuantumSubshellCellControl.NumberOfElectronsProperty, value);
                this.DistributeElectrons();
            }
        }

        protected override void OnApplyTemplate()
        {
            this.DistributeElectrons();
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        public QuantumSubshellCellControl()
        {
            this.DefaultStyleKey = typeof(QuantumSubshellCellControl);
        }

        public int QuantumSubshellKind
        {
            get => (int)GetValue(QuantumSubshellCellControl.QuantumSubshellKindProperty);
            set
            {
                SetValue(QuantumSubshellCellControl.QuantumSubshellKindProperty, value);
                this.UpdateVisualStates();
            }
        }

        private void UpdateVisualStates()
        {
            QuantumSubshellFromKind subshellFromKind = (QuantumSubshellFromKind)(this.QuantumSubshellKind);
            string state = subshellFromKind.ToString();

            if (!string.IsNullOrEmpty(state))
                VisualStateManager.GoToState(this, state, false);
        }
    }
}