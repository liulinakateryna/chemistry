﻿using Chemistry.Core.Providers;
using Chemistry.Models.Models;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl.AtomModelCellsSchema
{
    [TemplatePart(Type = typeof(AtomModelPeriodCellsSchemaControl)),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period1),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period2),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period3),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period4),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period5),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period6),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = Period7)]
    public sealed class AtomModelPeriodCellsSchemaControl : Control
    {
        private const string PeriodStateGroup = "PeriodStateGroup";
        private const string Period1 = "Period1";
        private const string Period2 = "Period2";
        private const string Period3 = "Period3";
        private const string Period4 = "Period4";
        private const string Period5 = "Period5";
        private const string Period6 = "Period6";
        private const string Period7 = "Period7";

        public AtomModelPeriodCellsSchemaControl()
        {
            this.DefaultStyleKey = typeof(AtomModelPeriodCellsSchemaControl);
        }

        public static readonly DependencyProperty ElectronsShellSProperty = DependencyProperty.Register(
            nameof(ElectronsShellS),
            typeof(int),
            typeof(AtomModelPeriodCellsSchemaControl),
            null);

        public static readonly DependencyProperty ElectronsShellPProperty = DependencyProperty.Register(
            nameof(ElectronsShellP),
            typeof(int),
            typeof(AtomModelPeriodCellsSchemaControl),
            null);

        public static readonly DependencyProperty ElectronsShellDProperty = DependencyProperty.Register(
            nameof(ElectronsShellD),
            typeof(int),
            typeof(AtomModelPeriodCellsSchemaControl),
            null);

        public static readonly DependencyProperty ElectronsShellFProperty = DependencyProperty.Register(
            nameof(ElectronsShellF),
            typeof(int),
            typeof(AtomModelPeriodCellsSchemaControl),
            null);

        public static readonly DependencyProperty PeriodProperty = DependencyProperty.Register(
            nameof(Period),
            typeof(Period),
            typeof(AtomModelPeriodCellsSchemaControl),
            null);

        public static readonly DependencyProperty PeriodTextProperty = DependencyProperty.Register(
            nameof(PeriodText),
            typeof(string),
            typeof(AtomModelPeriodCellsSchemaControl),
            null);

        public int ElectronsShellS
        {
            get => (int)GetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellSProperty);
            set => SetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellSProperty, value);
        }

        public int ElectronsShellP
        {
            get => (int)GetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellPProperty);
            set => SetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellPProperty, value);
        }

        public int ElectronsShellD
        {
            get => (int)GetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellDProperty);
            set => SetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellDProperty, value);
        }

        public int ElectronsShellF
        {
            get => (int)GetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellFProperty);
            set => SetValue(AtomModelPeriodCellsSchemaControl.ElectronsShellFProperty, value);
        }

        protected override void OnApplyTemplate()
        {
            this.UpdateShellsElectronsNumber();
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        public Period Period
        {
            get => (Period)GetValue(AtomModelPeriodCellsSchemaControl.PeriodProperty);
            set
            {
                SetValue(AtomModelPeriodCellsSchemaControl.PeriodProperty, value);

                this.UpdateShellsElectronsNumber();
                this.UpdateVisualStates();
            }
        }

        public string PeriodText
        {
            get => (string)GetValue(AtomModelPeriodCellsSchemaControl.PeriodTextProperty);
            set => SetValue(AtomModelPeriodCellsSchemaControl.PeriodTextProperty, value);
        }

        private void UpdateShellsElectronsNumber()
        {
            this.PeriodText = this.Period == null ? "" : this.Period.PeriodValue.ToString();
            this.ElectronsShellS = this.Period == null ? 0 : this.Period.ElectronsS;
            this.ElectronsShellP = this.Period == null ? 0 : this.Period.ElectronsP;
            this.ElectronsShellD = this.Period == null ? 0 : this.Period.ElectronsD;
            this.ElectronsShellF = this.Period == null ? 0 : this.Period.ElectronsF;
        }

        private void UpdateVisualStates()
        {
            if (this.Period.PeriodValue > 0 && this.Period.PeriodValue < 8)
                VisualStateManager.GoToState(this, $"Period{this.Period.PeriodValue}", false);
        }
    }
}