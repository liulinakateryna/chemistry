﻿using Chemistry.Core.Providers;
using Chemistry.Models.Models;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl.AtomModelCellsSchema
{
    [TemplatePart(Type = typeof(AtomModelPeriodCellsSchemaControl)),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod1),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod2),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod3),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod4),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod5),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod6),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod7)]
    public sealed class AtomModelCellsSchemaControl : Control
    {
        private readonly AtomModelProvider atomModelProvider;

        private const string PeriodStateGroup = "PeriodStateGroup";
        private const string VisualStatePeriod1 = "VisualStatePeriod1";
        private const string VisualStatePeriod2 = "VisualStatePeriod2";
        private const string VisualStatePeriod3 = "VisualStatePeriod3";
        private const string VisualStatePeriod4 = "VisualStatePeriod4";
        private const string VisualStatePeriod5 = "VisualStatePeriod5";
        private const string VisualStatePeriod6 = "VisualStatePeriod6";
        private const string VisualStatePeriod7 = "VisualStatePeriod7";

        public static readonly DependencyProperty AtomProperty = DependencyProperty.Register(
           nameof(Atom),
           typeof(Atom),
           typeof(AtomModelCellsSchemaControl),
           null);

        public Atom Atom
        {
            get => (Atom)GetValue(AtomModelCellsSchemaControl.AtomProperty);
            set
            {
                if (value != null)
                {
                    SetValue(AtomModelCellsSchemaControl.AtomProperty, value);
                    this.SetPeriods();
                    this.UpdateVisualStates();
                }
            }
        }

        public AtomModelCellsSchemaControl()
        {
            this.DefaultStyleKey = typeof(AtomModelCellsSchemaControl);
            this.atomModelProvider = new AtomModelProvider();
        }

        protected override void OnApplyTemplate()
        {
            this.SetPeriods();
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        public static readonly DependencyProperty Period1Property = DependencyProperty.Register(
           nameof(Period1),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public static readonly DependencyProperty Period2Property = DependencyProperty.Register(
           nameof(Period2),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public static readonly DependencyProperty Period3Property = DependencyProperty.Register(
           nameof(Period3),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public static readonly DependencyProperty Period4Property = DependencyProperty.Register(
           nameof(Period4),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public static readonly DependencyProperty Period5Property = DependencyProperty.Register(
           nameof(Period5),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public static readonly DependencyProperty Period6Property = DependencyProperty.Register(
           nameof(Period6),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public static readonly DependencyProperty Period7Property = DependencyProperty.Register(
           nameof(Period7),
           typeof(Period),
           typeof(AtomModelCellsSchemaControl),
           null);

        public Period Period1
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period1Property);
            set => SetValue(AtomModelCellsSchemaControl.Period1Property, value);
        }

        public Period Period2
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period2Property);
            set => SetValue(AtomModelCellsSchemaControl.Period2Property, value);
        }

        public Period Period3
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period3Property);
            set => SetValue(AtomModelCellsSchemaControl.Period3Property, value);
        }

        public Period Period4
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period4Property);
            set => SetValue(AtomModelCellsSchemaControl.Period4Property, value);
        }

        public Period Period5
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period5Property);
            set => SetValue(AtomModelCellsSchemaControl.Period5Property, value);
        }

        public Period Period6
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period6Property);
            set => SetValue(AtomModelCellsSchemaControl.Period6Property, value);
        }

        public Period Period7
        {
            get => (Period)GetValue(AtomModelCellsSchemaControl.Period7Property);
            set => SetValue(AtomModelCellsSchemaControl.Period7Property, value);
        }

        private void SetPeriods()
        {
            int atomicNumber = 0;

            if (this.Atom != null)
                atomicNumber = this.Atom.AtomicNumber;

            Dictionary<int, Period> periods = this.atomModelProvider.GetElectronsDistribution(atomicNumber);
            this.Period1 = periods[1];
            this.Period2 = periods[2];
            this.Period3 = periods[3];
            this.Period4 = periods[4];
            this.Period5 = periods[5];
            this.Period6 = periods[6];
            this.Period7 = periods[7];
        }

        private void UpdateVisualStates()
        {
            int period = 0;

            if (this.Atom != null)
                period = this.Atom.Period;

            if (period > 0 && period < 8)
                VisualStateManager.GoToState(this, $"VisualStatePeriod{period}", false);
        }
    }
}