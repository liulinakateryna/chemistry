﻿using Chemistry.Models.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl.AtomShellsSchema
{
    public sealed class AtomShellControl : Control
    {
        public AtomShellControl()
        {
            this.DefaultStyleKey = typeof(AtomShellControl);
        }

        public static readonly DependencyProperty NumberOfElectronsProperty = DependencyProperty.Register(
            nameof(NumberOfElectrons),
            typeof(string),
            typeof(AtomShellControl),
            null);

        public static readonly DependencyProperty PeriodProperty = DependencyProperty.Register(
            nameof(Period),
            typeof(Period),
            typeof(AtomShellControl),
            null);

        public static readonly DependencyProperty PeriodTextProperty = DependencyProperty.Register(
            nameof(PeriodText),
            typeof(string),
            typeof(AtomShellControl),
            null);

        protected override void OnApplyTemplate()
        {
            this.UpdateValues();

            base.OnApplyTemplate();
        }

        public string NumberOfElectrons
        {
            get => (string)GetValue(AtomShellControl.NumberOfElectronsProperty);
            set => SetValue(AtomShellControl.NumberOfElectronsProperty, value);
        }

        public Period Period
        {
            get => (Period)GetValue(AtomShellControl.PeriodProperty);
            set
            {
                SetValue(AtomShellControl.PeriodProperty, value);
                this.UpdateValues();
            }
        }

        public string PeriodText
        {
            get => (string)GetValue(AtomShellControl.PeriodTextProperty);
            set => SetValue(AtomShellControl.PeriodTextProperty, value);
        }

        private void UpdateValues()
        {
            if (this.Period == null)
                return;

            this.PeriodText = this.Period.PeriodValue.ToString();
            this.NumberOfElectrons = $"{this.Period.ElectronsD + this.Period.ElectronsF + this.Period.ElectronsP + this.Period.ElectronsS}";
        }
    }
}