﻿using Chemistry.Core.Providers;
using Chemistry.Models.Models;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl.AtomShellsSchema
{
    [TemplatePart(Type = typeof(AtomShellsSchemaControl)),
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = S),
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = P),
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = D),
        TemplateVisualState(GroupName = ChemicalElementTypeStateGroup, Name = F),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod1),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod2),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod3),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod4),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod5),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod6),
        TemplateVisualState(GroupName = PeriodStateGroup, Name = VisualStatePeriod7)]
    public sealed class AtomShellsSchemaControl : Control
    {
        private readonly AtomModelProvider atomModelProvider;

        private const string ChemicalElementTypeStateGroup = "ChemicalElementTypeStateGroup";
        private const string S = "S";
        private const string P = "P";
        private const string D = "D";
        private const string F = "F";

        private const string PeriodStateGroup = "PeriodStateGroup";
        private const string VisualStatePeriod1 = "VisualStatePeriod1";
        private const string VisualStatePeriod2 = "VisualStatePeriod2";
        private const string VisualStatePeriod3 = "VisualStatePeriod3";
        private const string VisualStatePeriod4 = "VisualStatePeriod4";
        private const string VisualStatePeriod5 = "VisualStatePeriod5";
        private const string VisualStatePeriod6 = "VisualStatePeriod6";
        private const string VisualStatePeriod7 = "VisualStatePeriod7";

        public static readonly DependencyProperty AtomProperty = DependencyProperty.Register(
           nameof(Atom),
           typeof(Atom),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty AtomChargeProperty = DependencyProperty.Register(
           nameof(AtomCharge),
           typeof(string),
           typeof(AtomShellsSchemaControl),
           null);

        public Atom Atom
        {
            get => (Atom)GetValue(AtomShellsSchemaControl.AtomProperty);
            set
            {
                if (value != null)
                {
                    SetValue(AtomShellsSchemaControl.AtomProperty, value);
                    this.UpdateAtomValues();
                    this.UpdateVisualStates();
                }
            }
        }

        public string AtomCharge
        {
            get => (string)GetValue(AtomShellsSchemaControl.AtomChargeProperty);
            set => SetValue(AtomShellsSchemaControl.AtomChargeProperty, value);
        }

        public AtomShellsSchemaControl()
        {
            this.DefaultStyleKey = typeof(AtomShellsSchemaControl);
            this.atomModelProvider = new AtomModelProvider();
        }

        public static readonly DependencyProperty Period1Property = DependencyProperty.Register(
           nameof(Period1),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty Period2Property = DependencyProperty.Register(
           nameof(Period2),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty Period3Property = DependencyProperty.Register(
           nameof(Period3),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty Period4Property = DependencyProperty.Register(
           nameof(Period4),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty Period5Property = DependencyProperty.Register(
           nameof(Period5),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty Period6Property = DependencyProperty.Register(
           nameof(Period6),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public static readonly DependencyProperty Period7Property = DependencyProperty.Register(
           nameof(Period7),
           typeof(Period),
           typeof(AtomShellsSchemaControl),
           null);

        public Period Period1
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period1Property);
            set => SetValue(AtomShellsSchemaControl.Period1Property, value);
        }

        public Period Period2
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period2Property);
            set => SetValue(AtomShellsSchemaControl.Period2Property, value);
        }

        public Period Period3
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period3Property);
            set => SetValue(AtomShellsSchemaControl.Period3Property, value);
        }

        public Period Period4
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period4Property);
            set => SetValue(AtomShellsSchemaControl.Period4Property, value);
        }

        public Period Period5
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period5Property);
            set => SetValue(AtomShellsSchemaControl.Period5Property, value);
        }

        public Period Period6
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period6Property);
            set => SetValue(AtomShellsSchemaControl.Period6Property, value);
        }

        public Period Period7
        {
            get => (Period)GetValue(AtomShellsSchemaControl.Period7Property);
            set => SetValue(AtomShellsSchemaControl.Period7Property, value);
        }

        private void SetPeriods()
        {
            int atomicNumber = 0;

            if (this.Atom != null)
                atomicNumber = this.Atom.AtomicNumber;

            Dictionary<int, Period> periods = this.atomModelProvider.GetElectronsDistribution(atomicNumber);
            this.Period1 = periods[1];
            this.Period2 = periods[2];
            this.Period3 = periods[3];
            this.Period4 = periods[4];
            this.Period5 = periods[5];
            this.Period6 = periods[6];
            this.Period7 = periods[7];
        }

        protected override void OnApplyTemplate()
        {
            this.UpdateAtomValues();
            base.OnApplyTemplate();
            this.UpdateVisualStates();
        }

        private void UpdateAtomValues()
        {
            if (this.Atom == null)
                return;

            this.AtomCharge = $"+{this.Atom.AtomicNumber}";
            this.SetPeriods();
        }

        private void UpdateVisualStates()
        {
            string state = this.Atom?.ElementType.ToString();

            if (!string.IsNullOrEmpty(state))
                VisualStateManager.GoToState(this, state, false);

            int period = 0;

            if (this.Atom != null)
                period = this.Atom.Period;

            if (period > 0 && period < 8)
                VisualStateManager.GoToState(this, $"VisualStatePeriod{period}", false);
        }
    }
}