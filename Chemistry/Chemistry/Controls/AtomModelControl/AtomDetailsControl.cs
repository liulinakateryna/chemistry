﻿using Chemistry.Models.Models;
using Chemistry.Resources;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Chemistry.Controls.AtomModelControl
{
    public sealed class AtomDetailsControl : Control
    {
        public Atom Atom
        {
            get => (Atom)GetValue(AtomDetailsControl.AtomProperty);
            set
            {
                if (value != null)
                {
                    SetValue(AtomDetailsControl.AtomProperty, value);
                    this.UpdateAtomValues();
                }
            }
        }

        public string Abbreviation
        {
            get => (string)GetValue(AtomDetailsControl.AbbreviationProperty);
            set =>  SetValue(AtomDetailsControl.AbbreviationProperty, value);
        }

        public string AtomicNumber
        {
            get => (string)GetValue(AtomDetailsControl.AtomicNumberProperty);
            set => SetValue(AtomDetailsControl.AtomicNumberProperty, value);
        }

        public string Ar
        {
            get => (string)GetValue(AtomDetailsControl.ArProperty);
            set => SetValue(AtomDetailsControl.ArProperty, value);
        }

        public AtomDetailsControl()
        {
            this.DefaultStyleKey = typeof(AtomDetailsControl);
        }

        public static readonly DependencyProperty AbbreviationProperty = DependencyProperty.Register(
           nameof(Abbreviation),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        public static readonly DependencyProperty AtomicNumberProperty = DependencyProperty.Register(
           nameof(AtomicNumber),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        public static readonly DependencyProperty ArProperty = DependencyProperty.Register(
           nameof(Ar),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        public static readonly DependencyProperty AtomProperty = DependencyProperty.Register(
           nameof(Atom),
           typeof(Atom),
           typeof(AtomDetailsControl),
           null);
        
        public string KernelCharge
        {
            get => (string)GetValue(AtomDetailsControl.KernelChargeProperty);
            set => SetValue(AtomDetailsControl.KernelChargeProperty, value);
        }

        public static readonly DependencyProperty KernelChargeProperty = DependencyProperty.Register(
           nameof(KernelCharge),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        public string NameValue
        {
            get => (string)GetValue(AtomDetailsControl.NameValueProperty);
            set => SetValue(AtomDetailsControl.NameValueProperty, value);
        }

        public static readonly DependencyProperty NameValueProperty = DependencyProperty.Register(
           nameof(NameValue),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        public string Period
        {
            get => (string)GetValue(AtomDetailsControl.PeriodProperty);
            set => SetValue(AtomDetailsControl.PeriodProperty, value);
        }

        public static readonly DependencyProperty PeriodProperty = DependencyProperty.Register(
           nameof(Period),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        public string Properties
        {
            get => (string)GetValue(AtomDetailsControl.PropertiesProperty);
            set => SetValue(AtomDetailsControl.PropertiesProperty, value);
        }

        public static readonly DependencyProperty PropertiesProperty = DependencyProperty.Register(
           nameof(Properties),
           typeof(string),
           typeof(AtomDetailsControl),
           null);

        protected override void OnApplyTemplate()
        {
            this.UpdateAtomValues();
            base.OnApplyTemplate();
        }

        private void UpdateAtomValues()
        {
            if (this.Atom == null)
                return;

            this.Abbreviation = this.Atom.Abbreviation;
            this.Ar = $"{this.Atom.Ar}";
            this.AtomicNumber = $"{this.Atom.AtomicNumber}";
            this.KernelCharge = $"+{this.Atom.AtomicNumber}";
            this.NameValue = PageResources.GetText(this.Atom.Abbreviation);
            this.Period = $"{this.Atom.Period}";
            this.Properties = PageResources.GetText($"Details{this.Atom.Abbreviation}");
        }
    }
}