﻿using Chemistry.Models.Commands;
using Chemistry.Models.Enum;
using Chemistry.Models.Parameters;
using Chemistry.Popups;
using Chemistry.UI.Core.PopUp;
using Chemistry.ViewModels.ViewModels;
using System;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace Chemistry
{
    public sealed partial class MainPage : Page
    {
        private bool alreadyCleanedUp;
        private ContentControl popupContentControl;
        private Grid popupContentBackground;

        public MainPage()
        {
            this.InitializeComponent();
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(900, 600));
            this.Loaded += this.OnLoaded;

            //
            // Hide title bar.
            //
            ApplicationViewTitleBar titleBar = ApplicationView.GetForCurrentView().TitleBar;
            titleBar.BackgroundColor = Color.FromArgb(0, 0, 0, 0);
            titleBar.ButtonBackgroundColor = Color.FromArgb(200, 253, 252, 158);
            titleBar.ButtonHoverBackgroundColor = Color.FromArgb(100, 253, 252, 158);
            CoreApplicationViewTitleBar coreTitleBar = CoreApplication.GetCurrentView().TitleBar;
            coreTitleBar.ExtendViewIntoTitleBar = true;

            //
            // Back button.
            //
            var currentView = SystemNavigationManager.GetForCurrentView();
            currentView.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            currentView.BackRequested += NavigateBack;
        }

        private void NavigateBack(object sender, BackRequestedEventArgs e)
        {
            var resource = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;

            if (resource?.MenuViewModel.CanNavigateBack == true)
                resource?.MenuViewModel.NavigateBackCommand.Execute(null);
        }

        private async void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= OnLoaded;
            this.popupContentBackground = this.PopupContentBackground;
            this.popupContentControl = this.PopupContentControl;
            PopupContentProvider.Instance.PopupContentChanged += this.OnPopupContentChanged;
            PopupContentProvider.Instance.ShowPopupContent(PopupNavigationSource.Menu);
        }

        private void OnPopupContentChanged(object sender, PopupContentChangedEventArgs eventArgs)
        {
            if (eventArgs?.Type != null)
            {
                this.alreadyCleanedUp = false;
                this.popupContentControl.Content = Activator.CreateInstance(eventArgs.Type);
                this.popupContentBackground.Visibility = this.PopupContentControl.Visibility = Visibility.Visible;
                this.popupContentBackground.Tapped -= this.OnPopupContentBackgroundTapped;
                this.popupContentBackground.Tapped += this.OnPopupContentBackgroundTapped;
            }
            else if (!this.alreadyCleanedUp)
                this.CleanAndClose();
        }


        private void CleanAndClose()
        {
            this.alreadyCleanedUp = true;
            PopupContentProvider.Instance.ShowPopupContent(PopupNavigationSource.CloseAnyPopup);
            this.popupContentBackground.Visibility = this.popupContentControl.Visibility = Visibility.Collapsed;
            this.popupContentBackground.Tapped -= this.OnPopupContentBackgroundTapped;
        }

        private async void OnPopupContentBackgroundTapped(object sender, TappedRoutedEventArgs e)
        {
            DismissablePopup dismissablePopup = this.popupContentControl.Content as DismissablePopup;

            bool canClose = true;

            if (dismissablePopup != null)
            {
                canClose = dismissablePopup.IsDismissEnabled;

                if (dismissablePopup.DismissCommand != null)
                {
                    if (dismissablePopup.DismissCommand is IAsyncCommand<object> awaitableCommand &&
                        awaitableCommand.CanExecute(dismissablePopup.DismissCommandParameter))
                    {
                        await awaitableCommand.ExecuteAsync(dismissablePopup.DismissCommandParameter);
                    }
                    else if (dismissablePopup.DismissCommand.CanExecute(dismissablePopup.DismissCommandParameter))
                        dismissablePopup.DismissCommand.Execute(dismissablePopup.DismissCommandParameter);
                }
            }

            if (!this.alreadyCleanedUp && canClose)
                this.CleanAndClose();
        }

    }
}