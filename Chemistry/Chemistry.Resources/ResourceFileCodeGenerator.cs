﻿
namespace Chemistry.Resources
{
	using System.Globalization;
	using Chemistry.Resources.Core;
	using System;

	/// <summary>
    /// Provides access to resources from PageResources.resw file.
    /// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors", Justification = "Yet it will be unstatic.")]
	public class PageResources
	{
		/// <summary>
        /// Contains logic for accessing contsnt of resource file.
        /// </summary>
		private static ResourceProvider resourceProvider = new ResourceProvider("Chemistry.Resources/PageResources");
		
		/// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        public static CultureInfo Culture
        {
            get => resourceProvider.OverridedCulture;
            set => resourceProvider.OverridedCulture = value;
        }

        /// <summary>
        /// Gets the text with the current locale related to the specified <paramref name="resourceKey"/>.
        /// </summary>
        /// <param name="resourceKey">The resource key.</param>
        /// <returns>The text with the current locale</returns>
        public static string GetText(string resourceKey)
            => resourceProvider.GetString(resourceKey);

		/// <summary>
        /// Gets a localized string similar to "Abbreviation:"
        /// </summary>
		public static string Abbreviation => resourceProvider.GetString("Abbreviation");

		/// <summary>
        /// Gets a localized string similar to "Actinium"
        /// </summary>
		public static string Ac => resourceProvider.GetString("Ac");

		/// <summary>
        /// Gets a localized string similar to "Acid"
        /// </summary>
		public static string Acid => resourceProvider.GetString("Acid");

		/// <summary>
        /// Gets a localized string similar to "Silver"
        /// </summary>
		public static string Ag => resourceProvider.GetString("Ag");

		/// <summary>
        /// Gets a localized string similar to "Aluminium"
        /// </summary>
		public static string Al => resourceProvider.GetString("Al");

		/// <summary>
        /// Gets a localized string similar to "Alkali"
        /// </summary>
		public static string Alkali => resourceProvider.GetString("Alkali");

		/// <summary>
        /// Gets a localized string similar to "Americium"
        /// </summary>
		public static string Am => resourceProvider.GetString("Am");

		/// <summary>
        /// Gets a localized string similar to "Chemistry"
        /// </summary>
		public static string AppName => resourceProvider.GetString("AppName");

		/// <summary>
        /// Gets a localized string similar to "Argon"
        /// </summary>
		public static string Ar => resourceProvider.GetString("Ar");

		/// <summary>
        /// Gets a localized string similar to "Arsenic"
        /// </summary>
		public static string As => resourceProvider.GetString("As");

		/// <summary>
        /// Gets a localized string similar to " Astatine"
        /// </summary>
		public static string At => resourceProvider.GetString("At");

		/// <summary>
        /// Gets a localized string similar to "Atomic number:"
        /// </summary>
		public static string AtomicNumber => resourceProvider.GetString("AtomicNumber");

		/// <summary>
        /// Gets a localized string similar to "Atom model"
        /// </summary>
		public static string AtomModel => resourceProvider.GetString("AtomModel");

		/// <summary>
        /// Gets a localized string similar to "Atom properties"
        /// </summary>
		public static string AtomProperties => resourceProvider.GetString("AtomProperties");

		/// <summary>
        /// Gets a localized string similar to "Atom Shells"
        /// </summary>
		public static string AtomShells => resourceProvider.GetString("AtomShells");

		/// <summary>
        /// Gets a localized string similar to "Atom valances"
        /// </summary>
		public static string AtomValances => resourceProvider.GetString("AtomValances");

		/// <summary>
        /// Gets a localized string similar to "Ar:"
        /// </summary>
		public static string AtomWeight => resourceProvider.GetString("AtomWeight");

		/// <summary>
        /// Gets a localized string similar to "Gold"
        /// </summary>
		public static string Au => resourceProvider.GetString("Au");

		/// <summary>
        /// Gets a localized string similar to "Boron"
        /// </summary>
		public static string B => resourceProvider.GetString("B");

		/// <summary>
        /// Gets a localized string similar to "Barium"
        /// </summary>
		public static string Ba => resourceProvider.GetString("Ba");

		/// <summary>
        /// Gets a localized string similar to "Beryllium"
        /// </summary>
		public static string Be => resourceProvider.GetString("Be");

		/// <summary>
        /// Gets a localized string similar to "Bohrium"
        /// </summary>
		public static string Bh => resourceProvider.GetString("Bh");

		/// <summary>
        /// Gets a localized string similar to "Bismuth"
        /// </summary>
		public static string Bi => resourceProvider.GetString("Bi");

		/// <summary>
        /// Gets a localized string similar to "Berkelium"
        /// </summary>
		public static string Bk => resourceProvider.GetString("Bk");

		/// <summary>
        /// Gets a localized string similar to "Bromine"
        /// </summary>
		public static string Br => resourceProvider.GetString("Br");

		/// <summary>
        /// Gets a localized string similar to "Build molecule"
        /// </summary>
		public static string BuildMolecule => resourceProvider.GetString("BuildMolecule");

		/// <summary>
        /// Gets a localized string similar to "Carbon"
        /// </summary>
		public static string C => resourceProvider.GetString("C");

		/// <summary>
        /// Gets a localized string similar to "Calcium"
        /// </summary>
		public static string Ca => resourceProvider.GetString("Ca");

		/// <summary>
        /// Gets a localized string similar to "Cadmium"
        /// </summary>
		public static string Cd => resourceProvider.GetString("Cd");

		/// <summary>
        /// Gets a localized string similar to "Cerium"
        /// </summary>
		public static string Ce => resourceProvider.GetString("Ce");

		/// <summary>
        /// Gets a localized string similar to "Californium"
        /// </summary>
		public static string Cf => resourceProvider.GetString("Cf");

		/// <summary>
        /// Gets a localized string similar to "Chemical element"
        /// </summary>
		public static string ChemicalElement => resourceProvider.GetString("ChemicalElement");

		/// <summary>
        /// Gets a localized string similar to "Chemicals"
        /// </summary>
		public static string Chemicals => resourceProvider.GetString("Chemicals");

		/// <summary>
        /// Gets a localized string similar to "Chlorine"
        /// </summary>
		public static string Cl => resourceProvider.GetString("Cl");

		/// <summary>
        /// Gets a localized string similar to "Clear"
        /// </summary>
		public static string Clear => resourceProvider.GetString("Clear");

		/// <summary>
        /// Gets a localized string similar to " Curium"
        /// </summary>
		public static string Cm => resourceProvider.GetString("Cm");

		/// <summary>
        /// Gets a localized string similar to "Copernicium"
        /// </summary>
		public static string Cn => resourceProvider.GetString("Cn");

		/// <summary>
        /// Gets a localized string similar to "Cobalt"
        /// </summary>
		public static string Co => resourceProvider.GetString("Co");

		/// <summary>
        /// Gets a localized string similar to "Chromium"
        /// </summary>
		public static string Cr => resourceProvider.GetString("Cr");

		/// <summary>
        /// Gets a localized string similar to "Caesium"
        /// </summary>
		public static string Cs => resourceProvider.GetString("Cs");

		/// <summary>
        /// Gets a localized string similar to "Copper"
        /// </summary>
		public static string Cu => resourceProvider.GetString("Cu");

		/// <summary>
        /// Gets a localized string similar to "Dubnium"
        /// </summary>
		public static string Db => resourceProvider.GetString("Db");

		/// <summary>
        /// Gets a localized string similar to "Details"
        /// </summary>
		public static string Details => resourceProvider.GetString("Details");

		/// <summary>
        /// Gets a localized string similar to "Actinium is a silver-white metal, heavy, soft, in appearance resembles lanthanum. Due to radioactivity in the dark it glows with a characteristic blue color. In moist air it is covered with an oxide film. Strong reducing agent, reacts with water and dilute acids. The chemical properties of actinium are also very similar to lanthanum, in the compounds it takes an oxidation state of +3 (Ac2O3, AcBr3, Ac (OH) 3), but is characterized by high reactivity and more basic properties."
        /// </summary>
		public static string DetailsAc => resourceProvider.GetString("DetailsAc");

		/// <summary>
        /// Gets a localized string similar to "A simple substance, silver is a malleable, ductile noble metal of silver-white color. It has a relatively low reactivity; it does not dissolve in hydrochloric and dilute sulfuric acids. However, in an oxidizing environment (in nitric, hot concentrated sulfuric acid, as well as in hydrochloric acid in the presence of free oxygen), silver dissolves. Silver is not oxidized by oxygen even at high temperatures, but in the form of thin films it can be oxidized by oxygen plasma or ozone when irradiated with ultraviolet light. In humid air, in the presence of even the slightest trace of divalent sulfur (hydrogen sulfide, thiosulfates, rubber), a coating of poorly soluble silver sulfide is formed, which causes the darkening of silver products. Free halogens easily oxidize silver to halides."
        /// </summary>
		public static string DetailsAg => resourceProvider.GetString("DetailsAg");

		/// <summary>
        /// Gets a localized string similar to "Aluminum - a light paramagnetic metal of silver-white color, easily amenable to molding, casting, machining. Under normal conditions, aluminum is coated with a thin and durable oxide film and therefore does not react with classic oxidizing agents. It readily reacts with oxygen, with halogens at room temperature, reacts with other non-metals when heated, as well as with alkalis and acids, with water."
        /// </summary>
		public static string DetailsAl => resourceProvider.GetString("DetailsAl");

		/// <summary>
        /// Gets a localized string similar to "Americium - silver-white metal, viscous and malleable. Glows in the dark due to its own α-radiation. Americium is a strong reducing agent. Reacts with hot water. Interacts with dilute acids. When heated with halogens, it gives tri- and tetrahalides, depending on the reaction conditions. Vigorously reacts with hydrogen at a temperature of 50-60 ° C."
        /// </summary>
		public static string DetailsAm => resourceProvider.GetString("DetailsAm");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is argon - an inert monatomic gas without color, taste or smell. So far, only 2 chemical compounds of argon are known - argon hydrofluoride and CU (Ar) O, which exist at very low temperatures. There is reason to believe that the extremely unstable Hg — Ar compound formed in an electric discharge is a truly chemical (valence) compound."
        /// </summary>
		public static string DetailsAr => resourceProvider.GetString("DetailsAr");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is a brittle steel semimetal with a greenish tint. Poison and carcinogen. Arsenic is chemically active. In air, at normal temperature, even compact (fused) metal arsenic easily oxidizes; upon heating, powdered arsenic ignites and burns with a blue flame to form As2O3 oxide. Under the action of strong reducing agents, it exhibits oxidizing properties. So, under the action of metals and hydrogen at the time of release, it is able to give the corresponding metal and hydrogen compounds."
        /// </summary>
		public static string DetailsAs => resourceProvider.GetString("DetailsAs");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of astatine under normal conditions is unstable crystals of a dark blue color. Astatine in aqueous solution is reduced with sulfur dioxide SO2. Like all halogens (except fluorine), astatine forms an insoluble salt of AgAt (silver astatide). Astatine reacts with bromine and iodine. Astatine is dissolved in dilute hydrochloric and nitric acids."
        /// </summary>
		public static string DetailsAt => resourceProvider.GetString("DetailsAt");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is gold - a noble yellow metal. Gold is one of the most inert metals. Under normal conditions, it does not interact with most acids and does not form oxides; therefore, it is classified as a noble metal, unlike ordinary metals, which are destroyed by acids and alkalis. In the XIV century, the ability of royal vodka to dissolve gold was discovered, which refuted the opinion of its chemical inertness. Of pure acids, gold dissolves only in concentrated selenic acid at 200 ° C. At room temperature (20-30 ° C), gold reacts with liquid bromine and its solutions in water and organic solvents."
        /// </summary>
		public static string DetailsAu => resourceProvider.GetString("DetailsAu");

		/// <summary>
        /// Gets a localized string similar to "Boron is a colorless, gray or red crystalline or dark amorphous substance. Chemically, boron is quite inert and at room temperature interacts only with fluorine. When heated, boron reacts with other halogens with the formation of trihalides, with nitrogen it forms boron nitride BN, with phosphorus - BP phosphide, with carbon - carbides of various compositions (B4C, B12C3, B13C2). When heated in an atmosphere of oxygen or in air, boron burns with a large release of heat, and oxide B2O3 is formed. Boron does not directly interact with hydrogen, although a rather large number of boron hydrogen of various compositions is known. With strong heating, boron exhibits reducing properties. It is capable, for example, of reducing silicon or phosphorus from their oxides."
        /// </summary>
		public static string DetailsB => resourceProvider.GetString("DetailsB");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is barium - a soft, malleable alkaline earth metal of silver-white color. With a sharp blow, it splits. It has high chemical activity. In air, barium rapidly oxidizes, forming a mixture of barium oxide BaO and barium nitride Ba3N2, and ignites with slight heating. Vigorously reacts with water to form barium hydroxide Ba (OH) 2. Actively interacts with dilute acids. Many barium salts are insoluble or slightly soluble in water. It easily reacts with halogens to form halides. When heated with hydrogen, it forms barium hydride BaH2. Barium reduces the oxides, halides and sulfides of many metals to the corresponding metal."
        /// </summary>
		public static string DetailsBa => resourceProvider.GetString("DetailsBa");

		/// <summary>
        /// Gets a localized string similar to "Beryllium is a relatively solid metal of light gray color, has a very high cost. Toxic Beryllium is characterized by two oxidation states +1 and +2. Beryllium (II) hydroxide is amphoteren, and both basic and acid properties are weakly expressed. Metallic beryllium is relatively poorly reactive at room temperature. In a compact form, it does not react with water and water vapor even at a red-hot temperature and is not oxidized by air to 600 ° C. When ignited, beryllium powder burns with a bright flame, and oxide and nitride are formed. Halogens react with beryllium at temperatures above 600 ° C. Ammonia interacts with beryllium at temperatures above 1200 ° C to form Be3N2 nitride, and carbon gives Be2C carbide at 1700 ° C. Beryllium does not directly react with hydrogen."
        /// </summary>
		public static string DetailsBe => resourceProvider.GetString("DetailsBe");

		/// <summary>
        /// Gets a localized string similar to "Borium is an unstable radioactive chemical element with atomic number 107. Isotopes with mass numbers from 261 to 272 are known. The most stable isotope from those obtained is borium-267 with a half-life of 17 s."
        /// </summary>
		public static string DetailsBh => resourceProvider.GetString("DetailsBh");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of bismuth is, under normal conditions, a shiny silver with a pinkish tint. In compounds, bismuth exhibits oxidation states of −3, +1, +2, +3, +4, +5. At room temperature it does not oxidize in dry air, but in a moist air it is covered with a thin film of oxide. Upon reaching a temperature above 1000 ° C, it burns with the formation of Bi2O3 oxide. Slightly dissolves phosphorus. Hydrogen in solid and liquid bismuth practically does not dissolve. Bismuth does not interact with carbon, nitrogen and silicon."
        /// </summary>
		public static string DetailsBi => resourceProvider.GetString("DetailsBi");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of Berkeley is a silver-white radioactive metal. Berkeley has been found to be very reactive. In its many compounds, it has oxidation states of + 3 and + 4. It interacts with oxygen (trioxide and dioxide), halogens and sulfur. Berkelium oxides, fluorides, phosphates and carbonates are insoluble in water."
        /// </summary>
		public static string DetailsBk => resourceProvider.GetString("DetailsBk");

		/// <summary>
        /// Gets a localized string similar to "The simple substance bromine under normal conditions is a heavy caustic liquid of reddish-brown color with a strong unpleasant "heavy" odor, vaguely reminiscent of the smell of both iodine and chlorine. Flying, poisonous. The bromine molecule is diatomic (formula Br2). A little bromine, but better than other halogens, soluble in water (3.58 g per 100 g of water at 20 ° C), the solution is called bromine water. The reaction of bromine with hydrogen proceeds when heated and leads to the formation of hydrogen bromide HBr.Brom does not directly react with oxygen and nitrogen. Bromine forms a large number of different compounds with the rest of the halogens. For example, with fluorine, bromine forms unstable BrF3 and BrF5, with iodine - IBr. When interacting with many metals, bromine forms bromides, for example, AlBr3, CuBr2, MgBr2 and others. Tantalum and platinum are resistant to bromine, to a lesser extent silver, titanium and lead."
        /// </summary>
		public static string DetailsBr => resourceProvider.GetString("DetailsBr");

		/// <summary>
        /// Gets a localized string similar to "Carbon is a tetravalent non-metal, i.e., it has four free electrons for the formation of covalent chemical bonds. The most famous representations of carbon are diamond, graphite, carbin, activated or charcoal, soot, etc. At ordinary temperatures, carbon is chemically inert, at sufficiently high temperatures it combines with many elements, and exhibits strong reducing properties. The chemical activity of different forms of carbon decreases in a row: amorphous carbon, graphite, diamond, they ignite in air at temperatures above 300-501 ° C, 600-700 ° C and 800-1000 ° C, respectively. The oxidation state is from −4 to +4. Carbon reacts with non-metals when heated. The ability of carbon to form polymer chains gives rise to a huge class of carbon-based compounds, which are much larger than inorganic ones, and are studied by organic chemistry. Among them, the most extensive groups: hydrocarbons, proteins, fats, carbohydrates, etc."
        /// </summary>
		public static string DetailsC => resourceProvider.GetString("DetailsC");

		/// <summary>
        /// Gets a localized string similar to "A simple substance, calcium is a soft, chemically active alkaline earth metal of silver-white color. Calcium is a typical alkaline earth metal. It easily interacts with oxygen, carbon dioxide and air moisture, which is why the surface of calcium metal is usually dull gray, so calcium is usually stored in a laboratory, like other alkaline earth metals, in a tightly closed jar under a layer of kerosene or liquid paraffin. When heated in air or in oxygen, calcium ignites and burns with a red flame with an orange tint (“brick red”). With less active non-metals (hydrogen, boron, carbon, silicon, nitrogen, phosphorus and others), calcium interacts when heated."
        /// </summary>
		public static string DetailsCa => resourceProvider.GetString("DetailsCa");

		/// <summary>
        /// Gets a localized string similar to "The simple substance cadmium under normal conditions is a soft malleable ductile transition metal of silver-white color. It is stable in dry air, in the moist on its surface an oxide film is formed, which prevents further oxidation of the metal. Cadmium and its many compounds are poisonous. In air, cadmium is stable and does not lose its metallic luster. It reacts with oxygen only when heated to 350 ° C with the formation of cadmium oxide. Sulfides and oxides of these elements are practically insoluble in water. Cadmium does not interact with carbon and does not form carbides. It reacts with non-oxidizing acids to produce hydrogen."
        /// </summary>
		public static string DetailsCd => resourceProvider.GetString("DetailsCd");

		/// <summary>
        /// Gets a localized string similar to "Cerium is a silver-white viscous and malleable metal that can be easily forged and machined at room temperature. Rare earth metal, unstable in air, gradually oxidizes, turning into white oxide and cerium carbonate. When heated to + 160 ... + 180 ° C, it lights up in air; cerium powder is pyrophoric. Cerium reacts with acids, when boiled, it is oxidized by water, and is resistant to alkalis. Vigorously interacts with halogens, chalcogens, nitrogen and carbon."
        /// </summary>
		public static string DetailsCe => resourceProvider.GetString("DetailsCe");

		/// <summary>
        /// Gets a localized string similar to "California is a silver-white actinide metal. Pure metal is malleable and easy to cut with a blade. California is an extremely volatile metal. In chemical properties, California is similar to actinides. In solutions, Cf4 + is obtained by acting on Cf3 + compounds with strong oxidizing agents. The solid California diiodide CfI2 was synthesized. From aqueous solutions, Cf3 + is reduced to Cf2 + electrochemically."
        /// </summary>
		public static string DetailsCf => resourceProvider.GetString("DetailsCf");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is chlorine under normal conditions - a poisonous gas of yellowish-green color, heavier than air, with a pungent odor and a sweet, “metallic” taste. Chlorine molecule diatomic. Chlorine directly reacts with almost all metals. With non-metals (except carbon, nitrogen, fluorine, oxygen and inert gases) forms the corresponding chlorides. With oxygen, chlorine forms oxides in which it exhibits an oxidation state of +1 to +7."
        /// </summary>
		public static string DetailsCl => resourceProvider.GetString("DetailsCl");

		/// <summary>
        /// Gets a localized string similar to "Curium is a soft silver-white metal. Glows in the dark under the influence of its own α-radiation. In solution, the oxidation state is most stable +3. Salts of Cm (III) are formed upon dissolution of curium in acids."
        /// </summary>
		public static string DetailsCm => resourceProvider.GetString("DetailsCm");

		/// <summary>
        /// Gets a localized string similar to "Koperniy is the 112th chemical element. The core of the most stable of its known isotopes, 285Cn, consists of 112 protons, 173 neutrons and has a half-life of about 34 seconds, the atomic mass of this nuclide is 285.177 (4) a. e. m. Refers to the same chemical group as zinc, cadmium and mercury."
        /// </summary>
		public static string DetailsCn => resourceProvider.GetString("DetailsCn");

		/// <summary>
        /// Gets a localized string similar to "A simple cobalt substance is silver-white, slightly yellowish metal with a pinkish or bluish tint. In air, cobalt oxidizes at temperatures above 300 ° C. When heated, cobalt reacts with halogens, and cobalt (III) compounds are formed only with fluorine. With other oxidizing elements, such as carbon, phosphorus, nitrogen, selenium, silicon, boron, cobalt also forms complex compounds, which are mixtures where cobalt with oxidation states of 1, 2, 3 is present. Cobalt can dissolve hydrogen without forming chemical compounds ."
        /// </summary>
		public static string DetailsCo => resourceProvider.GetString("DetailsCo");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is chrome - a bluish-white solid metal. Chrome is sometimes referred to as ferrous metals. Chromium is characterized by oxidation states of +2, +3, and +6 (see table). Almost all chromium compounds are colored. Does not react with sulfuric and nitric acids. At 2000 ° C, it burns with the formation of green chromium oxide (III) Cr2O3. Synthesized chromium compounds with boron (borides Cr2B, CrB, Cr3B4, CrB2, CrB4 and Cr5B3), with carbon (carbides Cr23C6, Cr7C3 and Cr3C2), with silicon (silicides Cr3Si, Cr5Si3 and CrSi) and nitrogen (nitrides CrN and Cr2N)."
        /// </summary>
		public static string DetailsCr => resourceProvider.GetString("DetailsCr");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is cesium - a soft alkaline metal of silver-yellow color. Cesium is the most chemically active metal. and in air, cesium instantly oxidizes with ignition, forming CsO2 superoxide. The interaction with water occurs with an explosion, the product of the interaction is cesium hydroxide and hydrogen H2. Cesium reacts with ice (even at −120 ° C), simple alcohols, organohalogen compounds, heavy metal halides, acids, dry ice (the reaction proceeds with a strong explosion). Reacts with benzene. Many salts formed by cesium - nitrates, chlorides, bromides, fluorides, iodides, chromates, manganates, azides, cyanides, carbonates, etc. - are extremely easily soluble in water and a number of organic solvents."
        /// </summary>
		public static string DetailsCs => resourceProvider.GetString("DetailsCs");

		/// <summary>
        /// Gets a localized string similar to "The simple substance copper is a plastic transition metal of a golden pink color (pink color in the absence of an oxide film). In compounds, copper exhibits two oxidation states: +1 and +2. Does not change in air in the absence of moisture and carbon dioxide. It is a weak reducing agent, does not react with water and dilute hydrochloric acid. It is oxidized by concentrated sulfuric and nitric acids, aqua regia, oxygen, halogens, chalcogens, non-metal oxides. Reacts when heated with hydrogen halide."
        /// </summary>
		public static string DetailsCu => resourceProvider.GetString("DetailsCu");

		/// <summary>
        /// Gets a localized string similar to "Dubnium is a radioactive chemical element. The color is unknown, but probably metallic and silver white or gray."
        /// </summary>
		public static string DetailsDb => resourceProvider.GetString("DetailsDb");

		/// <summary>
        /// Gets a localized string similar to "Darmstadtiy is an artificially synthesized chemical element of the 10th group of the periodic system, atomic number 110. For isotopes with mass numbers 267–273, the half-life does not exceed several milliseconds. But for the heaviest known isotope (with a mass number of 281), the half-life is about 10 seconds."
        /// </summary>
		public static string DetailsDs => resourceProvider.GetString("DetailsDs");

		/// <summary>
        /// Gets a localized string similar to "Dysprosium is a rare-earth silver-gray metal. It is not found in nature in its pure form, but it is part of some minerals. In compounds, it exhibits an oxidation state of +3. Dysprosium metal is slowly oxidized in air at a temperature of 20 ° C. When heated, metallic dysprosium reacts with halogens, nitrogen, and hydrogen. It interacts with mineral acids (except for HF), forming salts of Dy (III), does not interact with alkali solutions."
        /// </summary>
		public static string DetailsDy => resourceProvider.GetString("DetailsDy");

		/// <summary>
        /// Gets a localized string similar to "Erbium is a white (silver-white) metal. It is steady in air. When heated, reacts with oxygen, halogens and other non-metals. Erbium oxide Er2O3 is obtained by calcining nitrate, sulfate, oxalate and other salts of erbium at a temperature of 800-1000 ° C. It has the basic properties. Erbium interacts with mineral acids. Erbium salts soluble in water include chloride, nitrate, acetate and sulfate, and poorly soluble oxalate, fluoride, carbonate and phosphate."
        /// </summary>
		public static string DetailsEr => resourceProvider.GetString("DetailsEr");

		/// <summary>
        /// Gets a localized string similar to "Einstein is a radioactive silver metal. In compounds, Einsteinium exhibits oxidation states of +2 and +3. In an ordinary aqueous solution, Einsteinium exists in the most stable form in the form of Es3 + ions (gives a green color). It is characterized by relatively high volatility and can be obtained by reducing EsF3 with lithium. Many Einsteinium solid compounds have been synthesized and studied, such as Es2O3, EsCl3, EsOCl, EsBr2, EsBr3, EsI2 and EsI3."
        /// </summary>
		public static string DetailsEs => resourceProvider.GetString("DetailsEs");

		/// <summary>
        /// Gets a localized string similar to "The simple substance europium, like other lanthanides, is a soft silver-white metal that is easily oxidized in air. Europium is a typical active metal and reacts with most non-metals. It oxidizes quickly in air, and there is always an oxide film on the surface of a metal. Store in jars or ampoules under a layer of liquid paraffin or in kerosene. When heated in air to a temperature of 180 ° C, it ignites and burns with the formation of europium (III) oxide. Very active, can displace almost all metals from salt solutions."
        /// </summary>
		public static string DetailsEu => resourceProvider.GetString("DetailsEu");

		/// <summary>
        /// Gets a localized string similar to "Fluorine is the most chemically active non-metal and the strongest oxidizing agent, the lightest element from the halogen group. As a simple substance under normal conditions, fluorine is a diatomic gas (formula F2) of a pale yellow color with a pungent odor resembling ozone or chlorine. Elemental fluorine is highly toxic in high concentrations. It interacts violently with almost all substances (except fluorides in higher oxidation states and rare exceptions - fluoroplastics) and with most of them - with combustion and explosion. Forms compounds with all chemical elements. Does not react with helium, neon, argon, nitrogen, oxygen, tetrafluoromethane. At room temperature does not react with dry potassium sulfate, carbon dioxide and nitrous oxide. Without admixture, hydrogen fluoride at room temperature does not act on glass. In a fluorine atmosphere, even water and platinum burn."
        /// </summary>
		public static string DetailsF => resourceProvider.GetString("DetailsF");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is iron - a malleable silver-white metal with a high chemical reactivity. In pure oxygen, iron burns, and in a finely dispersed state it spontaneously ignites in air. Pure metal is plastic. For iron, the most characteristic oxidation states are +2 and +3. Iron (+3) most often exhibits weak oxidizing properties. Reacts with acids. Interacts with halogens when heated. Interaction with non-metals. Pure metallic iron is stable in water and in dilute alkali solutions. Iron does not dissolve in cold concentrated sulfuric and nitric acids."
        /// </summary>
		public static string DetailsFe => resourceProvider.GetString("DetailsFe");

		/// <summary>
        /// Gets a localized string similar to "Flerovium is the 114th chemical element of the 14th group, 7th period of the periodic system, atomic number 114, of the known isotopes the most stable is 289Fl with an atomic mass of 289,190 (4) a. E. m. The element is highly radioactive."
        /// </summary>
		public static string DetailsFl => resourceProvider.GetString("DetailsFl");

		/// <summary>
        /// Gets a localized string similar to "Fermium is a radioactive transuranic chemical element. In solutions, Fermium, like other heavy actinides, for example, Einsteinium and Mendelium, exhibits an oxidation state of +3, but under very reducing conditions it can also be obtained as Fm2 +"
        /// </summary>
		public static string DetailsFm => resourceProvider.GetString("DetailsFm");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of France is a radioactive alkali metal with the highest possible reducing chemical activity. Almost all compounds of France are soluble in water. Reacts violently with water, forming the strongest alkali - France hydroxide FrOH. FrH hydride and France oxide Fr2O behave similarly to similar cesium compounds, i.e. they react violently with water to form hydroxide."
        /// </summary>
		public static string DetailsFr => resourceProvider.GetString("DetailsFr");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of gallium is a soft, brittle metal of silver-white (according to other sources of light gray) color with a bluish tint. Gallium slowly reacts with hot water, displacing hydrogen from it and forming gallium hydroxide. Gallium interacts with mineral acids. The reaction products with alkalis and potassium and sodium carbonates are hydroxogallates. Gallium reacts with halogens: the reaction with chlorine and bromine occurs at room temperature, with fluorine already at −35 ° C (about 20 ° C with ignition), the interaction with iodine begins with heating. Gallium does not interact with hydrogen, carbon, nitrogen, silicon and boron."
        /// </summary>
		public static string DetailsGa => resourceProvider.GetString("DetailsGa");

		/// <summary>
        /// Gets a localized string similar to "Gadolinium, a light metal slightly oxidized in air, behaves in the same way as lanthanum and cerium with respect to acids and other reagents. In air, it slowly oxidizes at room temperature, quickly above 100 C. Reacts with water, diluted acids, when heated, with hydrogen, carbon, nitrogen, phosphorus. Does not react with alkali solutions."
        /// </summary>
		public static string DetailsGd => resourceProvider.GetString("DetailsGd");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of germanium is a typical semimetal of gray-white color, with a metallic sheen. Like silicon, it is a semiconductor. In chemical compounds, germanium usually exhibits oxidation states of +4 or +2. Under normal conditions, germanium is resistant to air and water, diluted with alkalis and acids. It slowly dissolves in hot concentrated solutions of sulfuric and nitric acids."
        /// </summary>
		public static string DetailsGe => resourceProvider.GetString("DetailsGe");

		/// <summary>
        /// Gets a localized string similar to "At standard temperature and pressure, hydrogen is a colorless, odorless, tasteless, non-toxic diatomic gas. Density is 0.08987 g / l (n.a.), boiling point −252.76 ° C, specific heat of combustion 120.9⋅106 J / kg, slightly soluble in water - 18.8 ml / l. Hydrogen is readily soluble in many metals (Ni, Pt, Pd, etc.). Liquid hydrogen exists in the temperature range from −252.76 to −259.2 ° C. It is a colorless liquid, very light. Three hydrogen isotopes are best known: protium H (the atomic nucleus is the proton), deuterium D (the nucleus consists of one proton and one neutron) and tritium T (the nucleus consists of one proton and two neutrons). Protium and deuterium are stable, while tritium turns into stable helium-3, undergoing beta decay with a period of 12.32 years."
        /// </summary>
		public static string DetailsH => resourceProvider.GetString("DetailsH");

		/// <summary>
        /// Gets a localized string similar to "Helium is the least chemically active element of the 18th group (inert gases) and, in general, the entire periodic table. It is an inert monatomic gas without color, taste or smell. Many helium compounds exist only in the gas phase. Helium forms diatomic molecules He +, HeF fluoride, HeCl chloride (molecules are formed by the action of an electric discharge or ultraviolet radiation on a mixture of helium with fluorine or chlorine)."
        /// </summary>
		public static string DetailsHe => resourceProvider.GetString("DetailsHe");

		/// <summary>
        /// Gets a localized string similar to "Hafnium is a brilliant silver-white metal, hard and refractory. In a finely divided state, it has a dark gray, almost black color; matte. Hafnium, like tantalum, is a rather inert material due to the formation of a thin passive oxide film on the surface. In general, the chemical resistance of hafnium is much greater than that of its analogue - zirconium. The best solvent for hafnium is hydrofluoric acid (HF) or a mixture of hydrofluoric and nitric acids, as well as aqua regia. At high temperatures (above 1000 K), hafnium is oxidized in air, and burns out in oxygen. Reacts with halogens."
        /// </summary>
		public static string DetailsHf => resourceProvider.GetString("DetailsHf");

		/// <summary>
        /// Gets a localized string similar to "The simple substance mercury is a transition metal, which at room temperature is a heavy silver-white liquid, the vapors of which are extremely toxic. Mercury is characterized by two oxidation states: +1 and +2. Mercury is a low-activity metal. It does not dissolve in solutions of acids that do not have oxidizing properties, but it dissolves in aqua regia. It is also difficult to dissolve in sulfuric acid when heated, with the formation of mercury sulfate. When heated to 300 ° C, mercury reacts with oxygen. Mercury also reacts with halogens (and in the cold - slowly). Mercury can also be oxidized with an alkaline solution of potassium permanganate."
        /// </summary>
		public static string DetailsHg => resourceProvider.GetString("DetailsHg");

		/// <summary>
        /// Gets a localized string similar to "Elemental holmium is a relatively soft and ductile silver-white metal. Relatively stable in dry air at room temperature and burns in air when heated. It is soluble in acids. Slowly oxidizes in air, forming Ho2O3. It interacts with acids (except for HF) to form Ho3 + salts. Reacts when heated with chlorine, bromine, nitrogen and hydrogen. Resistant to fluoride."
        /// </summary>
		public static string DetailsHo => resourceProvider.GetString("DetailsHo");

		/// <summary>
        /// Gets a localized string similar to "Hassiy - the 108th artificial radioactive chemical element of group VIII; refers to transactinoids. Presumably silver-white metal; by chemical properties, probably, an analogue of osmium (Os)."
        /// </summary>
		public static string DetailsHs => resourceProvider.GetString("DetailsHs");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is iodine under normal conditions - crystals of black-gray color with a violet metallic sheen, easily forms violet pairs with a pungent odor. Elemental iodine is highly toxic. The molecule of a simple substance is diatomic (formula I2). Chemically iodine is quite active. A fairly well-known qualitative reaction to iodine is its interaction with starch [12], in which blue staining is observed as a result of the formation of an inclusion compound. When lightly heated, iodine interacts with metals vigorously, forming iodides. Iodine reacts with hydrogen only when heated and not completely."
        /// </summary>
		public static string DetailsI => resourceProvider.GetString("DetailsI");

		/// <summary>
        /// Gets a localized string similar to "A simple indium substance is a malleable, fusible, very soft metal of silver-white color. Similar in chemical properties to aluminum and gallium, in appearance to zinc. It is stable and does not fade in dry air at room temperature, but above 800 ° C it burns with a violet-blue flame with the formation of oxide. It dissolves in sulfuric and hydrochloric acids, faster in nitric and perchloric acids, slowly reacts with hydrofluoric acid when heated, organic acids (formic, acetic, oxalic, citric) gradually dissolve indium. It does not react noticeably with alkali solutions, even boiling ones. Reacts with chlorine and bromine. When heated, reacts with iodine, sulfur (above 620 ° C), selenium, tellurium, sulfur dioxide (above 600 ° C), and phosphorus vapors. The oxidation state is from +1 to +3, the most stable are 3-valent compounds."
        /// </summary>
		public static string DetailsIn => resourceProvider.GetString("DetailsIn");

		/// <summary>
        /// Gets a localized string similar to "Iridium is a very hard, refractory, silver-white transition metal of the platinum group. Iridium is stable in air at ordinary temperature and heating; upon calcination of the powder in an oxygen stream at 600–1000 ° C it forms IrO2 in a small amount. Above 1200 ° C, it partially evaporates as IrO3. Compact iridium at temperatures up to 100 ° C does not react with all known acids and their mixtures."
        /// </summary>
		public static string DetailsIr => resourceProvider.GetString("DetailsIr");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is potassium - a soft silver metal with a characteristic luster on a freshly formed surface. Very light and fusible. Reacts with water with an explosion. It must be stored under a layer of gasoline, kerosene or silicone. Potassium at room temperature reacts with atmospheric oxygen, halogens; practically does not react with nitrogen (unlike lithium and sodium). Upon moderate heating, it reacts with hydrogen to form a hydride (KH)."
        /// </summary>
		public static string DetailsK => resourceProvider.GetString("DetailsK");

		/// <summary>
        /// Gets a localized string similar to "The simple substance krypton is an inert monatomic gas without color, taste or smell. Krypton is chemically inert. Under harsh conditions, it reacts with fluorine, forming krypton difluoride. Relatively recently, the first compound with Kr – O bonds (Kr (OTeF5) 2) was obtained."
        /// </summary>
		public static string DetailsKr => resourceProvider.GetString("DetailsKr");

		/// <summary>
        /// Gets a localized string similar to "The simple substance lanthanum is a shiny silver-white metal that belongs to rare earth elements. Lanthanum in its pure state is malleable and malleable. Metallic lanthanum has a high chemical activity. In humid air it quickly turns into the main carbonate of lanthanum. At 450 ° C, it burns in oxygen with the formation of lanthanum (III) oxide. Slowly reacts with cold water and quickly with hot, forming lanthanum (III) hydroxide. When heated, lanthanum reacts with fluorine, chlorine, bromine and iodine."
        /// </summary>
		public static string DetailsLa => resourceProvider.GetString("DetailsLa");

		/// <summary>
        /// Gets a localized string similar to "Lithium is a soft alkaline silver-white metal. Lithium is the least active alkali metal; it practically does not react with dry air at room temperature. For this reason, lithium is the only alkali metal that is not stored in kerosene. When heated in oxygen, it burns, turning into Li2O oxide. Lithium and its salts color the carmine-red flame; this is a qualitative indicator for determining lithium. Auto-ignition temperature is around 300 ° C. Combustion products irritate the mucous membrane of the nasopharynx."
        /// </summary>
		public static string DetailsLi => resourceProvider.GetString("DetailsLi");

		/// <summary>
        /// Gets a localized string similar to "Lawrence is an artificially produced chemical element of the actinoid group. The chemical properties of Lawrence should be similar to other heavy actinones, so that the most typical oxidation state of Lawrence should be +3."
        /// </summary>
		public static string DetailsLr => resourceProvider.GetString("DetailsLr");

		/// <summary>
        /// Gets a localized string similar to "Lutetium is a silver-white metal that can be easily machined. At room temperature in air, lutetium is covered with a dense oxide film, at a temperature of 400 ° C it is oxidized. When heated, interacts with halogens, sulfur and other non-metals. Lutetium reacts with inorganic acids to form salts. Upon evaporation of water-soluble salts of lutetium (chlorides, sulfates, acetates, nitrates), crystalline hydrates are formed. Lutetium hydroxide is formed by hydrolysis of its water-soluble salts."
        /// </summary>
		public static string DetailsLu => resourceProvider.GetString("DetailsLu");

		/// <summary>
        /// Gets a localized string similar to "Livermorium is the 116th chemical element, belongs to the 16th group and the 7th period of the periodic system, the atomic number is 116, the mass number of the most stable isotope is 293 (the atomic mass of this isotope is 293,204 (5) a.m. ) Artificially synthesized radioactive element, does not occur in nature."
        /// </summary>
		public static string DetailsLv => resourceProvider.GetString("DetailsLv");

		/// <summary>
        /// Gets a localized string similar to "Muscovy is a chemical element of the fifteenth group, the seventh period of the periodic system of chemical elements, atomic number is 115, the most stable is the nuclide 289Mc (half-life is estimated at 156 ms), the atomic mass of this nuclide is 289.194 (6) a. E. m. Artificially synthesized radioactive element, does not occur in nature."
        /// </summary>
		public static string DetailsMc => resourceProvider.GetString("DetailsMc");

		/// <summary>
        /// Gets a localized string similar to "By its chemical properties, Mendeleev resembles other actinides and, in particular, +3 will be the most characteristic oxidation state for it. Mendelium is easily converted into a fairly stable Md2 + ion."
        /// </summary>
		public static string DetailsMd => resourceProvider.GetString("DetailsMd");

		/// <summary>
        /// Gets a localized string similar to "Magnesium is a light, malleable metal of silver-white color, has a metallic luster. When heated in air, magnesium burns with the formation of oxide and a small amount of nitride. At the same time, a large amount of heat and light is released. Alkalis do not act on magnesium; it dissolves in acids with rapid evolution of hydrogen."
        /// </summary>
		public static string DetailsMg => resourceProvider.GetString("DetailsMg");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is manganese - a silver-white metal. Along with iron and its alloys, it belongs to ferrous metals. Typical oxidation states of manganese: 0, +2, +3, +4, +6, +7 (oxidation states of +1, +5 are uncharacteristic). Manganese absorbs hydrogen, with increasing temperature, its solubility in manganese increases. At temperatures above 1200 ° C, it interacts with nitrogen, forming nitrides of various compositions. Carbon reacts with molten manganese to form Mn3C carbides and others. It also forms silicides, borides, phosphides. In an alkaline solution, manganese is stable."
        /// </summary>
		public static string DetailsMn => resourceProvider.GetString("DetailsMn");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is molybdenum - a transition metal of light gray color. At room temperature in air, molybdenum is stable. It starts to oxidize at 400 ° C. With halogens, Mo forms a number of compounds in different oxidation states. When heating molybdenum with sulfur, molybdenum disulfide MoS2 is formed."
        /// </summary>
		public static string DetailsMo => resourceProvider.GetString("DetailsMo");

		/// <summary>
        /// Gets a localized string similar to "Meitnerium is a chemical element with atomic number 109. It belongs to the 9th group of the periodic table of chemical elements, and is located in the seventh period of the table. Synthesized artificially."
        /// </summary>
		public static string DetailsMt => resourceProvider.GetString("DetailsMt");

		/// <summary>
        /// Gets a localized string similar to "Nitrogen as a simple substance is a diatomic gas without color, taste or smell. One of the most common elements on Earth. Chemically inert, but reacts with complex compounds of transition metals. Due to its significant inertness, under normal conditions, nitrogen only reacts with lithium. When heated, it reacts with some other metals and non-metals, also forming nitrides. Of greatest practical importance is hydrogen nitride (ammonia) NH3, obtained by the interaction of hydrogen with nitrogen."
        /// </summary>
		public static string DetailsN => resourceProvider.GetString("DetailsN");

		/// <summary>
        /// Gets a localized string similar to "Sodium is a soft alkaline metal of silver-white color, in thin layers with a purple hue, plastic, soft (easy to cut with a knife), a fresh slice of sodium glistens. Qualitative determination of sodium using a flame - the bright yellow color of the spectrum. It is not found in free form, but can be obtained from various compounds. Sodium is a component of numerous minerals, including feldspars, sodalite and “rock salt” (halite, sodium chloride). The alkali metal in the air is easily oxidized to sodium oxide. To protect against oxygen, metallic sodium is stored under a layer of kerosene. Sodium reacts very violently with water, a piece of sodium placed in the water floats up, melts due to the released heat, turning into a white ball that quickly moves in different directions along the surface of the water, the reaction proceeds with the release of hydrogen, which can ignite. Like all alkali metals, sodium is a strong reducing agent and interacts energetically with many non-metals (with the exception of nitrogen, iodine, carbon, and noble gases)."
        /// </summary>
		public static string DetailsNa => resourceProvider.GetString("DetailsNa");

		/// <summary>
        /// Gets a localized string similar to "The simple substance niobium is a shiny silver-gray metal. Chemically, niobium is quite stable. It is practically not affected by hydrochloric, orthophosphoric, dilute sulfuric, nitric acids. The metal is dissolved in HF hydrofluoric acid, a mixture of HF and HNO3, concentrated solutions of caustic alkalis, and also in concentrated sulfuric acid when heated above 150 ° C. When calcined in air, it is oxidized to Nb2O5."
        /// </summary>
		public static string DetailsNb => resourceProvider.GetString("DetailsNb");

		/// <summary>
        /// Gets a localized string similar to "Neodymium is a rare-earth metal of silver-white color with a golden hue. Neodymium metal slowly dims in air and it burns easily at a temperature of about 150 ° C with the formation of neodymium (III) oxide. Neodymium is a sufficiently electropositive element, and it slowly reacts with cold water, but quickly enough with hot water with the formation of neodymium (III) hydroxide. Neodymium metal reacts actively with all halogens. Neodymium is readily soluble in dilute sulfuric acid to form solutions containing lilac Nd (III) ions."
        /// </summary>
		public static string DetailsNd => resourceProvider.GetString("DetailsNd");

		/// <summary>
        /// Gets a localized string similar to "Neon is an inert monatomic gas without color and odor. All noble gases have a complete electron shell, so they are chemically inert. The chemical inertness of neon is exceptional, in this only helium can compete with it. So far, not one of its valence compounds has been obtained."
        /// </summary>
		public static string DetailsNe => resourceProvider.GetString("DetailsNe");

		/// <summary>
        /// Gets a localized string similar to "Nichonium is a chemical element of the 13th group of the 7th period of the periodic system. The atomic number is 113. The atomic mass of the most stable known isotope, 286Nh, with a half-life of 20 s, is 286.182 (5) a. e. m. Like all superheavy metals, it is extremely radioactive."
        /// </summary>
		public static string DetailsNh => resourceProvider.GetString("DetailsNh");

		/// <summary>
        /// Gets a localized string similar to "The simple substance nickel is a ductile, malleable, transition metal of silver-white color, at ordinary temperatures in air it is covered with a thin film of oxide. Does not fade in air. Chemically inactive. Nickel forms compounds with oxidation states of +1, +2, +3, and +4. Moreover, nickel compounds with oxidation state +4 are rare and unstable. Nickel is characterized by high corrosion resistance - it is stable in air, in water, in alkalis, in a number of acids. Nickel is actively soluble in dilute nitric acid. Nickel burns only in powder form. Aqueous solutions of salts are usually colored green, and anhydrous salts are yellow or brownish-yellow."
        /// </summary>
		public static string DetailsNi => resourceProvider.GetString("DetailsNi");

		/// <summary>
        /// Gets a localized string similar to "Nobelium is an artificially produced transfermium chemical element of the actinoid group. The element does not have stable isotopes. Nobelium can have two oxidation states +2 and +3, and in chemical properties it is close to its analogue from the group of lanthanides, ytterbium."
        /// </summary>
		public static string DetailsNo => resourceProvider.GetString("DetailsNo");

		/// <summary>
        /// Gets a localized string similar to "Elemental Neptunium is a malleable, relatively soft metal with a silver sheen. This is one of the heaviest metals: in density it is second only to osmium, iridium, platinum and rhenium. It interacts with dry air slowly, becoming covered with a thin oxide film. At high temperatures in air, it quickly oxidizes to NpO2. In compounds, it has oxidation states from +2 to +7. Neptunium ions are prone to hydrolysis and complexation."
        /// </summary>
		public static string DetailsNp => resourceProvider.GetString("DetailsNp");

		/// <summary>
        /// Gets a localized string similar to "Oxygen is a chemically active non-metal. As a simple substance under normal conditions, it is a gas without color, taste and smell, the molecule of which consists of two oxygen atoms (O2 formula). Liquid oxygen has a light blue color, and solid oxygen is a light blue crystal. There are other forms of oxygen, for example, ozone - under normal conditions, a blue gas with a specific odor, the molecule of which consists of three oxygen atoms (formula O3). A strong oxidizing agent, the most active non-metal after fluorine, forms binary compounds (oxides) with all elements except helium, neon, argon. The most common oxidation state is −2. As a rule, the oxidation reaction proceeds with the release of heat and accelerates with increasing temperature. Oxygen reacts directly (under normal conditions, when heated and / or in the presence of catalysts) with all simple substances except Au and inert gases (He, Ne, Ar, Kr, Xe, Rn); reactions with halogens occur under the influence of an electric discharge or ultraviolet radiation."
        /// </summary>
		public static string DetailsO => resourceProvider.GetString("DetailsO");

		/// <summary>
        /// Gets a localized string similar to "Oganeson is a chemical element of the eighteenth group, the seventh period of the periodic system of chemical elements, the atomic number is 118. The most stable (and the only known for 2016) is the nuclide 294Og, whose half-life is estimated at 1 ms, the atomic mass - at 294,214 (5) a . E. m. Artificially synthesized radioactive element, does not occur in nature."
        /// </summary>
		public static string DetailsOg => resourceProvider.GetString("DetailsOg");

		/// <summary>
        /// Gets a localized string similar to "Under standard conditions, osmium is a shiny silver-white with a bluish tint. Osmium is a hard but brittle metal with a very high specific gravity, retaining its luster even at high temperatures. Osmium powder reacts with oxygen, halogens, sulfur vapor, selenium, tellurium, phosphorus, nitric and sulfuric acids when heated. Compact osmium does not interact with either acids or alkalis, but forms water-soluble osmates with alkali melts. Slowly reacts with nitric acid and aqua regia, reacts with molten alkalis in the presence of oxidizing agents (potassium nitrate or chlorate), with molten sodium peroxide. In compounds, it exhibits oxidation states from −2 to +8, of which the most common are +2, +3, +4 and +8."
        /// </summary>
		public static string DetailsOs => resourceProvider.GetString("DetailsOs");

		/// <summary>
        /// Gets a localized string similar to "Traditionally, three modifications of phosphorus are distinguished: white, red, black. White phosphorus is a white substance (due to impurities it may have a yellowish tint). In appearance, it is very similar to refined wax or paraffin, it is easily cut with a knife and deformed from small efforts. Crude white phosphorus is commonly referred to as yellow phosphorus. Highly toxic, flammable crystalline substance from light yellow to dark brown in color. White phosphorus is very active, in the process of transition to red and black phosphorus, chemical activity decreases. White phosphorus in air during oxidation by atmospheric oxygen at room temperature emits visible light, the glow is due to the photoemission reaction of phosphorus oxidation. Phosphorus is easily oxidized by oxygen. It interacts with many simple substances - halogens, sulfur, some metals, as well as water, alkalis, metals."
        /// </summary>
		public static string DetailsP => resourceProvider.GetString("DetailsP");

		/// <summary>
        /// Gets a localized string similar to "Protactinium is a dense silver-gray actinoid metal that readily reacts with oxygen, water vapor, and inorganic acids. Protactinium in air is usually coated with a thin film of monoxide. It readily reacts with hydrogen at 250–300 ° С, forming PaH3 hydride. With iodine forms volatile iodides of complex composition."
        /// </summary>
		public static string DetailsPa => resourceProvider.GetString("DetailsPa");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is lead - a malleable, relatively low-melting heavy metal of silver-white color with a bluish tint. Lead is toxic. The metal is soft, cut with a knife, easily scratched with a fingernail. Salts of divalent lead react with alkalis to form almost insoluble lead hydroxide. With an excess of alkali, the hydroxide dissolves. Reacts with alkalis and acids."
        /// </summary>
		public static string DetailsPb => resourceProvider.GetString("DetailsPb");

		/// <summary>
        /// Gets a localized string similar to "The simple substance palladium under normal conditions is a silver-white ductile metal. Palladium is the most chemically active of platinum metals. Does not react with water, diluted acids, alkalis, ammonia solution. Reacts with hot concentrated sulfuric and nitric acids, unlike other platinum metals. It can be converted into solution by anodic dissolution in hydrochloric acid. At room temperature it reacts with aqua regia, with moist chlorine and bromine. When heated, reacts with fluorine, sulfur, selenium, tellurium, arsenic and silicon. It is oxidized during fusion with potassium hydrosulfate KHSO4, and also interacts with the melt of sodium peroxide."
        /// </summary>
		public static string DetailsPd => resourceProvider.GetString("DetailsPd");

		/// <summary>
        /// Gets a localized string similar to "Promethium is lanthanide. Promethium shows only one stable oxidation state of +3. Even though some compounds have been synthesized, they are not fully understood; in general, they tend to be pink or red."
        /// </summary>
		public static string DetailsPm => resourceProvider.GetString("DetailsPm");

		/// <summary>
        /// Gets a localized string similar to "Under normal conditions, polonium is a soft silver-white radioactive metal. Metallic polonium is rapidly oxidized in air. Under the action of acids, it passes into solution with the formation of pink Po2 + cations. Polonium is the only chemical element that, at low temperature, forms a monoatomic simple cubic crystal lattice."
        /// </summary>
		public static string DetailsPo => resourceProvider.GetString("DetailsPo");

		/// <summary>
        /// Gets a localized string similar to "Praseodymium is a silver metal. In air, praseodymium is slowly oxidized; when heated, it ignites, forming oxide Pr6O11. Annealing this oxide in the atmosphere of H or CH4, Pr2O3 oxide is formed. If the oxidation of praseodymium is carried out in an O2 atmosphere at a pressure of 10 MPa, praseodymium dioxide PrO2 is formed. Pr oxides are basic; the reactions of these oxides with mineral acids give rise to the corresponding salts of Pr3 +. When heated, Pr reacts with halogens to form trihalides such as PrCl3 and PrF3."
        /// </summary>
		public static string DetailsPr => resourceProvider.GetString("DetailsPr");

		/// <summary>
        /// Gets a localized string similar to "Platinum is a brilliant noble metal of silver-white color. At room temperature it reacts with aqua regia. Platinum slowly dissolves in hot concentrated sulfuric acid and liquid bromine. It does not interact with other mineral and organic acids. When heated, reacts with alkalis and sodium peroxide, halogens. When heated, platinum reacts with sulfur, selenium, tellurium, carbon and silicon. When heated, platinum reacts with oxygen to produce volatile oxides. Fluorination of platinum at normal pressure and a temperature of 350-400 ° C gives platinum (IV) fluoride."
        /// </summary>
		public static string DetailsPt => resourceProvider.GetString("DetailsPt");

		/// <summary>
        /// Gets a localized string similar to "Plutonium is a heavy brittle highly toxic radioactive metal of silver-white color. It oxidizes in air, changing its color first to bronze, then to the blue color of the hardened metal, and then turns to dull black or green due to the formation of a loose oxide coating. Plutonium reacts with acids, oxygen and their vapors, but not with alkalis. Quickly soluble in hydrogen chloride, hydrogen iodide, hydrogen bromide, 72% perchloric acid, 85% phosphoric acid, concentrated CCl3COOH, sulfamic acid and boiling concentrated nitric acid."
        /// </summary>
		public static string DetailsPu => resourceProvider.GetString("DetailsPu");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is radium - a shiny silver-white metal that quickly fades in the air. Reacts with water. The usual oxidation state is +2. Radium hydroxide Ra (OH) 2 is a strong, corrosive base. Due to the strong radioactivity, all compounds of radium glow with a bluish light."
        /// </summary>
		public static string DetailsRa => resourceProvider.GetString("DetailsRa");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of rubidium is a soft fusible alkaline metal of silver-white color, having a metallic luster on a fresh cut. Alkaline metal, extremely unstable in air (reacts with air in the presence of traces of water with ignition). It forms all types of salts - mostly soluble. Rubidium hydroxide RbOH is a very aggressive substance to glass and other structural and container materials, and molten RbOH destroys most metals."
        /// </summary>
		public static string DetailsRb => resourceProvider.GetString("DetailsRb");

		/// <summary>
        /// Gets a localized string similar to "Under standard conditions, rhenium is a dense silver-white transition metal. Compact rhenium is stable in air at ordinary temperatures. At temperatures above 300 ° C, metal oxidation is observed; oxidation occurs intensively at temperatures above 600 ° C. When heated, rhenium interacts with fluorine, chlorine and bromine. Rhenium is almost insoluble in hydrochloric and hydrofluoric acids and only weakly reacts with sulfuric acid even when heated, but it is easily soluble in nitric acid. Rhenium interacts with aqueous solutions of hydrogen peroxide to form rhenium acid. Rhenium is the only refractory metal that does not form carbides."
        /// </summary>
		public static string DetailsRe => resourceProvider.GetString("DetailsRe");

		/// <summary>
        /// Gets a localized string similar to "Rutherfordium is a highly radioactive artificially synthesized element; the half-life of the most stable known isotope (267Rf) is about 1.3 hours. In the oxidation state +4, it forms the RfCl4 and RfBr4 halides volatile at temperatures of 250–300 ° C."
        /// </summary>
		public static string DetailsRf => resourceProvider.GetString("DetailsRf");

		/// <summary>
        /// Gets a localized string similar to "X-ray is an artificially synthesized chemical element of the 11th group, the seventh period of the periodic system of chemical elements of D. I. Mendeleev, with atomic number 111. A simple substance of x-ray is a transition metal. The most long-lived (half-life of 2.1 minutes) known isotope has a mass number of 282."
        /// </summary>
		public static string DetailsRg => resourceProvider.GetString("DetailsRg");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is rhodium - a solid transition metal of silver-white color. Rhodium is a noble metal; in chemical resistance in most corrosive environments it surpasses platinum. Rhodium metal dissolves in aqua regia by boiling, in the KHSO4 melt, in concentrated sulfuric acid when heated, and also electrochemically, anodically in a mixture of hydrogen peroxide and sulfuric acid. Rhodium is characterized by high chemical resistance. With non-metals, it interacts only at a red-hot temperature. Finely ground rhodium slowly oxidizes only at temperatures above 600 ° C. When heated, rhodium slowly interacts with concentrated sulfuric acid, a solution of sodium hypochlorite and hydrogen bromide."
        /// </summary>
		public static string DetailsRh => resourceProvider.GetString("DetailsRh");

		/// <summary>
        /// Gets a localized string similar to "The simple substance radon under normal conditions is a colorless inert gas; is radioactive, does not have stable isotopes, can be a danger to health and life. Chemically, radon is the most active of the noble gases. Radon forms chemical compounds with fluorine. In addition to fluorine, radon can form binary compounds with oxygen."
        /// </summary>
		public static string DetailsRn => resourceProvider.GetString("DetailsRn");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of ruthenium is a silver transition metal. Ruthenium is a very inert metal. Ruthenium is insoluble in acids and aqua regia (mixtures of HCl and HNO3). At the same time, ruthenium reacts with chlorine above 400 ° C (RuCl3 is formed) and with a mixture of alkali and nitrate during fusion (ruthenates, for example, Na2RuO4, are formed). Ruthenium is capable of producing compounds corresponding to different degrees of oxidation."
        /// </summary>
		public static string DetailsRu => resourceProvider.GetString("DetailsRu");

		/// <summary>
        /// Gets a localized string similar to "Crystalline sulfur is a fragile yellow substance. Sulfur melting is accompanied by a noticeable increase in volume (approximately 15%). The molten sulfur is a yellow, easily mobile liquid, which, above 160 ° C, turns into a very viscous dark brown mass. Sulfur burns in the air, forming sulfur dioxide - a colorless gas with a pungent odor. The reducing properties of sulfur are manifested in reactions of sulfur and with other non-metals, however, at room temperature, sulfur reacts only with fluorine. When heated, sulfur reacts with hydrogen, carbon, and silicon. When heated, sulfur interacts with many metals, often very violently. Sometimes a mixture of metal with sulfur lights up when set on fire. In this interaction, sulfides are formed."
        /// </summary>
		public static string DetailsS => resourceProvider.GetString("DetailsS");

		/// <summary>
        /// Gets a localized string similar to "A simple antimony substance is a semimetal of silver-white color with a bluish tint, a coarse-grained structure. With many metals it forms intermetallic compounds - antimonides. The main valence states in compounds: III and V. Oxidized concentrated acids actively interact with antimony. Antimony is soluble in Tsarskaya Vodka. Antimony readily reacts with halogens."
        /// </summary>
		public static string DetailsSb => resourceProvider.GetString("DetailsSb");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of scandium is a light silver metal with a characteristic yellow tint. The chemical properties of scandium are similar to those of aluminum. In most compounds, scandium exhibits an oxidation state of +3. Compact metal in air is coated from the surface with an oxide film. When heated to red heat, it reacts with fluorine, oxygen, nitrogen, carbon, phosphorus. At room temperature it reacts with chlorine, bromine and iodine. Reacts with diluted strong acids; concentrated oxidizing acids and HF is passivated. Reacts with concentrated alkali solutions."
        /// </summary>
		public static string DetailsSc => resourceProvider.GetString("DetailsSc");

		/// <summary>
        /// Gets a localized string similar to "Selenium is a fragile non-metallic shining non-metallic gray color (unstable forms - various shades of red). A simple substance selenium is much less chemically active than sulfur. So, unlike sulfur, selenium is not able to burn on air independently. Selenium can only be oxidized with additional heating, in which it slowly burns with a blue flame, turning into SeO2 dioxide. With alkali metals, selenium reacts (very violently), only being molten. At room temperature it reacts with halogens and alkalis."
        /// </summary>
		public static string DetailsSe => resourceProvider.GetString("DetailsSe");

		/// <summary>
        /// Gets a localized string similar to "Seaborgium is a short-lived radioactive element. Seaborgium was obtained artificially by nuclear fusion. A large number of particles in the nucleus makes the atom unstable and causes splitting into smaller fragments immediately after receipt."
        /// </summary>
		public static string DetailsSg => resourceProvider.GetString("DetailsSg");

		/// <summary>
        /// Gets a localized string similar to "Silicon is brittle, only when heated above 800 ° C it becomes a plastic substance. Under normal conditions, silicon is chemically inactive and actively reacts only with gaseous fluorine, and volatile silicon tetrafluoride SiF4 is formed. When heated to temperatures above 400-500 ° C, silicon reacts with chlorine, bromine and iodine to form the corresponding readily volatile tetrahalides SiHAl4 and, possibly, more complex halides. When heated to temperatures above 400-500 ° C, silicon reacts with oxygen to form SiO2."
        /// </summary>
		public static string DetailsSi => resourceProvider.GetString("DetailsSi");

		/// <summary>
        /// Gets a localized string similar to "The simple substance, samarium, is a white solid metal from the lanthanide group. This element, as a rule, acts as a reducing agent, exhibiting oxidation states characteristic of lanthanides, that is, +2 and +3. Samaria is a highly active metal. It slowly oxidizes in air, first being covered with a dark film of trivalent Sm2O3 oxide and then completely scattering into a yellow tinted powder. Samarium is able to react with nitrogen (forming nitride), carbon (forming carbides), chalcogenes (forming mono and trivalent sulfides, selenides, tellurides), hydrogen (forming hydrides), silicon (forming silicides), boron (forming borides), with phosphorus (phosphides), arsenic (arsenides), antimony (antimonides), bismuth (bismuthides) and all halogens, forming trivalent compounds (fluorides, chlorides, bromides, iodides). Samaria is soluble in acids."
        /// </summary>
		public static string DetailsSm => resourceProvider.GetString("DetailsSm");

		/// <summary>
        /// Gets a localized string similar to "Under normal conditions, a simple substance is tin - a ductile, malleable and fusible shiny metal of silver-white color. At room temperature, tin, like a German group neighbor, is resistant to air or water. When heated, tin reacts with most non-metals. In this case, compounds are formed in the oxidation state +4, which is more characteristic of tin than +2. It is soluble in dilute acids. Tin reacts with concentrated hydrochloric acid."
        /// </summary>
		public static string DetailsSn => resourceProvider.GetString("DetailsSn");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of strontium is a soft, malleable and ductile alkaline earth metal of silver-white color. It has high chemical activity, quickly reacts with moisture and oxygen in the air, becoming covered with a yellow oxide film. Strontium always exhibits an oxidation state of +2 in its compounds. It interacts with acids, displaces heavy metals from their salts. With concentrated acids (H2SO4, HNO3) reacts weakly. When heated in air, it ignites; powdered strontium in air is prone to self-ignition. Vigorously reacts with non-metals - sulfur, phosphorus, halogens. Interacts with hydrogen (above 200 ° C), nitrogen (above 400 ° C). Practically does not react with alkalis. At high temperatures, reacts with CO2 to form carbide."
        /// </summary>
		public static string DetailsSr => resourceProvider.GetString("DetailsSr");

		/// <summary>
        /// Gets a localized string similar to "Under standard conditions, tantalum is a shiny silver-white metal (with a weak lead (bluish) tint due to the formation of a dense oxide film). Under normal conditions, tantalum is inactive, it oxidizes in air only at temperatures above 280 ° C, being covered with an Ta2O5 oxide film; reacts with halogens at temperatures above 250 ° C. When heated, reacts with C, B, Si, P, Se, Te, H2O, CO, CO2, NO, HCl, H2S. Chemically pure tantalum is extremely resistant to liquid alkali metals, most inorganic and organic acids. In terms of chemical resistance to reagents, tantalum is similar to glass. Tantalum is insoluble in acids and their mixtures, except for a mixture of hydrofluoric and nitric acids; even aqua regia does not dissolve it. The reaction with hydrofluoric acid occurs only with metal dust and is accompanied by an explosion. Very resistant to sulfuric acid of any concentration and temperature."
        /// </summary>
		public static string DetailsTa => resourceProvider.GetString("DetailsTa");

		/// <summary>
        /// Gets a localized string similar to "Terbium is a silver-white metal. Compact terbium is stable in air at room temperature. It displaces hydrogen from boiling water to form Tb (OH) 3 hydroxide. Terbium reacts with mineral acids to form terbium (III) salts. By calcining terbium compounds in air at 800-1000 ° C, Tb4O7 oxide is obtained. When it is heated to 300 ° C, terbium dioxide TbO2 is formed. By careful thermal decomposition of Tb (OH) 3, Tb2O3 is obtained."
        /// </summary>
		public static string DetailsTb => resourceProvider.GetString("DetailsTb");

		/// <summary>
        /// Gets a localized string similar to "A simple substance of technetium is a silver gray radioactive transition metal. The lightest element that does not have stable isotopes. In compounds, it exhibits oxidation states from -1 to +7. Upon the interaction of Technetium or its compounds with oxygen, the oxides Tc2O7 and TcO2 are formed, with chlorine and fluorine the halides TcX6, TcX5, TcX4 are possible, the formation of oxyhalides, for example TcO3X (where X is halogen), with sulfur the sulfides Tc2S7 and TcS2."
        /// </summary>
		public static string DetailsTc => resourceProvider.GetString("DetailsTc");

		/// <summary>
        /// Gets a localized string similar to "Tellurium is a fragile silver-white substance with a metallic sheen. In thin layers the lumen is red-brown, in pairs - golden yellow. When heated, it acquires plasticity. Tellurium exhibits oxidation states of –2 in chemical compounds; +2; +4; +6. It is soluble in alkalis, amenable to the action of nitric and sulfuric acids, but it is slightly soluble in dilute hydrochloric acid. Metallic tellurium begins to react with water at 100 ° C. In the molten state, tellurium is rather inert, therefore, graphite and quartz are used as container materials for its melting. Tellurium forms a compound with hydrogen when heated, easily reacts with halogens, interacts with sulfur, phosphorus and metals. When reacted with concentrated sulfuric acid, it forms sulfite."
        /// </summary>
		public static string DetailsTe => resourceProvider.GetString("DetailsTe");

		/// <summary>
        /// Gets a localized string similar to "Thorium is a silver-white shiny, soft, malleable metal. The metal is pyrophoric, therefore it is recommended to store thorium powder in kerosene. In air, pure metal slowly dims and darkens, when heated, ignites and burns with a bright white flame with the formation of dioxide. Thorium is capable of oxidation states of +4, +3, and +2. The most stable +4. The oxidation states +3 and +2 of thorium are manifested in halides with Br and I obtained by the action of strong reducing agents in the solid phase. Thorium is poorly soluble in basic acids. Does not react with caustic alkalis. When heated, it interacts with hydrogen, halogens, sulfur, nitrogen, silicon, aluminum and a number of other elements."
        /// </summary>
		public static string DetailsTh => resourceProvider.GetString("DetailsTh");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is titanium - a lightweight, durable silver-white metal. It has high corrosion resistance. Titanium dust tends to explode. Flash point - 400 ° C. Titanium shavings are flammable. Titanium is resistant to dilute solutions of many acids and alkalis (except for HF, H3PO4 and concentrated H2SO4). Titanium is resistant to wet chlorine and aqueous chlorine solutions. It easily reacts even with weak acids in the presence of complexing agents. When heated in air to 1200 ° C, Ti lights up with a bright white flame. When heated, Ti interacts with halogens. Titanium is the only element that burns in an atmosphere of nitrogen."
        /// </summary>
		public static string DetailsTi => resourceProvider.GetString("DetailsTi");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is thallium - a soft, extremely toxic metal of silver-white color with a grayish-bluish tint. Reacts with non-metals: with halogens and oxygen at room temperature, with sulfur, selenium, tellurium, phosphorus - when heated. It fuses with arsenic without the formation of a compound. It does not react with hydrogen, nitrogen, carbon, silicon, boron, and also with ammonia and dry carbon dioxide. Easily soluble in nitric acid, worse in sulfuric acid. Does not react with alkalis. In compounds, it exhibits oxidation states of +1 and +3."
        /// </summary>
		public static string DetailsTl => resourceProvider.GetString("DetailsTl");

		/// <summary>
        /// Gets a localized string similar to "The simple substance thulium is an easily processed silver-white metal. Solutions of thulium salts are colored green. Reacts with acids."
        /// </summary>
		public static string DetailsTm => resourceProvider.GetString("DetailsTm");

		/// <summary>
        /// Gets a localized string similar to "Tennessee is a chemical element of the seventeenth group, the seventh period of the periodic system of chemical elements, denoted by the symbol Ts and having a charge number of 117. It is extremely radioactive. The half-life of the more stable of the two known isotopes, 294Ts, is about 78 milliseconds, the atomic mass of this isotope is 294,210 (5) amu. Formally refers to halogens, but its chemical properties have not yet been studied and may differ from those characteristic of this group of elements. Ununsepti was discovered by the last of the elements of the seventh period of the periodic table."
        /// </summary>
		public static string DetailsTs => resourceProvider.GetString("DetailsTs");

		/// <summary>
        /// Gets a localized string similar to "Uranium is a very heavy, silver-white glossy metal. In its pure form, it is slightly softer than steel, malleable, flexible, has small paramagnetic properties. Uranium can exhibit oxidation states from +3 to +6. Chemically, uranium is very active. It is rapidly oxidized in air and covered with an iridescent oxide film. Fine uranium powder spontaneously ignites in air; it ignites at a temperature of 150-175 ° C, forming U3O8. It interacts with water, displacing hydrogen, slowly at low temperature, and quickly at high. With strong shaking, the metal particles of uranium begin to glow."
        /// </summary>
		public static string DetailsU => resourceProvider.GetString("DetailsU");

		/// <summary>
        /// Gets a localized string similar to "A simple substance is vanadium - a plastic metal of silver-gray color, in appearance it looks like steel. Chemically, vanadium is quite inert. It has good resistance to corrosion, resistant to the action of sea water, dilute solutions of hydrochloric, nitric and sulfuric acids, alkalis. Vanadium forms several oxides with oxygen: VO, V2O3, VO2, V2O5. Orange V2O5 is an acid oxide, dark blue VO2 is amphoteric, the remaining vanadium oxides are basic. Reacts with acids."
        /// </summary>
		public static string DetailsV => resourceProvider.GetString("DetailsV");

		/// <summary>
        /// Gets a localized string similar to "Under normal conditions, tungsten is a hard shiny silver-gray transition metal, having the highest proven melting and boiling points. It exhibits a valence of 2 to 6. The most stable is 6-valence tungsten. 3- and 2-valent compounds of tungsten are unstable and have no practical value. Tungsten has high corrosion resistance: at room temperature it does not change in air; at a temperature of red heat, it is slowly oxidized to tungsten (VI) oxide. In nitric acid and aqua regia, it is oxidized from the surface. It is soluble in hydrogen peroxide. Easily soluble in a mixture of nitric and hydrofluoric acids. Reacts with molten alkalis in the presence of oxidizing agents."
        /// </summary>
		public static string DetailsW => resourceProvider.GetString("DetailsW");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is xenon - a noble monatomic gas without color, taste or smell. Xenon was the first inert gas for which real chemical compounds were obtained. Xenon reacts with fluorine. Examples of compounds can be xenon difluoride, xenon tetrafluoride, xenon hexafluoride, xenon trioxide, xenon acid and others."
        /// </summary>
		public static string DetailsXe => resourceProvider.GetString("DetailsXe");

		/// <summary>
        /// Gets a localized string similar to "The simple substance of yttrium is a light gray metal. In air, yttrium is coated with a dense protective oxide film. At 370–425 ° C, a dense black oxide film forms. Intensive oxidation starts at 750 ° C. The compact metal is oxidized by atmospheric oxygen in boiling water, reacts with mineral acids, acetic acid, does not react with hydrogen fluoride. When heated, yttrium interacts with halogens, hydrogen, nitrogen, sulfur, and phosphorus."
        /// </summary>
		public static string DetailsY => resourceProvider.GetString("DetailsY");

		/// <summary>
        /// Gets a localized string similar to "The simple substance ytterbium is a light gray metal. Ytterbium reacts with water to form hydroxide. Reacts with acids. It burns in the air. Reacts with sulfur to form yellow sulfide."
        /// </summary>
		public static string DetailsYb => resourceProvider.GetString("DetailsYb");

		/// <summary>
        /// Gets a localized string similar to "The simple substance zinc, under normal conditions, is a brittle transition metal of a bluish-white color (tarnishes in air, becoming covered with a thin layer of zinc oxide). In air, zinc is coated with a thin film of ZnO oxide. With strong heating, it burns with the formation of amphoteric white oxide ZnO. Zinc oxide reacts with both acid solutions and alkalis. When heated, zinc reacts with halogens to form ZnHal2 halides. With phosphorus, zinc forms phosphides Zn3P2 and ZnP2. With sulfur and its analogues - selenium and tellurium - various chalcogenides, ZnS, ZnSe, ZnSe2 and ZnTe. Zinc does not directly react with hydrogen, nitrogen, carbon, silicon and boron."
        /// </summary>
		public static string DetailsZn => resourceProvider.GetString("DetailsZn");

		/// <summary>
        /// Gets a localized string similar to "The simple substance is zirconium - a shiny silver-gray metal. It has high ductility and corrosion resistance. Powdered metal is pyrophoric - it can ignite in air at ordinary temperature. Zirconium actively absorbs hydrogen even at 300 ° C. Zirconium interacts with carbon at temperatures above 900 ° C to form ZrC carbide. Zirconium reacts with fluorine at ordinary temperature, and with chlorine, bromine and iodine at temperatures above 200 ° C, forming higher ZrHal4 halides (where Hal is halogen). Zirconium is stable in water and water vapor up to 300 ° C."
        /// </summary>
		public static string DetailsZr => resourceProvider.GetString("DetailsZr");

		/// <summary>
        /// Gets a localized string similar to "Drag & Drop"
        /// </summary>
		public static string DragAndDrop => resourceProvider.GetString("DragAndDrop");

		/// <summary>
        /// Gets a localized string similar to "Darmstadtium"
        /// </summary>
		public static string Ds => resourceProvider.GetString("Ds");

		/// <summary>
        /// Gets a localized string similar to "Dysprosium"
        /// </summary>
		public static string Dy => resourceProvider.GetString("Dy");

		/// <summary>
        /// Gets a localized string similar to "Electrons distribution"
        /// </summary>
		public static string ElectronsDistribution => resourceProvider.GetString("ElectronsDistribution");

		/// <summary>
        /// Gets a localized string similar to "Elements"
        /// </summary>
		public static string Elements => resourceProvider.GetString("Elements");

		/// <summary>
        /// Gets a localized string similar to ""
        /// </summary>
		public static string EmptyValue => resourceProvider.GetString("EmptyValue");

		/// <summary>
        /// Gets a localized string similar to "English"
        /// </summary>
		public static string English => resourceProvider.GetString("English");

		/// <summary>
        /// Gets a localized string similar to "Erbium"
        /// </summary>
		public static string Er => resourceProvider.GetString("Er");

		/// <summary>
        /// Gets a localized string similar to "Einsteinium"
        /// </summary>
		public static string Es => resourceProvider.GetString("Es");

		/// <summary>
        /// Gets a localized string similar to "Europium"
        /// </summary>
		public static string Eu => resourceProvider.GetString("Eu");

		/// <summary>
        /// Gets a localized string similar to "Fluorine"
        /// </summary>
		public static string F => resourceProvider.GetString("F");

		/// <summary>
        /// Gets a localized string similar to "Iron"
        /// </summary>
		public static string Fe => resourceProvider.GetString("Fe");

		/// <summary>
        /// Gets a localized string similar to "Flerovium"
        /// </summary>
		public static string Fl => resourceProvider.GetString("Fl");

		/// <summary>
        /// Gets a localized string similar to "Fermium"
        /// </summary>
		public static string Fm => resourceProvider.GetString("Fm");

		/// <summary>
        /// Gets a localized string similar to "Francium"
        /// </summary>
		public static string Fr => resourceProvider.GetString("Fr");

		/// <summary>
        /// Gets a localized string similar to "from "Elements""
        /// </summary>
		public static string FromElements => resourceProvider.GetString("FromElements");

		/// <summary>
        /// Gets a localized string similar to "Gallium"
        /// </summary>
		public static string Ga => resourceProvider.GetString("Ga");

		/// <summary>
        /// Gets a localized string similar to "Gadolinium"
        /// </summary>
		public static string Gd => resourceProvider.GetString("Gd");

		/// <summary>
        /// Gets a localized string similar to "Germanium"
        /// </summary>
		public static string Ge => resourceProvider.GetString("Ge");

		/// <summary>
        /// Gets a localized string similar to "Hydrogen"
        /// </summary>
		public static string H => resourceProvider.GetString("H");

		/// <summary>
        /// Gets a localized string similar to "Helium"
        /// </summary>
		public static string He => resourceProvider.GetString("He");

		/// <summary>
        /// Gets a localized string similar to "Hafnium"
        /// </summary>
		public static string Hf => resourceProvider.GetString("Hf");

		/// <summary>
        /// Gets a localized string similar to "Mercury"
        /// </summary>
		public static string Hg => resourceProvider.GetString("Hg");

		/// <summary>
        /// Gets a localized string similar to "Holmium"
        /// </summary>
		public static string Ho => resourceProvider.GetString("Ho");

		/// <summary>
        /// Gets a localized string similar to "Hassium"
        /// </summary>
		public static string Hs => resourceProvider.GetString("Hs");

		/// <summary>
        /// Gets a localized string similar to "Iodine"
        /// </summary>
		public static string I => resourceProvider.GetString("I");

		/// <summary>
        /// Gets a localized string similar to "Images:"
        /// </summary>
		public static string Images => resourceProvider.GetString("Images");

		/// <summary>
        /// Gets a localized string similar to "Indium"
        /// </summary>
		public static string In => resourceProvider.GetString("In");

		/// <summary>
        /// Gets a localized string similar to "Iridium"
        /// </summary>
		public static string Ir => resourceProvider.GetString("Ir");

		/// <summary>
        /// Gets a localized string similar to "Potassium"
        /// </summary>
		public static string K => resourceProvider.GetString("K");

		/// <summary>
        /// Gets a localized string similar to "Kernel charge:"
        /// </summary>
		public static string KernelCharge => resourceProvider.GetString("KernelCharge");

		/// <summary>
        /// Gets a localized string similar to "Krypton"
        /// </summary>
		public static string Kr => resourceProvider.GetString("Kr");

		/// <summary>
        /// Gets a localized string similar to "Lanthanum"
        /// </summary>
		public static string La => resourceProvider.GetString("La");

		/// <summary>
        /// Gets a localized string similar to "Lithium"
        /// </summary>
		public static string Li => resourceProvider.GetString("Li");

		/// <summary>
        /// Gets a localized string similar to "Lawrencium"
        /// </summary>
		public static string Lr => resourceProvider.GetString("Lr");

		/// <summary>
        /// Gets a localized string similar to "Lutetium"
        /// </summary>
		public static string Lu => resourceProvider.GetString("Lu");

		/// <summary>
        /// Gets a localized string similar to "Livermorium"
        /// </summary>
		public static string Lv => resourceProvider.GetString("Lv");

		/// <summary>
        /// Gets a localized string similar to "Moscovium"
        /// </summary>
		public static string Mc => resourceProvider.GetString("Mc");

		/// <summary>
        /// Gets a localized string similar to "Mendelevium"
        /// </summary>
		public static string Md => resourceProvider.GetString("Md");

		/// <summary>
        /// Gets a localized string similar to "Menu"
        /// </summary>
		public static string Menu => resourceProvider.GetString("Menu");

		/// <summary>
        /// Gets a localized string similar to "Magnesium"
        /// </summary>
		public static string Mg => resourceProvider.GetString("Mg");

		/// <summary>
        /// Gets a localized string similar to "Manganese"
        /// </summary>
		public static string Mn => resourceProvider.GetString("Mn");

		/// <summary>
        /// Gets a localized string similar to "Niobium"
        /// </summary>
		public static string Mo => resourceProvider.GetString("Mo");

		/// <summary>
        /// Gets a localized string similar to "Molecule model"
        /// </summary>
		public static string MoleculeModel => resourceProvider.GetString("MoleculeModel");

		/// <summary>
        /// Gets a localized string similar to "Meitnerium"
        /// </summary>
		public static string Mt => resourceProvider.GetString("Mt");

		/// <summary>
        /// Gets a localized string similar to "Nitrogen"
        /// </summary>
		public static string N => resourceProvider.GetString("N");

		/// <summary>
        /// Gets a localized string similar to "Sodium"
        /// </summary>
		public static string Na => resourceProvider.GetString("Na");

		/// <summary>
        /// Gets a localized string similar to "Name:"
        /// </summary>
		public static string Name => resourceProvider.GetString("Name");

		/// <summary>
        /// Gets a localized string similar to "Niobium"
        /// </summary>
		public static string Nb => resourceProvider.GetString("Nb");

		/// <summary>
        /// Gets a localized string similar to "Neodymium"
        /// </summary>
		public static string Nd => resourceProvider.GetString("Nd");

		/// <summary>
        /// Gets a localized string similar to "Neon"
        /// </summary>
		public static string Ne => resourceProvider.GetString("Ne");

		/// <summary>
        /// Gets a localized string similar to "Nihonium"
        /// </summary>
		public static string Nh => resourceProvider.GetString("Nh");

		/// <summary>
        /// Gets a localized string similar to "Nickel"
        /// </summary>
		public static string Ni => resourceProvider.GetString("Ni");

		/// <summary>
        /// Gets a localized string similar to "Nobelium"
        /// </summary>
		public static string No => resourceProvider.GetString("No");

		/// <summary>
        /// Gets a localized string similar to "Neptunium"
        /// </summary>
		public static string Np => resourceProvider.GetString("Np");

		/// <summary>
        /// Gets a localized string similar to "Oxygen"
        /// </summary>
		public static string O => resourceProvider.GetString("O");

		/// <summary>
        /// Gets a localized string similar to "Oganesson"
        /// </summary>
		public static string Og => resourceProvider.GetString("Og");

		/// <summary>
        /// Gets a localized string similar to "Osmium"
        /// </summary>
		public static string Os => resourceProvider.GetString("Os");

		/// <summary>
        /// Gets a localized string similar to "Oxide"
        /// </summary>
		public static string Oxide => resourceProvider.GetString("Oxide");

		/// <summary>
        /// Gets a localized string similar to "Phosphorus"
        /// </summary>
		public static string P => resourceProvider.GetString("P");

		/// <summary>
        /// Gets a localized string similar to "Protactinium"
        /// </summary>
		public static string Pa => resourceProvider.GetString("Pa");

		/// <summary>
        /// Gets a localized string similar to "Lead"
        /// </summary>
		public static string Pb => resourceProvider.GetString("Pb");

		/// <summary>
        /// Gets a localized string similar to "Palladium"
        /// </summary>
		public static string Pd => resourceProvider.GetString("Pd");

		/// <summary>
        /// Gets a localized string similar to "Period:"
        /// </summary>
		public static string Period => resourceProvider.GetString("Period");

		/// <summary>
        /// Gets a localized string similar to "Periodic Table"
        /// </summary>
		public static string PeriodicTable => resourceProvider.GetString("PeriodicTable");

		/// <summary>
        /// Gets a localized string similar to "Promethium"
        /// </summary>
		public static string Pm => resourceProvider.GetString("Pm");

		/// <summary>
        /// Gets a localized string similar to "Polonium"
        /// </summary>
		public static string Po => resourceProvider.GetString("Po");

		/// <summary>
        /// Gets a localized string similar to "Praseodymium"
        /// </summary>
		public static string Pr => resourceProvider.GetString("Pr");

		/// <summary>
        /// Gets a localized string similar to "Properties:"
        /// </summary>
		public static string Properties => resourceProvider.GetString("Properties");

		/// <summary>
        /// Gets a localized string similar to "Platinum"
        /// </summary>
		public static string Pt => resourceProvider.GetString("Pt");

		/// <summary>
        /// Gets a localized string similar to "Plutonium"
        /// </summary>
		public static string Pu => resourceProvider.GetString("Pu");

		/// <summary>
        /// Gets a localized string similar to "Radium"
        /// </summary>
		public static string Ra => resourceProvider.GetString("Ra");

		/// <summary>
        /// Gets a localized string similar to "Rubidium"
        /// </summary>
		public static string Rb => resourceProvider.GetString("Rb");

		/// <summary>
        /// Gets a localized string similar to "Rhenium"
        /// </summary>
		public static string Re => resourceProvider.GetString("Re");

		/// <summary>
        /// Gets a localized string similar to "Rutherfordium"
        /// </summary>
		public static string Rf => resourceProvider.GetString("Rf");

		/// <summary>
        /// Gets a localized string similar to "Roentgenium"
        /// </summary>
		public static string Rg => resourceProvider.GetString("Rg");

		/// <summary>
        /// Gets a localized string similar to "Rhodium"
        /// </summary>
		public static string Rh => resourceProvider.GetString("Rh");

		/// <summary>
        /// Gets a localized string similar to "Radon"
        /// </summary>
		public static string Rn => resourceProvider.GetString("Rn");

		/// <summary>
        /// Gets a localized string similar to "Ruthenium"
        /// </summary>
		public static string Ru => resourceProvider.GetString("Ru");

		/// <summary>
        /// Gets a localized string similar to "Русский"
        /// </summary>
		public static string Russian => resourceProvider.GetString("Russian");

		/// <summary>
        /// Gets a localized string similar to "Sulfur"
        /// </summary>
		public static string S => resourceProvider.GetString("S");

		/// <summary>
        /// Gets a localized string similar to "Salt"
        /// </summary>
		public static string Salt => resourceProvider.GetString("Salt");

		/// <summary>
        /// Gets a localized string similar to "Antimony"
        /// </summary>
		public static string Sb => resourceProvider.GetString("Sb");

		/// <summary>
        /// Gets a localized string similar to "Scandium"
        /// </summary>
		public static string Sc => resourceProvider.GetString("Sc");

		/// <summary>
        /// Gets a localized string similar to "Selenium"
        /// </summary>
		public static string Se => resourceProvider.GetString("Se");

		/// <summary>
        /// Gets a localized string similar to "Select language"
        /// </summary>
		public static string SelectLanguage => resourceProvider.GetString("SelectLanguage");

		/// <summary>
        /// Gets a localized string similar to "Settings"
        /// </summary>
		public static string Settings => resourceProvider.GetString("Settings");

		/// <summary>
        /// Gets a localized string similar to "Seaborgium"
        /// </summary>
		public static string Sg => resourceProvider.GetString("Sg");

		/// <summary>
        /// Gets a localized string similar to "Silicon"
        /// </summary>
		public static string Si => resourceProvider.GetString("Si");

		/// <summary>
        /// Gets a localized string similar to "Simple substance"
        /// </summary>
		public static string Simple => resourceProvider.GetString("Simple");

		/// <summary>
        /// Gets a localized string similar to "Samarium"
        /// </summary>
		public static string Sm => resourceProvider.GetString("Sm");

		/// <summary>
        /// Gets a localized string similar to "Tin"
        /// </summary>
		public static string Sn => resourceProvider.GetString("Sn");

		/// <summary>
        /// Gets a localized string similar to "Strontium"
        /// </summary>
		public static string Sr => resourceProvider.GetString("Sr");

		/// <summary>
        /// Gets a localized string similar to "Substances"
        /// </summary>
		public static string Substances => resourceProvider.GetString("Substances");

		/// <summary>
        /// Gets a localized string similar to "Type of Substance"
        /// </summary>
		public static string SubstanceType => resourceProvider.GetString("SubstanceType");

		/// <summary>
        /// Gets a localized string similar to "Tantalum"
        /// </summary>
		public static string Ta => resourceProvider.GetString("Ta");

		/// <summary>
        /// Gets a localized string similar to "Terbium"
        /// </summary>
		public static string Tb => resourceProvider.GetString("Tb");

		/// <summary>
        /// Gets a localized string similar to "Technetium"
        /// </summary>
		public static string Tc => resourceProvider.GetString("Tc");

		/// <summary>
        /// Gets a localized string similar to "Tellurium"
        /// </summary>
		public static string Te => resourceProvider.GetString("Te");

		/// <summary>
        /// Gets a localized string similar to "Thorium"
        /// </summary>
		public static string Th => resourceProvider.GetString("Th");

		/// <summary>
        /// Gets a localized string similar to "Titanium"
        /// </summary>
		public static string Ti => resourceProvider.GetString("Ti");

		/// <summary>
        /// Gets a localized string similar to "Thallium"
        /// </summary>
		public static string Tl => resourceProvider.GetString("Tl");

		/// <summary>
        /// Gets a localized string similar to "Thulium"
        /// </summary>
		public static string Tm => resourceProvider.GetString("Tm");

		/// <summary>
        /// Gets a localized string similar to "Tennessine"
        /// </summary>
		public static string Ts => resourceProvider.GetString("Ts");

		/// <summary>
        /// Gets a localized string similar to "Uranium"
        /// </summary>
		public static string U => resourceProvider.GetString("U");

		/// <summary>
        /// Gets a localized string similar to "Українська"
        /// </summary>
		public static string Ukrainian => resourceProvider.GetString("Ukrainian");

		/// <summary>
        /// Gets a localized string similar to "Vanadium"
        /// </summary>
		public static string V => resourceProvider.GetString("V");

		/// <summary>
        /// Gets a localized string similar to "Tungsten"
        /// </summary>
		public static string W => resourceProvider.GetString("W");

		/// <summary>
        /// Gets a localized string similar to "Xenon"
        /// </summary>
		public static string Xe => resourceProvider.GetString("Xe");

		/// <summary>
        /// Gets a localized string similar to "Yttrium"
        /// </summary>
		public static string Y => resourceProvider.GetString("Y");

		/// <summary>
        /// Gets a localized string similar to "Ytterbium"
        /// </summary>
		public static string Yb => resourceProvider.GetString("Yb");

		/// <summary>
        /// Gets a localized string similar to "Zinc"
        /// </summary>
		public static string Zn => resourceProvider.GetString("Zn");

		/// <summary>
        /// Gets a localized string similar to "Zirconium"
        /// </summary>
		public static string Zr => resourceProvider.GetString("Zr");

	}


	public sealed class LocalizedStrings : ObservableObject
    {
		/// <summary>
        /// Initialize a new instance of <see cref="LocalizedStrings"/> class.
        /// </summary>
        public LocalizedStrings() => this.RefreshLanguageSettings();

		public static event EventHandler LocalizedStringsRefreshedEvent;

		public void OnLocalizedStringsRefreshedEvent()
        {
            // Make a temporary copy of the event to avoid possibility of 
            // a race condition if the last subscriber unsubscribes 
            // immediately after the null check and before the event is raised.
            EventHandler handler = LocalizedStringsRefreshedEvent;

            // Event will be null if there are no subscribers 
            if (handler != null)
            {
                // Use the () operator to raise the event.
                handler(this, new EventArgs());
            }
        }

		        /// <summary>
		/// Gets resources that are common across application.
		/// </summary>
		public PageResources PageResources { get; private set; }

	
		public void RefreshLanguageSettings()
        {
			            this.PageResources = new PageResources();
			this.OnPropertyChanged("PageResources");
		

			OnLocalizedStringsRefreshedEvent();
		}
	}
}