﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Windows.ApplicationModel.Resources.Core;

namespace Chemistry.Resources.Core
{
    public class ResourceProvider
    {
        private string subtreeName;

        private ResourceMap resourceStringMap;

        private ResourceContext context;

        private CultureInfo lastUsedCultureInfo;

        public ResourceProvider(string resourceSubtreeName)
        {
            this.subtreeName = resourceSubtreeName;
        }

        public static CultureInfo GlobalOverrideCulture
        {
            get
            {
                return CultureInfo.DefaultThreadCurrentUICulture ?? CultureInfo.CurrentUICulture;
            }
        }

        public static CultureInfo GlobalActualCulture
        {
            get
            {
                var actual = CultureInfo.CurrentUICulture;

                if (GlobalOverrideCulture != null)
                {
                    actual = GlobalOverrideCulture;
                }

                return actual;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Overrided")]
        public CultureInfo OverridedCulture { get; set; }

        private ResourceMap ResourceStringMap
        {
            get
            {
                if (Object.ReferenceEquals(resourceStringMap, null))
                {
                    resourceStringMap = ResourceManager.Current.MainResourceMap.GetSubtree(this.subtreeName);
                }

                return resourceStringMap;
            }
        }

        private ResourceContext Context
        {
            get
            {
                if (this.ActualCulture != this.lastUsedCultureInfo)
                {
                    this.lastUsedCultureInfo = ActualCulture;
                    this.context = new ResourceContext();
                    this.context.Languages = new List<string> { this.lastUsedCultureInfo.Name };
                }

                return this.context;
            }
        }

        private CultureInfo ActualCulture
        {
            get
            {
                var actual = GlobalActualCulture;

                if (this.OverridedCulture != null)
                {
                    actual = this.OverridedCulture;
                }
                return actual;
            }
        }

        public string GetString(string resourceKey)
        {
            return ResourceStringMap.GetValue(resourceKey, this.Context).ValueAsString;
        }
    }
}