﻿using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Chemistry.Core.Test.Creators
{
    [TestClass]
    public class QuantumSubshellsCreatorTest
    {
        private List<(int Period, QuantumSubshellFromKind SubshellKind)> subshellOrdered = new List<(int, QuantumSubshellFromKind)>()
        {
            (1, QuantumSubshellFromKind.S),
            (2, QuantumSubshellFromKind.S),
            (2, QuantumSubshellFromKind.P),
            (3, QuantumSubshellFromKind.S),
            (3, QuantumSubshellFromKind.P),
            (4, QuantumSubshellFromKind.S),
            (3, QuantumSubshellFromKind.D),
            (4, QuantumSubshellFromKind.P),
            (5, QuantumSubshellFromKind.S),
            (4, QuantumSubshellFromKind.D),
            (5, QuantumSubshellFromKind.P),
            (6, QuantumSubshellFromKind.S),
            (4, QuantumSubshellFromKind.F),
            (5, QuantumSubshellFromKind.D),
            (6, QuantumSubshellFromKind.P),
            (7, QuantumSubshellFromKind.S),
            (5, QuantumSubshellFromKind.F),
            (6, QuantumSubshellFromKind.D),
            (7, QuantumSubshellFromKind.P)
        };

        [TestMethod]
        public void Create_QuantumSubshellCollection_IsOrdered()
        {
            List<QuantumSubshell> subshells = QuantumSubshellsProvider.Subshells;

            for (int i = 0; i < subshells.Count; i++)
            {
                Assert.AreEqual(subshellOrdered[i].Period, subshells[i].Period,
                    $"Expected {subshellOrdered[i].Period}{subshellOrdered[i].SubshellKind}. " +
                    $"Actual {subshells[i].Period}{subshells[i].QuantumSubshellFromKind}");

                Assert.AreEqual(subshellOrdered[i].SubshellKind, subshells[i].QuantumSubshellFromKind,
                    $"Expected {subshellOrdered[i].Period}{subshellOrdered[i].SubshellKind}. " +
                    $"Actual {subshells[i].Period}{subshells[i].QuantumSubshellFromKind}");
            }
        }
    }
}