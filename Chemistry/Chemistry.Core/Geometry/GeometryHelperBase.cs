﻿using Chemistry.Models;
using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Drawing;

namespace Chemistry.Core.Geometry
{
    public abstract class GeometryHelperBase
    {
        protected const int MarginY = 4;

        public abstract List<GeometryUnit> CreateGeometry(Molecule molecule);

        protected int GetX(int targetColumn, int maxColumn)
        {
            int startX = this.StartX(maxColumn);
            return startX + 2 * (targetColumn - 1) * Constant.Length;
        }

        protected int GetY(int targetRow, int maxRow)
        {
            int startY = this.StartY(maxRow);
            return startY + (targetRow - 1) * (Constant.Length + GeometryHelperBase.MarginY);
        }

        protected int GetAlignedY(int targetRow, int maxTargetRow, int maxRow, bool isTop = false)
        {
            int startY = this.StartY(maxRow);
            int block = (Constant.Length + GeometryHelperBase.MarginY) * (maxRow / maxTargetRow);
            return isTop 
                ? startY + (targetRow - 1) * block
                : startY + block / 2 - (Constant.Length + GeometryHelperBase.MarginY) / 2 + (targetRow - 1) * block;
        }

        protected Point GetLinkFrom(Point start)
           => new Point(start.X + Constant.Length + 1, start.Y + Constant.Length / 2);

        protected Point GetLinkTo(Point end)
           => new Point(end.X - 1, end.Y + Constant.Length / 2);

        protected int StartX(int maxColumns)
        {
            // width of atoms + lenght of links
            int length = maxColumns * Constant.Length + (maxColumns - 1) * Constant.Length;
            return (Constant.CanvasWidth - length) / 2;
        }

        protected int StartY(int maxRows)
        {
            // height of atoms + lenght of links
            int length = maxRows * Constant.Length + (maxRows - 1) * GeometryHelperBase.MarginY;
            return (Constant.CanvasHeight - length) / 2;
        }
    }
}