﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Chemistry.Core.Geometry
{
    public class AlkaliGeometryHelper : GeometryHelperBase
    {
        public override List<GeometryUnit> CreateGeometry(Molecule molecule)
        {
            var moleculeStructure = new List<GeometryUnit>();

            var metal = molecule.MoleculeUnits[0];
            var oxygen = molecule.MoleculeUnits[1];
            var hydrogen = molecule.MoleculeUnits[2];

            int width = 3;

            for (int i = 0; i < metal.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = metal.Atom,
                    From = new Point(this.GetX(1, width), this.GetY(i + 1, metal.Count)),
                    GeometryType = GeometryType.Atom
                });
            }

            for (int i = 0; i < oxygen.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = oxygen.Atom,
                    From = new Point(this.GetX(2, width), this.GetY(i + 1, oxygen.Count)),
                    GeometryType = GeometryType.Atom
                });

                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = hydrogen.Atom,
                    From = new Point(this.GetX(3, width), this.GetY(i + 1, hydrogen.Count)),
                    GeometryType = GeometryType.Atom
                });
            }

            for (int i = 0, index = metal.Count; i < Math.Abs(metal.Valance); i++, index += 2)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    From = this.GetLinkFrom(moleculeStructure[0].From),
                    To = this.GetLinkTo(moleculeStructure[index].From),
                    GeometryType = GeometryType.Link,
                    LinkMultiplicity = 1
                });

                moleculeStructure.Add(new GeometryUnit()
                {
                    From = this.GetLinkFrom(moleculeStructure[index].From),
                    To = this.GetLinkTo(moleculeStructure[index + 1].From),
                    GeometryType = GeometryType.Link,
                    LinkMultiplicity = 1
                });
            }

            return moleculeStructure;
        }
    }
}