﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Chemistry.Core.Geometry
{
    public sealed class GeometryHelper
    {
        public ObservableCollection<GeometryUnit> BuildGeometryMolecule(Molecule molecule, SubstanceType substanceType)
        {
            var atoms = new List<GeometryUnit>();

            switch (substanceType)
            {
                case SubstanceType.Simple:
                    atoms = new SimpleGeometryHelper().CreateGeometry(molecule);
                    break;

                case SubstanceType.Oxide:
                    atoms = new OxideGeometryHelper().CreateGeometry(molecule);
                    break;

                case SubstanceType.Acid:
                    atoms = new AcidGeometryHelper().CreateGeometry(molecule);
                    break;

                case SubstanceType.Alkali:
                    atoms = new AlkaliGeometryHelper().CreateGeometry(molecule);
                    break;

                case SubstanceType.Salt:
                    atoms = new SaltGeometryHelper().CreateGeometry(molecule);
                    break;
            }

            return new ObservableCollection<GeometryUnit>(atoms);
        }
    }
}