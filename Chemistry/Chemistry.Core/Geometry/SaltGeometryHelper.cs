﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Chemistry.Core.Geometry
{
    public class SaltGeometryHelper : GeometryHelperBase
    {
        public override List<GeometryUnit> CreateGeometry(Molecule molecule)
        {
            var moleculeStructure = new List<GeometryUnit>();

            if (molecule.MoleculeUnits.Count < 3)
                return this.CreateSaltWithoutOxygen(molecule);

            var metal = molecule.MoleculeUnits[0];
            var nonmetal = molecule.MoleculeUnits[1];
            var oxygen = molecule.MoleculeUnits[2];

            int width = 3;
            int height = Math.Max(oxygen.Count, metal.Count);

            for (int i = 0; i < nonmetal.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = nonmetal.Atom,
                    From = new Point(this.GetX(1, width), this.GetAlignedY(i + 1, nonmetal.Count, height)),
                    GeometryType = GeometryType.Atom
                });
            }

            for (int i = 0; i < oxygen.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = oxygen.Atom,
                    From = new Point(this.GetX(2, width), this.GetY(i + 1, oxygen.Count)),
                    GeometryType = GeometryType.Atom
                });
            }

            for (int i = 0; i < metal.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = metal.Atom,
                    From = new Point(this.GetX(3, width), this.GetAlignedY(i + 1, metal.Count, height, true)),
                    GeometryType = GeometryType.Atom
                });
            }

            var metalValances = Math.Abs(metal.Valance) * metal.Count;
            var metalValanceProOxygen = metalValances / nonmetal.Count;

            List<int> freeOxygen = new List<int>();

            for (int i = 0, j = 0, oxygenSimpleValance = 0; i < oxygen.Count; i++)
            {
                if (i != 0 && i % (oxygen.Count / nonmetal.Count) == 0)
                {
                    oxygenSimpleValance = 0;
                    j++;
                }

                moleculeStructure.Add(new GeometryUnit()
                {
                    From = this.GetLinkFrom(moleculeStructure[j].From),
                    To = this.GetLinkTo(moleculeStructure[nonmetal.Count + i].From),
                    GeometryType = GeometryType.Link,
                    LinkMultiplicity = 2
                });

                if (oxygenSimpleValance < metalValanceProOxygen)
                {
                    moleculeStructure.Last().LinkMultiplicity = 1;
                    freeOxygen.Add(nonmetal.Count + i);
                }

                oxygenSimpleValance++;
            }

            for (int i = 0, metalIndex = oxygen.Count + nonmetal.Count; i < freeOxygen.Count; i++)
            {
                if (i != 0 && ((i != 1 && i % metal.Valance == 0) || metal.Valance == 1))
                    metalIndex++;

                moleculeStructure.Add(new GeometryUnit()
                {
                    From = this.GetLinkFrom(moleculeStructure[freeOxygen[i]].From),
                    To = this.GetLinkTo(moleculeStructure[metalIndex].From),
                    GeometryType = GeometryType.Link,
                    LinkMultiplicity = 1
                });
            }

            return moleculeStructure;
        }

        private List<GeometryUnit> CreateSaltWithoutOxygen(Molecule molecule)
        {
            var moleculeStructure = new List<GeometryUnit>();

            var metal = molecule.MoleculeUnits[0];
            var nonmetal = molecule.MoleculeUnits[1];

            int width = 2;
            int height = Math.Max(nonmetal.Count, metal.Count);

            for (int i = 0; i < nonmetal.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = nonmetal.Atom,
                    From = new Point(this.GetX(1, width), this.GetAlignedY(i + 1, nonmetal.Count, height)),
                    GeometryType = GeometryType.Atom
                });
            }

            for (int i = 0; i < metal.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = metal.Atom,
                    From = new Point(this.GetX(3, width), this.GetAlignedY(i + 1, metal.Count, height, true)),
                    GeometryType = GeometryType.Atom
                });
            }

            int metalValance = 0;
            bool isFull = true;
            int toFill = 0;

            if (Math.Abs(metal.Valance) > Math.Abs(nonmetal.Valance))
                for (int i = 0, j = nonmetal.Count; i < metal.Count; i++)
                {
                    metalValance = Math.Abs(metal.Valance);

                    while (metalValance > 0)
                    {
                        int valance = Math.Min(metalValance, Math.Abs(nonmetal.Valance));
                        if (!isFull)
                            valance = toFill;

                        moleculeStructure.Add(new GeometryUnit()
                        {
                            From = this.GetLinkFrom(moleculeStructure[i].From),
                            To = this.GetLinkTo(moleculeStructure[j].From),
                            GeometryType = GeometryType.Link,
                            LinkMultiplicity = valance
                        });

                        isFull = Math.Abs(nonmetal.Valance) - valance <= 0;
                        toFill = nonmetal.Valance - metalValance;
                        metalValance -= valance;
                        i++;
                    }
                }
            else { }
                //for (int i = 0, j = metal.Count; j < metal.Count + nonmetal.Count; j++)
                //{
                //    int metalValance = Math.Abs(metal.Valance);

                //    while (metalValance > 0)
                //    {
                //        moleculeStructure.Add(new GeometryUnit()
                //        {
                //            From = this.GetLinkFrom(moleculeStructure[i].From),
                //            To = this.GetLinkTo(moleculeStructure[j].From),
                //            GeometryType = GeometryType.Link,
                //            LinkMultiplicity = Math.Min(metalValance, Math.Abs(nonmetal.Valance))
                //        });

                //        metalValance -= Math.Abs(nonmetal.Valance);
                //        i++;
                //    }
                //}

            return moleculeStructure;
        }
    }
}