﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Chemistry.Core.Geometry
{
    public class SimpleGeometryHelper : GeometryHelperBase
    {
        private Dictionary<string, int> SimpleTwoAtomsElements = new Dictionary<string, int>()
        {
            { "H", 1 },
            { "N", 3 },
            { "O", 2 },
            { "F", 1 },
            { "Cl", 1 },
            { "Br", 1 },
            { "I", 1 }
        };

        public override List<GeometryUnit> CreateGeometry(Molecule molecule)
        {
            var moleculeStructure = new List<GeometryUnit>();

            var moleculeUnit = molecule.MoleculeUnits.First();
            var count = moleculeUnit.Count;

            var firstAtom = new GeometryUnit()
            {
                Atom = moleculeUnit.Atom,
                From = new Point(this.GetX(1, count), this.GetY(1, 1)),
                GeometryType = GeometryType.Atom
            };

            moleculeStructure.Add(firstAtom);

            if (count > 1)
            {
                var secondAtom = new GeometryUnit()
                {
                    Atom = moleculeUnit.Atom,
                    From = new Point(this.GetX(2, count), this.GetY(1, 1)),
                    GeometryType = GeometryType.Atom
                };

                var link = new GeometryUnit()
                {
                    From = this.GetLinkFrom(firstAtom.From),
                    To = this.GetLinkTo(secondAtom.From),
                    GeometryType = GeometryType.Link,
                    LinkMultiplicity = this.SimpleTwoAtomsElements[moleculeUnit.Atom.Abbreviation]
                };

                moleculeStructure.Add(secondAtom);
                moleculeStructure.Add(link);
            }

            return moleculeStructure;
        }
    }
}