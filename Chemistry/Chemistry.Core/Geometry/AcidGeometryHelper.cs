﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Chemistry.Core.Geometry
{
    public class AcidGeometryHelper : GeometryHelperBase
    {
        public override List<GeometryUnit> CreateGeometry(Molecule molecule)
        {
            var moleculeStructure = new List<GeometryUnit>();

            var hydrogen = molecule.MoleculeUnits[0];
            var nonmetal = molecule.MoleculeUnits[1];
            var oxygen = molecule.MoleculeUnits[2];

            int width = 3;

            moleculeStructure.Add(new GeometryUnit()
            {
                Atom = nonmetal.Atom,
                From = new Point(this.GetX(1, width), this.GetY(1, 1)),
                GeometryType = GeometryType.Atom
            });

            for (int i = 0, j = 0; i < oxygen.Count; i++, j++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = oxygen.Atom,
                    From = new Point(this.GetX(2, width), this.GetY(i + 1, oxygen.Count)),
                    GeometryType = GeometryType.Atom
                });

                if (j < hydrogen.Count)
                    moleculeStructure.Add(new GeometryUnit()
                    {
                        Atom = hydrogen.Atom,
                        From = new Point(this.GetX(3, width), this.GetY(i + 1, oxygen.Count)),
                        GeometryType = GeometryType.Atom
                    });
            }

            for (int i = 0, index = 0; i < oxygen.Count; i++, index++)
            {
                if (index / 2 < hydrogen.Count)
                {
                    moleculeStructure.Add(new GeometryUnit()
                    {
                        From = this.GetLinkFrom(moleculeStructure[0].From),
                        To = this.GetLinkTo(moleculeStructure[index + 1].From),
                        GeometryType = GeometryType.Link,
                        LinkMultiplicity = 1
                    });

                    moleculeStructure.Add(new GeometryUnit()
                    {
                        From = this.GetLinkFrom(moleculeStructure[index + 1].From),
                        To = this.GetLinkTo(moleculeStructure[index + 2].From),
                        GeometryType = GeometryType.Link,
                        LinkMultiplicity = 1
                    });

                    index++;
                }
                else
                {
                    moleculeStructure.Add(new GeometryUnit()
                    {
                        From = this.GetLinkFrom(moleculeStructure[0].From),
                        To = this.GetLinkTo(moleculeStructure[index + 1].From),
                        GeometryType = GeometryType.Link,
                        LinkMultiplicity = 2
                    });
                }
            }

            return moleculeStructure;
        }
    }
}