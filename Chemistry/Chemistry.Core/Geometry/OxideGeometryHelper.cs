﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Chemistry.Core.Geometry
{
    public class OxideGeometryHelper : GeometryHelperBase
    {
        public override List<GeometryUnit> CreateGeometry(Molecule molecule)
        {
            var moleculeStructure = new List<GeometryUnit>();

            var element = molecule.MoleculeUnits[0];
            var oxygen = molecule.MoleculeUnits[1];

            int width = 2;

            for (int i = 0; i < element.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = element.Atom,
                    From = new Point(this.GetX(1, width), this.GetAlignedY(i + 1, element.Count, Math.Max(element.Count, oxygen.Count))),
                    GeometryType = GeometryType.Atom
                });
            }

            for (int i = 0; i < oxygen.Count; i++)
            {
                moleculeStructure.Add(new GeometryUnit()
                {
                    Atom = oxygen.Atom,
                    From = new Point(this.GetX(2, width), this.GetY(i + 1, oxygen.Count)),
                    GeometryType = GeometryType.Atom
                });
            }

            int valance = 0;

            for (int i = 0, oxygenCount = element.Count; i < element.Count; i++)
            {
                valance += Math.Abs(element.Valance);

                while (valance > 0)
                {
                    if (valance < 2)
                    {
                        moleculeStructure.Add(new GeometryUnit()
                        {
                            From = this.GetLinkFrom(moleculeStructure[i].From),
                            To = this.GetLinkTo(moleculeStructure[oxygenCount].From),
                            GeometryType = GeometryType.Link,
                            LinkMultiplicity = 1
                        });

                        moleculeStructure.Add(new GeometryUnit()
                        {
                            From = this.GetLinkFrom(moleculeStructure[i + 1].From),
                            To = this.GetLinkTo(moleculeStructure[oxygenCount].From),
                            GeometryType = GeometryType.Link,
                            LinkMultiplicity = 1
                        });
                    }
                    else
                        moleculeStructure.Add(new GeometryUnit()
                        {
                            From = this.GetLinkFrom(moleculeStructure[i].From),
                            To = this.GetLinkTo(moleculeStructure[oxygenCount].From),
                            GeometryType = GeometryType.Link,
                            LinkMultiplicity = 2
                        });

                    valance -= 2;
                    oxygenCount++;
                }
            }

            return moleculeStructure;
        }
    }
}