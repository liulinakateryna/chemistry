﻿using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Linq;

namespace Chemistry.Core.Helpers
{
    public sealed class ValancesDistributionHelper
    {
        private AtomBehaviourProvider atomBehaviourProvider;

        public void SetPrefferedValences(List<MoleculeComponent> moleculeComponents)
        {
            var components = moleculeComponents.Where(component => component != null);

            var metals = components.Where(component => this.atomBehaviourProvider.IsMetal(component.Atom)).ToList();
            metals.ForEach(component => this.SetValentsToMetal(component));

            bool isPositiveNonMetal = false;
            var nonMetalComponents = components.Where(component => this.atomBehaviourProvider.IsNonMetal(component.Atom));
            var O = nonMetalComponents.FirstOrDefault(component => component.Atom.Abbreviation == "O");
            
            if (O != null)
            {
                O.SelectedValance = -2;
                isPositiveNonMetal = true;
            }

            if (!metals.Any())
                isPositiveNonMetal = true;

            nonMetalComponents.Where(component => component.Atom.Abbreviation != "O")
                .ToList().ForEach(component => component.SelectedValance = this.GetNonMetalValance(component, isPositiveNonMetal));
        }

        private void SetValentsToMetal(MoleculeComponent component)
        {
            component.SelectedValance = component.Valances.First();
        }

        public ValancesDistributionHelper()
        {
            this.atomBehaviourProvider = new AtomBehaviourProvider();
        }

        private int GetNonMetalValance(MoleculeComponent component, bool isPositive)
        {
            int prefferedValance = 0;

            switch (component.Atom.Group)
            {
                case 4:
                    prefferedValance = (isPositive) ? 4 : -4;
                    break;

                case 5:
                    prefferedValance = (isPositive) ? 5 : -3;
                    break;

                case 6:
                    prefferedValance = (isPositive) ? 6 : -2;
                    break;

                case 7:
                    prefferedValance = (isPositive) ? 7 : -1;
                    break;
            }

            if (!component.Valances.Contains(prefferedValance))
            {
                var valanceValue = isPositive 
                    ? component.Valances.Where(valance => valance > 0).FirstOrDefault()
                    : component.Valances.Where(valance => valance < 0).FirstOrDefault();

                if (valanceValue != 0)
                    prefferedValance = valanceValue;
            }

            return prefferedValance;
        }
    }
}