﻿using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Chemistry.Core.Helpers
{
    public sealed class SubstanceTypeHelper
    {
        private AtomBehaviourProvider atomBehaviourProvider;

        public ObservableCollection<SubstanceType> GetSubstanceTypes(List<MoleculeComponent> moleculeComponents)
        {
            var substances = new ObservableCollection<SubstanceType>();

            var atoms = moleculeComponents.Where(atom => atom != null).ToList();

            if (atoms.Any())
                substances.Add(SubstanceType.Simple);

            bool hasOxygen = atoms.Any(atom => atom.Atom.Abbreviation == "O");
            bool hasHydrogen = atoms.Any(atom => atom.Atom.Abbreviation == "H");
            bool hasMetal = atoms.Any(atom => this.atomBehaviourProvider.IsMetal(atom.Atom) && atom.Atom.Abbreviation != "H");
            bool hasNonMetal = atoms.Any(atom => this.atomBehaviourProvider.IsNonMetal(atom.Atom) && atom.Atom.Abbreviation != "O");

            if (hasOxygen && (hasHydrogen || hasMetal || hasNonMetal))
                substances.Add(SubstanceType.Oxide);

            if (hasHydrogen && hasNonMetal)
                substances.Add(SubstanceType.Acid);

            if (hasHydrogen && hasOxygen && hasMetal)
                substances.Add(SubstanceType.Alkali);

            if (hasMetal && hasNonMetal)
                substances.Add(SubstanceType.Salt);

            return substances;
        }

        public SubstanceTypeHelper()
        {
            this.atomBehaviourProvider = new AtomBehaviourProvider();
        }
    }
}