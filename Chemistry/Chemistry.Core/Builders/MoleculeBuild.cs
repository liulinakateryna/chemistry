﻿using Chemistry.Core.Helpers;
using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Chemistry.Core.Builders
{
    public sealed class MoleculeBuild
    {
        private AtomBehaviourProvider atomBehaviourProvider;

        private List<string> SimpleTwoAtomsElements = new List<string>()
        {
            "H",
            "N",
            "O",
            "F",
            "Cl",
            "Br",
            "I"
        };

        private List<string> TriBasicAtomsElements = new List<string>()
        {
            "P",            
            "As",
            "Sb",
            "Bi",
            "Mc"
        };

        public ObservableCollection<Molecule> Build(SubstanceType substanceType, List<MoleculeComponent> components)
        {
            var moleculeComponents = components.Where(component => component != null).ToList();
            var possibleSubstanceTypes = new SubstanceTypeHelper().GetSubstanceTypes(moleculeComponents);

            ObservableCollection<Molecule> molecules;

            switch (substanceType)
            {
                case SubstanceType.Acid:
                    molecules = this.BuildAcid(moleculeComponents);
                    break;

                case SubstanceType.Alkali:
                    molecules = new ObservableCollection<Molecule>()
                    {
                        this.BuildAlkali(moleculeComponents)
                    };
                    break;

                case SubstanceType.Oxide:
                    molecules = this.BuildOxide(moleculeComponents);
                    break;

                case SubstanceType.Salt:
                    molecules = this.BuildSalt(moleculeComponents);
                    break;

                case SubstanceType.Simple:
                    molecules = this.BuildSimple(moleculeComponents);
                    break;

                default:
                    throw new ArgumentException();
            }

            this.SetFormulaNames(molecules, substanceType);
            return molecules;
        }

        private ObservableCollection<Molecule> BuildAcid(List<MoleculeComponent> components)
        {
            var molecules = new ObservableCollection<Molecule>();

            var H = components.Where(component => component.Atom.Abbreviation == "H").First();
            var O = components.Where(component => component.Atom.Abbreviation == "O").FirstOrDefault();
            var nonMetal = components.Where(component => this.atomBehaviourProvider.IsNonMetal(component.Atom) && component.Atom.Abbreviation != "O").First();

            if (O == null)
                molecules.Add(this.BuildAcidWithoutOxygen(H, nonMetal));
            else
            {
                bool isTriBasicAcid = nonMetal.SelectedValance == 5;
                molecules.Add(this.BuildAcidWithOxygen(H, nonMetal, O, false));

                if (isTriBasicAcid)
                    molecules.Add(this.BuildAcidWithOxygen(H, nonMetal, O, true));
            }

            return molecules;
        }

        private Molecule BuildAcidWithoutOxygen(MoleculeComponent H, MoleculeComponent nonMetal)
        {
            var valance = nonMetal.SelectedValance;
            if (valance > 0)
                valance = nonMetal.Valances.FirstOrDefault(value => value < 0);

            var unitH = new MoleculeUnit()
            {
                Atom = H.Atom,
                Count = Math.Abs(valance),
                Valance = 1
            };

            var element = new MoleculeUnit()
            {
                Atom = nonMetal.Atom,
                Count = 1,
                Valance = valance
            };

            var molecule = new Molecule();
            molecule.MoleculeUnits.Add(unitH);
            molecule.MoleculeUnits.Add(element);

            return molecule;
        }

        private Molecule BuildAcidWithOxygen(MoleculeComponent H, MoleculeComponent nonMetal, MoleculeComponent O, bool customizedChemical)
        {
            var valance = nonMetal.SelectedValance;
            if (valance <= 0)
                valance = nonMetal.Valances.FirstOrDefault(value => value > 0);

            int countH = valance % 2 == 0 ? 2 : 1;
            if (customizedChemical && this.TriBasicAtomsElements.Contains(nonMetal.Atom.Abbreviation))
                countH = 3;

            int countO = (Math.Abs(valance) + countH) / 2;

            var unitH = new MoleculeUnit()
            {
                Atom = H.Atom,
                Count = countH,
                Valance = 1
            };

            var element = new MoleculeUnit()
            {
                Atom = nonMetal.Atom,
                Count = 1,
                Valance = valance
            };

            var unitO = new MoleculeUnit()
            {
                Atom = O.Atom,
                Count = countO,
                Valance = -2
            };

            var molecule = new Molecule();
            molecule.MoleculeUnits.Add(unitH);
            molecule.MoleculeUnits.Add(element);
            molecule.MoleculeUnits.Add(unitO);

            return molecule;
        }

        private Molecule BuildAlkali(List<MoleculeComponent> components)
        {
            var O = components.Where(component => component.Atom.Abbreviation == "O").First();
            var H = components.Where(component => component.Atom.Abbreviation == "H").First();
            var metal = components.Where(component => this.atomBehaviourProvider.IsMetal(component.Atom) && component.Atom.Abbreviation != "H").First();

            var valance = metal.SelectedValance;

            var countO = Math.Abs(valance);
            var countH = Math.Abs(valance);
            var countMetal = 1;

            var atomO = new MoleculeUnit()
            {
                Atom = O.Atom,
                Count = countO,
                Valance = -2
            };

            var atomH = new MoleculeUnit()
            {
                Atom = H.Atom,
                Count = countH,
                Valance = 1
            };

            var atomMetal = new MoleculeUnit()
            {
                Atom = metal.Atom,
                Count = countMetal,
                Valance = valance
            };

            var molecule = new Molecule();
            molecule.MoleculeUnits.Add(atomMetal);
            molecule.MoleculeUnits.Add(atomO);
            molecule.MoleculeUnits.Add(atomH);

            return molecule;
        }

        private ObservableCollection<Molecule> BuildOxide(List<MoleculeComponent> components)
        {
            var molecules = new ObservableCollection<Molecule>();
            var moleculeO = components.Where(component => component.Atom.Abbreviation == "O").First();
            var moleculeComponents = components.Where(component => component.Atom.Abbreviation != "O").ToList();

            foreach (var component in moleculeComponents)
            {
                var valance = component.SelectedValance > 0 ? component.SelectedValance 
                    : component.Valances.FirstOrDefault(valanceValue => valanceValue > 0);

                var countO = Math.Abs(valance) % 2 == 0 ? Math.Abs(valance) / 2 : Math.Abs(valance);
                var countElement = Math.Abs(valance) % 2 == 0 ? 1 : 2;

                var O = new MoleculeUnit()
                {
                    Atom = moleculeO.Atom,
                    Count = countO,
                    Valance = -2
                };

                var element = new MoleculeUnit()
                {
                    Atom = component.Atom,
                    Count = countElement,
                    Valance = valance
                };

                var molecule = new Molecule();
                molecule.MoleculeUnits.Add(element);
                molecule.MoleculeUnits.Add(O);

                molecules.Add(molecule);
            }

            return molecules;
        }

        private ObservableCollection<Molecule> BuildSimple(List<MoleculeComponent> components)
        {
            var molecules = new ObservableCollection<Molecule>();

            foreach (var component in components)
            {
                var molecule = new Molecule();

                var moleculeUnit = new MoleculeUnit()
                {
                    Atom = component.Atom,
                    Count = this.SimpleTwoAtomsElements.Contains(component.Atom.Abbreviation) ? 2 : 1,
                    Valance = component.SelectedValance
                };

                molecule.MoleculeUnits.Add(moleculeUnit);

                molecules.Add(molecule);
            }

            return molecules;
        }

        private ObservableCollection<Molecule> BuildSalt(List<MoleculeComponent> components)
        {
            var molecules = new ObservableCollection<Molecule>();

            var O = components.Where(component => component.Atom.Abbreviation == "O").FirstOrDefault();
            var nonMetal = components.Where(component => this.atomBehaviourProvider.IsNonMetal(component.Atom) && component.Atom.Abbreviation != "O").First();
            var metal = components.Where(component => this.atomBehaviourProvider.IsMetal(component.Atom) && component.Atom.Abbreviation != "H").First();
            var H = AtomsProvider.Atoms.Where(atom => atom.Abbreviation == "H").First();

            var acidComponents = new List<MoleculeComponent>() 
            { 
                new MoleculeComponent() 
                { 
                    Atom = H,
                    SelectedValance = 1
                }, 
                nonMetal 
            };

            if (O != null)
                acidComponents.Add(O);

            var acids = this.BuildAcid(acidComponents);
            
            foreach (var acid in acids)
            {
                var molecule = new Molecule();
                var acidRestValance = acid.MoleculeUnits.First().Count;

                int countMetal = acidRestValance;
                int countRestValance = metal.SelectedValance;

                if (countMetal == countRestValance || countMetal % countRestValance == 0)
                {
                    countMetal /= metal.SelectedValance;
                    countRestValance /= metal.SelectedValance;
                }

                molecule.MoleculeUnits.Add(new MoleculeUnit()
                {
                    Atom = metal.Atom,
                    Count = countMetal,
                    Valance = metal.SelectedValance
                });

                for (int i = 1; i < acid.MoleculeUnits.Count; i++)
                {
                    acid.MoleculeUnits[i].Count *= countRestValance;
                    molecule.MoleculeUnits.Add(acid.MoleculeUnits[i]);
                }

                molecules.Add(molecule);
            }

            return molecules;
        }

        private string GetFormula(Molecule molecule, SubstanceType substanceType)
        {
            string formula = string.Empty;

            if (substanceType == SubstanceType.Salt || substanceType == SubstanceType.Alkali)
            {
                int index = molecule.MoleculeUnits[1].Count;

                formula += $"{molecule.MoleculeUnits[0].Atom.Abbreviation}";
                if (molecule.MoleculeUnits[0].Count > 1)
                    formula += molecule.MoleculeUnits[0].Count;

                if (index > 1 && molecule.MoleculeUnits.Count > 2)
                    formula += "(";

                for (int i = 1; i < molecule.MoleculeUnits.Count; i++)
                {
                    formula += molecule.MoleculeUnits[i].Atom.Abbreviation;
                    if (molecule.MoleculeUnits[i].Count / index > 1)
                        formula += molecule.MoleculeUnits[i].Count / index;
                }

                if (index > 1 && molecule.MoleculeUnits.Count > 2)
                    formula += $")";

                if (index > 1)
                    formula += index;
            }
            else
            {
                foreach (var moleculeUnit in molecule.MoleculeUnits)
                {
                    formula += moleculeUnit.Atom.Abbreviation;
                    if (moleculeUnit.Count > 1)
                        formula += moleculeUnit.Count;
                }
            }

            return formula;
        }

        public MoleculeBuild()
        {
            this.atomBehaviourProvider = new AtomBehaviourProvider();
        }

        private void SetFormulaNames(ObservableCollection<Molecule> molecules, SubstanceType substanceType)
        {
            foreach (var molecule in molecules)
                molecule.Nomenclature = this.GetFormula(molecule, substanceType);
        }
    }
}