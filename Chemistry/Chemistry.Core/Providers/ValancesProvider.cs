﻿using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Chemistry.Core.Providers
{
    public sealed class ValancesProvider
    {
        private Dictionary<string, ObservableCollection<int>> Valances = new Dictionary<string, ObservableCollection<int>>()
        {
            { "H", new ObservableCollection<int>() { 1 } },
            { "He", new ObservableCollection<int>() { 0 } },
            { "Li", new ObservableCollection<int>() { 1 } },
            { "Be", new ObservableCollection<int>() { 2 } },
            { "B", new ObservableCollection<int>() { 3 } },
            { "C", new ObservableCollection<int>() { 4, 2, -1, -2, -3, -4 } },
            { "N", new ObservableCollection<int>() { 5, 4, 3, 2, 1, -1, -2, -3 } },
            { "O", new ObservableCollection<int>() { -2 } },
            { "F", new ObservableCollection<int>() { -1, 1 } },
            { "Ne", new ObservableCollection<int>() { 0 } },
            { "Na", new ObservableCollection<int>() { 1 } },
            { "Mg", new ObservableCollection<int>() { 2 } },
            { "Al", new ObservableCollection<int>() { 3 } },
            { "Si", new ObservableCollection<int>() { -4, 4, 2 } },
            { "P", new ObservableCollection<int>() { -3, 1, 3, 5 } },
            { "S", new ObservableCollection<int>() { -2, 2, 4, 6 } },
            { "Cl", new ObservableCollection<int>() { -1, 1, 2, 3, 4, 5, 7 } },
            { "Ar", new ObservableCollection<int>() { 0 } },
            { "K", new ObservableCollection<int>() { 1 } },
            { "Ca", new ObservableCollection<int>() { 2 } },
            { "Sc", new ObservableCollection<int>() { 3 } },
            { "Ti", new ObservableCollection<int>() { 2, 3, 4 } },
            { "V", new ObservableCollection<int>() { 2, 3, 4, 5 } },
            { "Cr", new ObservableCollection<int>() { 2, 3, 6 } },
            { "Mn", new ObservableCollection<int>() { 2, 3, 4, 6, 7 } },
            { "Fe", new ObservableCollection<int>() { 3, 2, 4, 6 } },
            { "Co", new ObservableCollection<int>() { 2, 3, 4 } },
            { "Ni", new ObservableCollection<int>() { 2, 1, 3, 4 } },
            { "Cu", new ObservableCollection<int>() { 2, 3, 1 } },
            { "Zn", new ObservableCollection<int>() { 2 } },
            { "Ga", new ObservableCollection<int>() { 3, 2 } },
            { "Ge", new ObservableCollection<int>() { -4, 2, 4 } },
            { "As", new ObservableCollection<int>() { -3, 3, 5, 2 } },
            { "Se", new ObservableCollection<int>() { -2, 4, 6, 2 } },
            { "Br", new ObservableCollection<int>() { -1, 1, 5, 3, 4 } },
            { "Kr", new ObservableCollection<int>() { 0 } },
            { "Rb", new ObservableCollection<int>() { 1 } },
            { "Sr", new ObservableCollection<int>() { 2 } },
            { "Y", new ObservableCollection<int>() { 3 } },
            { "Zr", new ObservableCollection<int>() { 4, 3, 2 } },
            { "Nb", new ObservableCollection<int>() { 3, 5, 2, 4 } },
            { "Mo", new ObservableCollection<int>() { 3, 6, 2, 4, 5 } },
            { "Tc", new ObservableCollection<int>() { 6 } },
            { "Ru", new ObservableCollection<int>() { 3, 4, 8, 2, 6, 7 } },
            { "Rh", new ObservableCollection<int>() { 4, 2, 3, 6 } },
            { "Pd", new ObservableCollection<int>() { 2, 4, 6 } },
            { "Ag", new ObservableCollection<int>() { 1, 2, 3 } },
            { "Cd", new ObservableCollection<int>() { 2, 1 } },
            { "In", new ObservableCollection<int>() { 3, 1, 2 } },
            { "Sn", new ObservableCollection<int>() { 2, 4 } },
            { "Sb", new ObservableCollection<int>() { -3, 3, 4, 5 } },
            { "Te", new ObservableCollection<int>() { -2, 4, 6, 2 } },
            { "I", new ObservableCollection<int>() { -1, 1, 3, 5, 7, 4 } },
            { "Xe", new ObservableCollection<int>() { 0 } },
            { "Cs", new ObservableCollection<int>() { 1 } },
            { "Ba", new ObservableCollection<int>() { 2 } },
            { "La", new ObservableCollection<int>() { 3 } },
            { "Ce", new ObservableCollection<int>() { 3, 4 } },
            { "Pr", new ObservableCollection<int>() { 3 } },
            { "Nd", new ObservableCollection<int>() { 3, 4 } },
            { "Pm", new ObservableCollection<int>() { 3 } },
            { "Sm", new ObservableCollection<int>() { 3, 2 } },
            { "Eu", new ObservableCollection<int>() { 3, 2 } },
            { "Gd", new ObservableCollection<int>() { 3 } },
            { "Tb", new ObservableCollection<int>() { 3, 4 } },
            { "Dy", new ObservableCollection<int>() { 3 } },
            { "Ho", new ObservableCollection<int>() { 3 } },
            { "Er", new ObservableCollection<int>() { 3 } },
            { "Tm", new ObservableCollection<int>() { 3, 2 } },
            { "Yb", new ObservableCollection<int>() { 3, 2 } },
            { "Lu", new ObservableCollection<int>() { 3 } },
            { "Hf", new ObservableCollection<int>() { 4 } },
            { "Ta", new ObservableCollection<int>() { 5, 4, 3 } },
            { "W", new ObservableCollection<int>() { 6, 5, 4, 3, 2 } },
            { "Re", new ObservableCollection<int>() { 2, 4, 6, 7, 1, 3, 5, -1 } },
            { "Os", new ObservableCollection<int>() { 3, 4, 6, 8, 2 } },
            { "Ir", new ObservableCollection<int>() { 3, 4, 6, 1, 2 } },
            { "Pt", new ObservableCollection<int>() { 2, 4, 6, 1, 3 } },
            { "Au", new ObservableCollection<int>() { 1, 2, 3 } },
            { "Hg", new ObservableCollection<int>() { 1, 2 } },
            { "Tl", new ObservableCollection<int>() { 1, 3, 2 } },
            { "Pb", new ObservableCollection<int>() { 2, 4 } },
            { "Bi", new ObservableCollection<int>() { 3, 2, 4, 5, -3 } },
            { "Po", new ObservableCollection<int>() { 2, 4, 6, -2 } },
            { "At", new ObservableCollection<int>() { 7 } },
            { "Rn", new ObservableCollection<int>() { 0 } },
            { "Fr", new ObservableCollection<int>() { 1 } },
            { "Ra", new ObservableCollection<int>() { 2 } },
            { "Ac", new ObservableCollection<int>() { 3 } },
            { "Th", new ObservableCollection<int>() { 4 } },
            { "Pa", new ObservableCollection<int>() { 5 } },
            { "U", new ObservableCollection<int>() { 3, 4, 6, 2, 3 } }
        };

        public ObservableCollection<int> GetValances(Atom atom)
        {
            if (this.Valances.TryGetValue(atom.Abbreviation, out ObservableCollection<int> valances))
                return valances;

            return new ObservableCollection<int>();
        }
    }
}