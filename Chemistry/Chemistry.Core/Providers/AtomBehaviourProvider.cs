﻿using Chemistry.Models.Models;
using System.Collections.Generic;

namespace Chemistry.Core.Providers
{
    public sealed class AtomBehaviourProvider
    {
        private const int MaxGroup = 8;

        private List<int> metalGroups = new List<int>()
        {
            1,
            2,
            3
        };

        public bool IsInert(Atom atom)
            => atom.Group == AtomBehaviourProvider.MaxGroup && atom.IsMainGroup;

        public bool IsMetal(Atom atom)
        {
            bool isMetal = this.metalGroups.Contains(atom.Group) 
                || (atom.Group == AtomBehaviourProvider.MaxGroup && !atom.IsMainGroup);

            return isMetal;
        }

        public bool IsNonMetal(Atom atom)
            => atom.Group >= 4 && atom.Group < AtomBehaviourProvider.MaxGroup;
    }
}