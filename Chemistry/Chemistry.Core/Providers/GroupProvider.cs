﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System.Collections.Generic;
using System.Linq;

namespace Chemistry.Core.Providers
{
    public sealed class GroupProvider
    {
        private const int MaxGroup = 8;

        public void SetGroups(List<Atom> atoms)
        {
            this.SetGroupsPeriod1(atoms);
            this.SetGroupsPeriod(atoms, 2);
            this.SetGroupsPeriod(atoms, 3);
            this.SetGroupsPeriod(atoms, 4);
            this.SetGroupsPeriod(atoms, 5);
            this.SetGroupsPeriod(atoms, 6);
            this.SetGroupsPeriod(atoms, 7);
        }

        private void SetGroupsPeriod(List<Atom> atoms, int period)
        {
            var periodAtoms = atoms.Where(atom => atom.Period == period).ToList();
            var atomsSP = periodAtoms
                .Where(atom => atom.ElementType == QuantumSubshellFromKind.S || atom.ElementType == QuantumSubshellFromKind.P)
                .ToList();

            var atomsD = periodAtoms
                .Where(atom => atom.ElementType == QuantumSubshellFromKind.D)
                .ToList();

            var atomsF = periodAtoms
                .Where(atom => atom.ElementType == QuantumSubshellFromKind.F)
                .ToList();

            for (int i = 0; i < GroupProvider.MaxGroup; i++)
            {
                atomsSP[i].Group = i + 1;
                atomsSP[i].IsMainGroup = true;
            }

            for(int i = 0, groupValue = 3; i < atomsD.Count; i++)
            {
                atomsD[i].Group = groupValue;
                atomsD[i].IsMainGroup = false;

                if (i < 5 && i > 7)
                    groupValue++;
                else if (i == 8)
                    groupValue = 1;
            }

            atomsF.ForEach(atom =>
            {
                atom.Group = 3;
                atom.IsMainGroup = false;
            });
        }

        private void SetGroupsPeriod1(List<Atom> atoms)
        {
            var period1 = atoms.Where(atom => atom.Period == 1).ToList();
            period1.ForEach(atom => atom.IsMainGroup = true);

            Atom H = period1.First();
            Atom He = period1.Last();

            H.Group = 1;
            He.Group = GroupProvider.MaxGroup;
        }
    }
}