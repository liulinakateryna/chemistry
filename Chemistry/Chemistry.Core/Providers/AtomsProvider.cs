﻿using Chemistry.DataAccess.Reader;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;

namespace Chemistry.Core.Providers
{
    public sealed class AtomsProvider
    {
        private static Lazy<List<Atom>> AtomsInstance = new Lazy<List<Atom>>(() => AtomsProvider.Create());

        public static List<Atom> Atoms = AtomsProvider.AtomsInstance.Value;

        public static List<Atom> Create()
        {

            var periodicTableProvider = new PeriodicTableProvider(new AtomReader());

            List<Atom> atoms = periodicTableProvider.GenerateAtoms();
            new GroupProvider().SetGroups(atoms);

            return atoms;
        }
    }
}