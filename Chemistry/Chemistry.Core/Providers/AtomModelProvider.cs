﻿using Chemistry.Models.Models;
using System;
using System.Collections.Generic;

namespace Chemistry.Core.Providers
{
    public sealed class AtomModelProvider
    {
        public Dictionary<int, Period> GetElectronsDistribution(int atomNumber)
        {
            Dictionary<int, Period> periods = this.InitializeAtomModel();

            int pointer = 0;

            while (atomNumber > 0)
            {
                var currentSubshell = QuantumSubshellsProvider.Subshells[pointer];

                if (periods.TryGetValue(currentSubshell.Period, out Period period))
                {
                    period.SetNumberOfElectrons(currentSubshell.QuantumSubshellFromKind, 
                        Math.Min(atomNumber, currentSubshell.ElectronNumber));
                }

                atomNumber -= currentSubshell.ElectronNumber;
                pointer++;
            }

            return periods;
        }

        private Dictionary<int, Period> InitializeAtomModel()
            => new Dictionary<int, Period>()
            {
                { 1, new Period() { PeriodValue = 1} },
                { 2, new Period() { PeriodValue = 2} },
                { 3, new Period() { PeriodValue = 3} },
                { 4, new Period() { PeriodValue = 4} },
                { 5, new Period() { PeriodValue = 5} },
                { 6, new Period() { PeriodValue = 6} },
                { 7, new Period() { PeriodValue = 7} }
            };
    }
}