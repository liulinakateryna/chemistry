﻿using Chemistry.DataAccess.DTOs;
using Chemistry.DataAccess.Reader;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chemistry.Core.Providers
{
    public sealed class PeriodicTableProvider
    {          
        private readonly IDatabaseReader<int, AtomDto> atomReader;

        public PeriodicTableProvider(IDatabaseReader<int, AtomDto> atomReader)
        {
            if (atomReader is null)
                throw new ArgumentException();

            this.atomReader = atomReader;
        }

        public List<Atom> GeneratePeriodicTable()
        {
            return new List<Atom>();
        }

        public List<Atom> GenerateAtoms()
        {
            Dictionary<int, AtomDto> atomDtos = this.atomReader.Read();

            var atoms = new List<Atom>();

            atomDtos.Values.ToList()
                .OrderBy(atomDto => atomDto.KeyChemicalElement).ToList()
                .ForEach(atomDto => atoms.Add(new Atom()
                {
                    AtomicNumber = atomDto.KeyChemicalElement,
                    Abbreviation = atomDto.ShortName,
                    Ar = atomDto.Weight,
                    Electroneutrolity = atomDto.Electroneutrolity,
                    TextId = atomDto.TextId,
                    IsFake = false
                }));

            this.SetElementTypes(atoms);

            return atoms;
        }

        private void SetElementTypes(List<Atom> atoms)
        {
            List<QuantumSubshell> quantumSubshells = QuantumSubshellsProvider.Subshells;
            int atomIndex = 0;
            int period = 0;

            for (int i = 0; i < quantumSubshells.Count; i++)
            {
                if (quantumSubshells[i].QuantumSubshellFromKind == QuantumSubshellFromKind.S)
                    period++;

                for (int j = 0; j < quantumSubshells[i].ElectronNumber; j++)
                {
                    atoms[atomIndex].ElementType = quantumSubshells[i].QuantumSubshellFromKind;
                    atoms[atomIndex].Period = period;
                    atomIndex++;
                }
            }
        }
    }
}