﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Chemistry.Core.Providers
{
    public class CultureProvider
    {
        private static readonly CultureInfo defaultCulture = new CultureInfo("en-US");
        private static CultureInfo selectedCulture;

        private static readonly List<CultureInfo> supportedLanguages = new List<CultureInfo>
        {
            new CultureInfo("en-US"),
            new CultureInfo("ru-RU"),
            new CultureInfo("uk-UA")
        };

        public static IReadOnlyDictionary<string, CultureInfo> SupportedLanguages 
            => CultureProvider.supportedLanguages.ToDictionary(culture => culture.Name, culture => culture);

        public static CultureInfo SelectedCulture
        {
            get
            {
                if (CultureProvider.selectedCulture == null)
                {
                    CultureProvider.InitializeCultureInFirstTime();
                }

                return selectedCulture;
            }
            set
            {
                selectedCulture = value;
                CultureProvider.ApplyCulture();
            }
        }

        public static void ApplyCulture()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureProvider.SelectedCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureProvider.SelectedCulture;
        }

        private static void InitializeCultureInFirstTime()
        {
            var language = Windows.System.UserProfile.GlobalizationPreferences.Languages[0].ToString();
            var currentUICulture = new CultureInfo(language);

            var cultureInfo = supportedLanguages.FirstOrDefault(c => c.TwoLetterISOLanguageName == currentUICulture.TwoLetterISOLanguageName);

            if (cultureInfo != null && currentUICulture != null)
            {
                CultureProvider.SelectedCulture = cultureInfo;
            }
            else
            {
                CultureProvider.SelectedCulture = CultureProvider.defaultCulture;
            }
        }
    }
}