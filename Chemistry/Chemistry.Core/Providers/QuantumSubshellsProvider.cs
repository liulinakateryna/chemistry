﻿using Chemistry.Models;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chemistry.Core.Providers
{
    /// <summary>
    /// Creeates the collection of quantum subshells ordered by the Aufbau principle.
    /// Правило Клечковского.
    /// </summary>
    public sealed class QuantumSubshellsProvider
    {
        private static Lazy<List<QuantumSubshell>> SubshellsInstance = new Lazy<List<QuantumSubshell>>(() => QuantumSubshellsProvider.Create());
        
        public static List<QuantumSubshell> Subshells = QuantumSubshellsProvider.SubshellsInstance.Value;

        public static List<QuantumSubshell> Create()
        {
            List<QuantumSubshell> quantumSubshells = new List<QuantumSubshell>();

            for (int i = 0; i < Constant.MaxPeriodNumber; i++)
            {
                for (int j = 0; j < i + 1; j++)
                {
                    quantumSubshells.Add(new QuantumSubshell()
                    {
                        ElectronNumber = 2 + j * 4,
                        Period = i + 1,
                        QuantumSubshellFromKind = (QuantumSubshellFromKind)j
                    });
                }
            }

            Sort(quantumSubshells);
            Trim(quantumSubshells);

            return quantumSubshells;
        }

        public static int GetNumberOfAtomsInPeriod(int period)
        {
            List<QuantumSubshell> subshellsS = QuantumSubshellsProvider.Subshells
                .Where(subshell => subshell.QuantumSubshellFromKind == QuantumSubshellFromKind.S).ToList();

            int firstPeriodIndex = QuantumSubshellsProvider.Subshells.IndexOf(subshellsS[period - 1]);
            int subshellsCount = (period < subshellsS.Count)
                ? QuantumSubshellsProvider.Subshells.IndexOf(subshellsS[period]) - firstPeriodIndex
                : QuantumSubshellsProvider.Subshells.Count - firstPeriodIndex;

            return QuantumSubshellsProvider.Subshells.GetRange(firstPeriodIndex, subshellsCount)
                .Sum(subshell => subshell.ElectronNumber);
        }

        public static int GetNumberOfAtomsUpTo(int period)
        {
            if (period <= 0 || period > Constant.MaxPeriodNumber)
                throw new ArgumentException("Period value has to be in range (0; 7].");

            int numberOfElements = 0;

            for (int i = 0; i < period; i++)
                numberOfElements += QuantumSubshellsProvider.GetNumberOfAtomsInPeriod(i + 1);

            return numberOfElements;
        }

        private static void Sort(List<QuantumSubshell> subshells) =>
        subshells.Sort((right, left) =>
        {
            int aufbauRightNumber = right.Period + (int)right.QuantumSubshellFromKind;
            int aufbauLeftNumber = left.Period + (int)left.QuantumSubshellFromKind;

            if (aufbauRightNumber < aufbauLeftNumber)
                return -1;

            if (aufbauRightNumber == aufbauLeftNumber)
                if (right.Period < left.Period)
                    return -1;

            return 1;
        });

        /// <summary>
        /// Removes the non-existing quantum subshells.
        /// </summary>
        /// <param name="subshells">The collection of quantum subshells.</param>
        private static void Trim(List<QuantumSubshell> subshells) 
        {
            var firstNonExistingSubshellIndex = 1 + subshells.FindIndex(subshell => subshell.Period == Constant.MaxPeriodNumber && 
                subshell.QuantumSubshellFromKind == QuantumSubshellFromKind.P);

            subshells.RemoveRange(firstNonExistingSubshellIndex,
                subshells.Count - firstNonExistingSubshellIndex);
        }
    }
}