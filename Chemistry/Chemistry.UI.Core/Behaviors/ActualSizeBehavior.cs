﻿using Chemistry.UI.Core.AwaitableUI;
using Windows.UI.Xaml;

namespace Chemistry.UI.Core.Behaviors
{
    public static class ActualSizeBehavior
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty ActualSizeProperty = DependencyProperty.RegisterAttached(
            "ActualSize",
            typeof(bool),
            typeof(ActualSizeBehavior),
            new PropertyMetadata(false, OnActualSizeChanged));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty ActualWidthProperty = DependencyProperty.RegisterAttached(
            "ActualWidth",
            typeof(double),
            typeof(ActualSizeBehavior),
            new PropertyMetadata(default(double)));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty ActualHeightProperty = DependencyProperty.RegisterAttached(
            "ActualHeight",
            typeof(double),
            typeof(ActualSizeBehavior),
            new PropertyMetadata(default(double)));

        private static async void OnActualSizeChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            if (dependencyObject is FrameworkElement element)
            {
                if ((bool)e.NewValue)
                {
                    element.SizeChanged += OnElementSizeChanged;

                    await element.WaitForLayoutUpdateAsync();
                    SetActualWidth(element, element.ActualWidth);
                    SetActualHeight(element, element.ActualHeight);
                }
                else
                {
                    element.SizeChanged -= OnElementSizeChanged;
                }
            }
        }

        private static void OnElementSizeChanged(object sender, SizeChangedEventArgs e)
        {

            if (sender is FrameworkElement element)
            {
                SetActualWidth(element, element.ActualWidth);
                SetActualHeight(element, element.ActualHeight);
            }
        }

        public static bool GetActualSize(DependencyObject obj)
        {
            return (bool)obj.GetValue(ActualSizeProperty);
        }

        public static void SetActualSize(DependencyObject obj, bool value)
        {
            obj.SetValue(ActualSizeProperty, value);
        }

        public static void SetActualWidth(DependencyObject element, double value)
        {
            element.SetValue(ActualWidthProperty, value);
        }

        public static double GetActualWidth(DependencyObject element)
        {
            return (double)element.GetValue(ActualWidthProperty);
        }

        public static void SetActualHeight(DependencyObject element, double value)
        {
            element.SetValue(ActualHeightProperty, value);
        }

        public static double GetActualHeight(DependencyObject element)
        {
            return (double)element.GetValue(ActualHeightProperty);
        }
    }
}