﻿using System;
using System.Collections.Generic;
using System.Threading;
using Chemistry.Models.Enum;
using Chemistry.Models.Parameters;

namespace Chemistry.UI.Core.PopUp
{
    public class PopupContentProvider
    {
        private readonly Dictionary<PopupNavigationSource, Type> popupContentTypeMapping = new Dictionary<PopupNavigationSource, Type>();

        private static readonly Lazy<PopupContentProvider> instance = new Lazy<PopupContentProvider>(() => new PopupContentProvider());

        public event EventHandler<PopupContentChangedEventArgs> PopupContentChanged;

        public PopupNavigationSource CurrentPopupNavigationSource { get; private set; }

        private PopupContentProvider()
        {

        }

        public static PopupContentProvider Instance => instance.Value;

        public void AddMapping(PopupNavigationSource source, Type elementType)
        {
            if (!this.popupContentTypeMapping.ContainsKey(source))
            {
                this.popupContentTypeMapping.Add(source, elementType);
            }
        }

        public void ShowPopupContent(PopupNavigationSource source)
        {
            this.CurrentPopupNavigationSource = source;

            var handler = Volatile.Read(ref this.PopupContentChanged);

            if (handler != null)
            {
                var type = this.popupContentTypeMapping[source];
                handler.Invoke(this, new PopupContentChangedEventArgs(type, source));
            }
        }

        public void CloseAnyPopup()
        {
            this.ShowPopupContent(PopupNavigationSource.CloseAnyPopup);
        }
    }
}