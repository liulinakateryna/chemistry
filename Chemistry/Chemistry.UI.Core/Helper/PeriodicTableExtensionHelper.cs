﻿using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chemistry.UI.Core.Helper
{
    public sealed class PeriodicTableExtensionHelper
    {
        private const int MaxNumberOfElementsInRow = 19;

        private List<Atom> CreateFakeAtomRange(int numberOfAtoms)
            => Enumerable.Repeat(new Atom() { IsFake = true }, numberOfAtoms).ToList();   

        public void Extend(List<Atom> atoms)
        {
            this.RemoveEmptyAtoms(atoms);
            this.MoveFElementsToEnd(atoms);
            this.ExtendPeriod1(atoms);
            this.ExtendPeriod(atoms, 2);
            this.ExtendPeriod(atoms, 3);
            this.ExtendPeriod(atoms, 4);
            this.ExtendPeriod(atoms, 5);
            this.SetLinks(atoms);
            this.SetEmptyRow(atoms);
            this.ExtendAdditionalPeriods(atoms, 6);
            this.ExtendAdditionalPeriods(atoms, 7);
        }

        private void ExtendPeriod1(List<Atom> atoms)
        {
            int hydrogen = 1;
            int period = 1;

            int numberOfFakeAtoms = MaxNumberOfElementsInRow - QuantumSubshellsProvider.GetNumberOfAtomsInPeriod(period);

            atoms.InsertRange(hydrogen, this.CreateFakeAtomRange(numberOfFakeAtoms));
        }

        /// <summary>
        /// Extends any period except the first one and rows with F-elements.
        /// </summary>
        /// <param name="atoms">A collection of atoms to extend.</param>
        /// <param name="period">The period id (number) to extend.</param>
        private void ExtendPeriod(List<Atom> atoms, int period)
        {
            int periodStartIndex = atoms.FindIndex(atom => atom.Period == period);
            
            int metalicElementsCount = 2;
            int numberOfAtomsToSkip = periodStartIndex + metalicElementsCount;

            int notExtendedNumberOfAtomsInPeriod = atoms.Count(atom => atom.Period == period);
            int numberOfFakeAtoms = PeriodicTableExtensionHelper.MaxNumberOfElementsInRow - notExtendedNumberOfAtomsInPeriod;

            atoms.InsertRange(numberOfAtomsToSkip, this.CreateFakeAtomRange(numberOfFakeAtoms));
        }

        private void ExtendAdditionalPeriods(List<Atom> atoms, int period)
        {
            int numberOfAtomsToSkip = atoms.FindIndex(atom => atom.ElementType == QuantumSubshellFromKind.F && atom.Period == period);
            int numberOfAtoms = atoms.Count(atom => atom.ElementType == QuantumSubshellFromKind.F && atom.Period == period);

            int numberOfFakeAtoms = PeriodicTableExtensionHelper.MaxNumberOfElementsInRow - numberOfAtoms;
            atoms.InsertRange(numberOfAtomsToSkip, this.CreateFakeAtomRange(numberOfFakeAtoms - 1));

            atoms.InsertRange(numberOfAtomsToSkip + numberOfAtoms + numberOfFakeAtoms - 1, this.CreateFakeAtomRange(1));
        }

        private void MoveFElementsToEnd(List<Atom> atoms)
        {
            List<Atom> elementsF = atoms.Where(atom => atom.ElementType == QuantumSubshellFromKind.F).ToList();

            atoms.RemoveAll(atom => atom.ElementType == QuantumSubshellFromKind.F);
            atoms.AddRange(elementsF);
        }

        private void RemoveEmptyAtoms(List<Atom> atoms)
            => atoms.RemoveAll(atom => atom.IsFake || atom.IsLink);

        private void SetEmptyRow(List<Atom> atoms)
        {
            int startIndex = atoms.FindIndex(atom => atom.ElementType == QuantumSubshellFromKind.F);
            atoms.InsertRange(startIndex, this.CreateFakeAtomRange(PeriodicTableExtensionHelper.MaxNumberOfElementsInRow));
        }

        private void SetLinks(List<Atom> atoms)
        {
            int firstLinkIndex = atoms.FindIndex(atom => atom.Period == 6 && atom.ElementType == QuantumSubshellFromKind.S) + 2;
            int secondLinkIndex = atoms.FindIndex(atom => atom.Period == 7 && atom.ElementType == QuantumSubshellFromKind.S) + 2;

            atoms.Insert(secondLinkIndex, new Atom()
            {
                IsLink = true,
                LinkText = "89-102",
                Period = 7
            });

            atoms.Insert(firstLinkIndex, new Atom()
            {
                IsLink = true,
                LinkText = "57-70",
                Period = 6
            });
        }
    }
}