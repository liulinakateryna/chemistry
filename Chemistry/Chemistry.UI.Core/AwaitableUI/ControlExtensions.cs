﻿using System;
using System.Linq;
using System.Threading.Tasks;
using global::Windows.UI.Xaml;
using global::Windows.UI.Xaml.Controls;
using global::Windows.UI.Xaml.Media.Animation;

namespace Chemistry.UI.Core.AwaitableUI
{
    public static class ControlExtensions
    {
        public static async Task GoToVisualStateAsync(this Control control, FrameworkElement visualStatesHost, string stateGroupName, string stateName)
        {
            var tcs = new TaskCompletionSource<Storyboard>();
            var storyboard = GetStoryboardForVisualState(visualStatesHost, stateGroupName, stateName);

            if (storyboard != null)
            {
                void eh(object s, object e)
                {
                    storyboard.Completed -= eh;
                    tcs.SetResult(storyboard);
                }

                storyboard.Completed += eh;
            }

            VisualStateManager.GoToState(control, stateName, true);

            if (storyboard == null)
                return;

            await tcs.Task;
        }
        
        private static Storyboard GetStoryboardForVisualState(FrameworkElement visualStatesHost, string stateGroupName, string stateName)
        {
            Storyboard storyboard = null;

            var stateGroups = VisualStateManager.GetVisualStateGroups(visualStatesHost);
            VisualStateGroup stateGroup = null;

            if (!string.IsNullOrEmpty(stateGroupName))
                stateGroup = stateGroups.FirstOrDefault(g => g.Name == stateGroupName);

            VisualState state = null;

            if (stateGroup != null)
                state = stateGroup.States.FirstOrDefault(s => s.Name == stateName);

            if (state == null)
                foreach (var group in stateGroups)
                {
                    state = group.States.FirstOrDefault(s => s.Name == stateName);

                    if (state != null)
                        break;
                }

            if (state != null)
                storyboard = state.Storyboard;

            return storyboard;
        }
    }
}