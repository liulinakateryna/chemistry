﻿using System;
using System.Threading.Tasks;
using global::Windows.UI.Xaml;

namespace Chemistry.UI.Core.AwaitableUI
{
    public static class EventAsync
    {
        public static Task<object> FromEvent<T>(Action<EventHandler<T>> addEventHandler, Action<EventHandler<T>> removeEventHandler, Action beginAction = null)
        {
            return new EventHandlerTaskSource<T>(addEventHandler, removeEventHandler, beginAction).Task;
        }

        public static Task<RoutedEventArgs> FromRoutedEvent(Action<RoutedEventHandler> addEventHandler, Action<RoutedEventHandler> removeEventHandler, Action beginAction = null)
        {
            return new RoutedEventHandlerTaskSource(addEventHandler, removeEventHandler, beginAction).Task;
        }

        private sealed class EventHandlerTaskSource<TEventArgs>
        {
            private readonly Action<EventHandler<TEventArgs>> removeEventHandler;
            private readonly TaskCompletionSource<object> taskCompletionSource;

            public EventHandlerTaskSource(Action<EventHandler<TEventArgs>> addEventHandler, Action<EventHandler<TEventArgs>> removeEventHandler, Action beginAction = null)
            {
                if (addEventHandler == null)
                {
                    throw new ArgumentNullException(nameof(addEventHandler));
                }

                this.taskCompletionSource = new TaskCompletionSource<object>();
                this.removeEventHandler = removeEventHandler ?? throw new ArgumentNullException(nameof(removeEventHandler));
                addEventHandler.Invoke(this.EventCompleted);

                beginAction?.Invoke();
            }

            private void EventCompleted(object sender, TEventArgs args)
            {
                this.removeEventHandler.Invoke(this.EventCompleted);
                this.taskCompletionSource.SetResult(args);
            }

            public Task<object> Task => this.taskCompletionSource.Task;
        }

        private sealed class RoutedEventHandlerTaskSource
        {
            private readonly Action<RoutedEventHandler> removeEventHandler;
            private readonly TaskCompletionSource<RoutedEventArgs> taskCompletionSource;

            public RoutedEventHandlerTaskSource(Action<RoutedEventHandler> addEventHandler, Action<RoutedEventHandler> removeEventHandler, Action beginAction = null)
            {
                if (addEventHandler == null)
                {
                    throw new ArgumentNullException(nameof(addEventHandler));
                }

                this.taskCompletionSource = new TaskCompletionSource<RoutedEventArgs>();
                this.removeEventHandler = removeEventHandler ?? throw new ArgumentNullException(nameof(removeEventHandler));
                addEventHandler.Invoke(this.EventCompleted);

                beginAction?.Invoke();
            }

            private void EventCompleted(object sender, RoutedEventArgs args)
            {
                this.removeEventHandler.Invoke(this.EventCompleted);
                this.taskCompletionSource.SetResult(args);
            }

            public Task<RoutedEventArgs> Task => this.taskCompletionSource.Task;
        }
    }
}