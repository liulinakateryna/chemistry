﻿using System;
using Windows.UI.Xaml;

namespace Chemistry.Models.Proxy
{
    public class Proxy
    {
        public object Value { get; set; }
    }

    public class Proxy<T> : DependencyObject
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "immutable")]
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(T),
            typeof(Proxy<T>),
            new PropertyMetadata(default(T)));

        public T Value
        {
            get { return (T)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
    }

    public class BooleanProxy : Proxy<Boolean>
    {
    }

    public class VisibilityProxy : Proxy<Visibility>
    {
    }

    public class DoubleProxy : Proxy<Double>
    {
    }

    public class StringProxy : Proxy<String>
    {
    }
}