﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Chemistry.Models.Commands
{
   
    public interface IAsyncCommand<in T> : ICanExecuteAware
    {
        bool CanExecute(object obj);

        ICommand Command { get; }

        Task ExecuteAsync(T obj);
    }
}
