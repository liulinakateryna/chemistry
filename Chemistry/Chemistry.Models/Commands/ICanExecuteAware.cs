﻿namespace Chemistry.Models.Commands
{
    public interface ICanExecuteAware
    {
        void RaiseCanExecuteChanged();
    }
}