﻿using Chemistry.Models.Enum;
using System;

namespace Chemistry.Models.Parameters
{
    public class PopupContentChangedEventArgs : EventArgs
    {
        public PopupContentChangedEventArgs(Type type, PopupNavigationSource popupNavigationSource)
        {
            this.Type = type;
            this.PopupNavigationSource = popupNavigationSource;
        }

        public PopupContentChangedEventArgs(Type type, PopupNavigationSource popupNavigationSource, object parameter)
        {
            this.Type = type;
            this.PopupNavigationSource = popupNavigationSource;
            this.Parameter = parameter;
        }

        public PopupNavigationSource PopupNavigationSource { get; private set; }

        public Type Type { get; private set; }

        public object Parameter { get; private set; }

    }
}