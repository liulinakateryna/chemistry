﻿namespace Chemistry.Models
{
    public class Constant
    {
        public const string ConnectionString =
            @"Data Source=localhost;Initial Catalog=chemistry;Integrated Security=SSPI";

        public const int MaxPeriodNumber = 7;

        public const int CanvasWidth = 540;
        public const int CanvasHeight = 448;
        public const int Length = 32;
    }
}