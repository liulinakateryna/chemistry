﻿namespace Chemistry.Models.Enum
{
    public enum GeometryType
    {
        Atom,
        Link
    }
}