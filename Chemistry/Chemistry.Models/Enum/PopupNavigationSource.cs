﻿namespace Chemistry.Models.Enum
{
    public enum PopupNavigationSource
    {
        AtomPopup,
        Chemicals,
        CloseAnyPopup,
        Menu,
        PeriodicTable,
        Settings
    }
}