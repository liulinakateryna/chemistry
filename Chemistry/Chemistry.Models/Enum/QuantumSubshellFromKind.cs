﻿namespace Chemistry.Models.Enum
{
    public enum QuantumSubshellFromKind
    {
        S,
        P,
        D,
        F,
        G,
        H,
        I
    }
}