﻿namespace Chemistry.Models.Enum
{
    public enum SubstanceType
    {
        Simple,
        Oxide,
        Acid,
        Alkali,
        Salt
    }
}