﻿using Chemistry.Models.Enum;
using System.Drawing;

namespace Chemistry.Models.Models
{
    public sealed class GeometryUnit : ObservableObject
    {
        private Atom atom;
        private GeometryType geometryType;
        private Point from;
        private int linkMultiplicity;
        private Point to;

        public Atom Atom
        {
            get => this.atom;
            set
            {
                if (this.atom != value)
                {
                    this.atom = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public GeometryType GeometryType
        {
            get => this.geometryType;
            set
            {
                if (this.geometryType != value)
                {
                    this.geometryType = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public Point From
        {
            get => this.from;
            set
            {
                if (this.from != value)
                {
                    this.from = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int LinkMultiplicity
        {
            get => this.linkMultiplicity;
            set
            {
                if (this.linkMultiplicity != value)
                {
                    this.linkMultiplicity = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public Point To
        {
            get => this.to;
            set
            {
                if (this.to != value)
                {
                    this.to = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}