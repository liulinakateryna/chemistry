﻿using Chemistry.Models.Enum;

namespace Chemistry.Models.Models
{
    public sealed class QuantumSubshell
    {
        public int ElectronNumber
        {
            get;
            set;
        }

        public int Period
        {
            get;
            set;
        }

        public QuantumSubshellFromKind QuantumSubshellFromKind
        {
            get;
            set;
        }
    }
}