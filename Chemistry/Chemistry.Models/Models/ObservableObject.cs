﻿using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace Chemistry.Models.Models
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            var handler = this.PropertyChanged;

            if (handler != null)
            {
                handler?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
}
