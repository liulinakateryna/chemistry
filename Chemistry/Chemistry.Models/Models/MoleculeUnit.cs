﻿namespace Chemistry.Models.Models
{
    public sealed class MoleculeUnit : ObservableObject
    {
        private Atom atom;
        private int count;
        private int valance;

        public Atom Atom
        {
            get => this.atom;
            set
            {
                if (this.atom != value)
                {
                    this.atom = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int Count
        {
            get => this.count;
            set
            {
                if (this.count != value)
                {
                    this.count = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int Valance
        {
            get => this.valance;
            set
            {
                if (this.valance != value)
                {
                    this.valance = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}