﻿using Chemistry.Models.Enum;

namespace Chemistry.Models.Models
{
    public class Atom : ObservableObject
    {
        private string abbreviation;
        private int atomicNumber;
        private string atomicNumberHeader;
        private double ar;
        private int group;
        private double? electroneutrolity;
        private QuantumSubshellFromKind elementType;
        private int id;
        private bool isFake;
        private bool isLink;
        private bool isMainGroup;
        private string linkText;
        private int textId;
        private int period;

        public string Abbreviation
        {
            get => this.abbreviation;
            set
            {
                if (this.abbreviation != value)
                {
                    this.abbreviation = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int AtomicNumber
        {
            get => this.atomicNumber;
            set
            {
                if (this.atomicNumber != value)
                {
                    this.atomicNumber = value;
                    this.atomicNumberHeader = value.ToString();
                    this.OnPropertyChanged();
                }
            }
        }

        public string AtomicNumberHeader
        {
            get => this.atomicNumberHeader;
            set
            {
                if (this.atomicNumberHeader != value)
                {
                    this.atomicNumberHeader = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public double Ar
        {
            get => this.ar;
            set
            {
                if (this.ar != value)
                {
                    this.ar = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int Group
        {
            get => this.group;
            set
            {
                if (this.group != value)
                {
                    this.group = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public double? Electroneutrolity
        {
            get => this.electroneutrolity;
            set
            {
                if (this.electroneutrolity != value)
                {
                    this.electroneutrolity = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public QuantumSubshellFromKind ElementType
        {
            get => this.elementType;
            set
            {
                if (this.elementType != value)
                {
                    this.elementType = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int Id
        {
            get => this.id;
            set
            {
                if (this.id != value)
                {
                    this.id = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public bool IsFake
        {
            get => this.isFake;
            set
            {
                if (this.isFake != value)
                {
                    this.isFake = value;
                    this.OnPropertyChanged();
                    this.OnPropertyChanged(nameof(this.IsSelectable));
                }
            }
        }

        public bool IsLink
        {
            get => this.isLink;
            set
            {
                if (this.isLink != value)
                {
                    this.isLink = value;
                    this.OnPropertyChanged();
                    this.OnPropertyChanged(nameof(this.IsSelectable));
                }
            }
        }

        public bool IsMainGroup
        {
            get => this.isMainGroup;
            set
            {
                if (this.isMainGroup != value)
                {
                    this.isMainGroup = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public bool IsSelectable
            => !(this.IsLink || this.IsFake);

        public string LinkText
        {
            get => this.IsLink ? this.linkText : string.Empty;
            set
            {
                if (this.linkText != value)
                {
                    this.linkText = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int Period
        {
            get => this.period;
            set
            {
                if (this.period != value)
                {
                    this.period = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int TextId
        {
            get => this.textId;
            set
            {
                if (this.textId != value)
                {
                    this.textId = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}