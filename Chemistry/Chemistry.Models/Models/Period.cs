﻿using Chemistry.Models.Enum;

namespace Chemistry.Models.Models
{
    public sealed class Period : ObservableObject
    {
        private int electronsD;
        private int electronsF;
        private int electronsP;
        private int electronsS;
        private int period;

        public int ElectronsD
        {
            get => this.electronsD;
            set
            {
                if (this.electronsD != value)
                {
                    this.electronsD = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int ElectronsF
        {
            get => this.electronsF;
            set
            {
                if (this.electronsF != value)
                {
                    this.electronsF = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int ElectronsP
        {
            get => this.electronsP;
            set
            {
                if (this.electronsP != value)
                {
                    this.electronsP = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int ElectronsS
        {
            get => this.electronsS;
            set
            {
                if (this.electronsS != value)
                {
                    this.electronsS = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public int PeriodValue
        {
            get => this.period;
            set
            {
                if (this.period != value)
                {
                    this.period = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public void SetNumberOfElectrons(QuantumSubshellFromKind quantumSubshell, int numberOfElectrons)
        {
            switch (quantumSubshell)
            {
                case QuantumSubshellFromKind.S:
                    this.ElectronsS = numberOfElectrons;
                    return;

                case QuantumSubshellFromKind.P:
                    this.ElectronsP = numberOfElectrons;
                    return;

                case QuantumSubshellFromKind.D:
                    this.ElectronsD = numberOfElectrons;
                    return;

                case QuantumSubshellFromKind.F:
                    this.ElectronsF = numberOfElectrons;
                    return;
            }
        }
    }
}