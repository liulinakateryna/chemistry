﻿using Chemistry.Models.Enum;
using System.Collections.ObjectModel;
using System.Linq;

namespace Chemistry.Models.Models
{
    public sealed class Molecule : ObservableObject
    {
        private ObservableCollection<MoleculeUnit> moleculeUnits;
        private string nomenclature;

        public Molecule()
        {
            this.MoleculeUnits = new ObservableCollection<MoleculeUnit>();
        }

        public ObservableCollection<MoleculeUnit> MoleculeUnits
        {
            get => this.moleculeUnits;
            set
            {
                if (this.moleculeUnits != value)
                {
                    this.moleculeUnits = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string Nomenclature
        {
            get => this.nomenclature;
            set
            {
                if (this.nomenclature != value)
                {
                    this.nomenclature = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}