﻿using System.Collections.ObjectModel;

namespace Chemistry.Models.Models
{
    public sealed class MoleculeComponent : ObservableObject
    {
        private Atom atom;
        private int selectedValance;
        private ObservableCollection<int> valances;

        public delegate void PropertyChangedDelegate();
        public event PropertyChangedDelegate ModelChanged;

        public Atom Atom
        {
            get => this.atom;
            set
            {
                if (this.atom != value)
                {
                    this.atom = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public MoleculeComponent()
        {
            this.Valances = new ObservableCollection<int>();
            this.Valances.CollectionChanged += (sender, e) => this.OnPropertyChanged(nameof(this.Valances));
        }

        public int SelectedValance
        {
            get => this.selectedValance;
            set
            {
                if (this.selectedValance != value)
                {
                    this.selectedValance = value;
                    this.OnPropertyChanged();
                    this.OnModelChanged();
                }
            }
        }

        public ObservableCollection<int> Valances
        {
            get => this.valances;
            set
            {
                if (this.valances != value)
                {
                    this.valances = value;
                    this.OnPropertyChanged();
                }
            }
        }

        private void OnModelChanged()
        {
            if (this.ModelChanged != null)
                this.ModelChanged?.Invoke();
        }
    }
}