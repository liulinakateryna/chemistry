﻿using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using Chemistry.UI.Core.PopUp;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Windows.Input;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class MenuViewModel : ObservableObject
    {
        private Stack<PopupNavigationSource> navigation { get; }

        public bool CanNavigateBack
            => this.navigation.Count > 1;

        public MenuViewModel()
        {
            this.navigation = new Stack<PopupNavigationSource>();
            this.navigation.Push(PopupNavigationSource.Menu);

            this.NavigateCommand = new RelayCommand<PopupNavigationSource>(this.NavigateExecute);
            this.NavigateBackCommand = new RelayCommand(this.NavigateBackExecute);
        }

        public ICommand NavigateBackCommand
        {
            get;
            set;
        }

        public ICommand NavigateCommand
        {
            get;
            set;
        }

        private void NavigateBackExecute()
        {
            if (this.navigation.Count <= 1)
            {
                PopupContentProvider.Instance.ShowPopupContent(PopupNavigationSource.Menu);
                return;
            }

            this.navigation.Pop();
            PopupContentProvider.Instance.ShowPopupContent(this.navigation.Peek());

            this.OnPropertyChanged(nameof(this.CanNavigateBack));
        }

        private void NavigateExecute(PopupNavigationSource source)
        {
            navigation.Push(source);
            PopupContentProvider.Instance.ShowPopupContent(source);

            this.OnPropertyChanged(nameof(this.CanNavigateBack));
        }
    }
}