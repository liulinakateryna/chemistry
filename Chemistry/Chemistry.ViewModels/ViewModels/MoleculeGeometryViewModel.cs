﻿using Chemistry.Core.Geometry;
using Chemistry.Models.Models;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class MoleculeGeometryViewModel : ObservableObject
    {
        private GeometryHelper geometryHelper;
        private ObservableCollection<GeometryUnit> geometryUnits;

        public delegate void Action();
        public event Action OnClearing;
        public event Action OnDrawing;

        public ObservableCollection<GeometryUnit> GeometryUnits
        {
            get => this.geometryUnits;
            set
            {
                if (this.geometryUnits != value)
                {
                    this.geometryUnits = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public ICommand ClearCanvasCommand
        {
            get;
            set;
        }

        public ICommand DradMoleculeCommand
        {
            get;
            set;
        }

        public MoleculeGeometryViewModel()
        {
            this.ClearCanvasCommand = new RelayCommand(this.ClearCanvasExecute);
            this.DradMoleculeCommand = new RelayCommand(this.DradMoleculeExecute);

            this.GeometryUnits = new ObservableCollection<GeometryUnit>();
            this.geometryHelper = new GeometryHelper();
        }

        private void ClearCanvasExecute()
        => this.OnClearing?.Invoke();

        private void DradMoleculeExecute()
        {
            var viewModelLocator = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;

            if (viewModelLocator == null)
                return;

            var substanceType = viewModelLocator.AtomPropertiesViewModel.SelectedSubstanceType;
            var molecule = viewModelLocator.AtomPropertiesViewModel.SelectedMolecule;

            this.GeometryUnits = this.geometryHelper.BuildGeometryMolecule(molecule, substanceType);
            this.OnDrawing?.Invoke();
        }
    }
}