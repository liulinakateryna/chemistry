﻿using Chemistry.Core.Providers;
using models = Chemistry.Models.Models;
using Chemistry.Resources;
using GalaSoft.MvvmLight.Command;
using System.Globalization;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class SettingsViewModel : models.ObservableObject
    {
        private string selectedLanguage;

        public ICommand ChangeLanguage
        {
            get;
            set;
        }

        public string SelectedLanguage
        {
            get => this.selectedLanguage;
            set
            {
                if (this.selectedLanguage != value)
                {
                    this.selectedLanguage = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public SettingsViewModel()
        {
            this.ChangeLanguage = new RelayCommand<string>(this.ChangeLanguageExecute);
            this.SelectedLanguage = this.GetDefaultLanguage();
        }

        private void ChangeLanguageExecute(string locale)
        {

            if (CultureProvider.SupportedLanguages.TryGetValue(locale, out CultureInfo culture))
            {
                this.SelectedLanguage = locale;
                PageResources.Culture = CultureProvider.SupportedLanguages[locale];

                var resources = Application.Current.Resources["Resources"] as LocalizedStrings;
                resources?.RefreshLanguageSettings();
            }
        }

        private string GetDefaultLanguage()
        {
            string defaultLanguage = "en-US";

            if (CultureProvider.SelectedCulture != null)
                defaultLanguage = CultureProvider.SelectedCulture?.Name;

            return defaultLanguage;
        }
    }
}