﻿using Chemistry.Core.Providers;
using Chemistry.Models.Models;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class ChemicalsViewModel : ObservableObject
    {
        private const string H = "H";
        private const string O = "O";

        public ObservableCollection<Atom> Atoms
        {
            get;
            set;
        }

        private AtomBehaviourProvider atomBehaviourProvider;

        private AtomPropertiesViewModel atomPropertiesViewModel;

        public ICommand SelectAtomCommand
        {
            get;
            set;
        }

        public ObservableCollection<Atom> SelectedAtoms
        {
            get;
            set;
        }

        public ICommand UnselectAtomCommand
        {
            get;
            set;
        }

        public ChemicalsViewModel()
        {
            var viewModelLocator = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;
            this.atomPropertiesViewModel = viewModelLocator?.AtomPropertiesViewModel;

            this.atomBehaviourProvider = new AtomBehaviourProvider();
            this.Atoms = this.InitializeAtoms();
            this.SelectAtomCommand = new RelayCommand<Atom>(this.SelectAtomExecute);
            this.UnselectAtomCommand = new RelayCommand<Atom>(this.UnselectAtomExecute);
            this.SelectedAtoms = new ObservableCollection<Atom>();
            this.SelectedAtoms.CollectionChanged += (sender, e) => this.OnPropertyChanged(nameof(this.SelectedAtoms));
        }

        private ObservableCollection<Atom> InitializeAtoms()
        {
            List<Atom> atoms = AtomsProvider.Atoms;

            var atomsCollection = new ObservableCollection<Atom>();
            atoms.ForEach(atom => atomsCollection.Add(atom));

            return atomsCollection;
        }

        private void RemoveInertElements()
        {
            Atom inertElement = this.SelectedAtoms.Where(atom => this.atomBehaviourProvider.IsInert(atom)).FirstOrDefault();
            
            if (inertElement != null)
                this.SelectedAtoms.Remove(inertElement);
        }

        private void SelectAtomExecute(Atom atom)
        {
            if (this.SelectedAtoms.Contains(atom))
            {
                this.SelectedAtoms.Remove(atom);
                this.SelectedAtoms.Add(atom);
                return;
            }

            this.TryAdd(atom);

            this.atomPropertiesViewModel.UpdateAtomProperties();
        }

        private void TryAdd(Atom atom)
        {
            this.RemoveInertElements();

            if (this.atomBehaviourProvider.IsMetal(atom))
                this.TryAddMetalElement(atom);
            else if (this.atomBehaviourProvider.IsNonMetal(atom))
                this.TryAddNonMetalElement(atom);
            else
            {
                this.SelectedAtoms.Clear();
                this.SelectedAtoms.Add(atom);
            }
        }

        private void TryAddMetalElement(Atom atom)
        {
            var metals = this.SelectedAtoms.Where(entity => this.atomBehaviourProvider.IsMetal(entity)).ToList();

            if (atom.Abbreviation == ChemicalsViewModel.H)
                this.SelectedAtoms.Add(atom);
            else
            {
                var notH = metals.Where(entity => entity.Abbreviation != ChemicalsViewModel.H).FirstOrDefault();

                if (notH != null)
                    this.SelectedAtoms.Remove(notH);

                this.SelectedAtoms.Add(atom);
            }
        }

        private void TryAddNonMetalElement(Atom atom)
        {
            var metals = this.SelectedAtoms.Where(entity => this.atomBehaviourProvider.IsNonMetal(entity)).ToList();

            if (atom.Abbreviation == ChemicalsViewModel.O)
                this.SelectedAtoms.Add(atom);
            else
            {
                var notO = metals.Where(entity => entity.Abbreviation != ChemicalsViewModel.O).FirstOrDefault();

                if (notO != null)
                    this.SelectedAtoms.Remove(notO);

                this.SelectedAtoms.Add(atom);
            }
        }

        private void UnselectAtomExecute(Atom atom)
        {
            this.SelectedAtoms.Remove(atom);
            this.atomPropertiesViewModel.UpdateAtomProperties();
        }
    }
}