﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<AtomPropertiesViewModel>();
            SimpleIoc.Default.Register<ChemicalsViewModel>();
            SimpleIoc.Default.Register<MenuViewModel>();
            SimpleIoc.Default.Register<MoleculeGeometryViewModel>();
            SimpleIoc.Default.Register<PeriodicTableViewModel>();
            SimpleIoc.Default.Register<SettingsViewModel>();
        }

        public AtomPropertiesViewModel AtomPropertiesViewModel => SimpleIoc.Default.GetInstance<AtomPropertiesViewModel>();

        public ChemicalsViewModel ChemicalsViewModel => SimpleIoc.Default.GetInstance<ChemicalsViewModel>();

        public MenuViewModel MenuViewModel => SimpleIoc.Default.GetInstance<MenuViewModel>();

        public MoleculeGeometryViewModel MoleculeGeometryViewModel => SimpleIoc.Default.GetInstance<MoleculeGeometryViewModel>();

        public PeriodicTableViewModel PeriodicTableViewModel => SimpleIoc.Default.GetInstance<PeriodicTableViewModel>();
        
        public SettingsViewModel SettingsViewModel => SimpleIoc.Default.GetInstance<SettingsViewModel>();
    }
}