﻿using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using Chemistry.UI.Core.Helper;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class PeriodicTableViewModel : ObservableObject
    {
        private Atom selectedAtom;

        public ObservableCollection<Atom> Atoms { get; private set; }

        private ObservableCollection<Atom> InitializeAtoms()
        {
            List<Atom> atoms = new List<Atom>();
            atoms.AddRange(AtomsProvider.Atoms);

            var periodTableExtender = new PeriodicTableExtensionHelper();
            periodTableExtender.Extend(atoms);

            var atomsCollection = new ObservableCollection<Atom>();
            atoms.ForEach(atom => atomsCollection.Add(atom));

            return atomsCollection;
        }

        private void OpenAtomModel(Atom atom)
        {
            if (atom == null)
                return;

            if (atom.IsFake || atom.IsLink)
                return;

            this.SelectedAtom = atom;

            ServiceLocator.Current.GetInstance<MenuViewModel>().NavigateCommand.Execute(PopupNavigationSource.AtomPopup);
        }

        public ICommand OpenAtomModelCommand
        {
            get;
            set;
        }

        public PeriodicTableViewModel()
        {
            this.Atoms = this.InitializeAtoms();
            this.OpenAtomModelCommand = new RelayCommand<Atom>(this.OpenAtomModel);
        }

        public Atom SelectedAtom
        {
            get => this.selectedAtom;
            set
            {
                if (this.selectedAtom != value)
                {
                    this.selectedAtom = value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}