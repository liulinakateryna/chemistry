﻿using Chemistry.Core.Builders;
using Chemistry.Core.Helpers;
using Chemistry.Core.Providers;
using Chemistry.Models.Enum;
using Chemistry.Models.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;

namespace Chemistry.ViewModels.ViewModels
{
    public sealed class AtomPropertiesViewModel : ObservableObject
    {
        private MoleculeBuild moleculeBuild;
        private SubstanceTypeHelper substanceTypeHelper;
        private ValancesProvider valancesProvider;
        private ValancesDistributionHelper valancesDistributionHelper;

        private ObservableCollection<Molecule> molecules;
        private Molecule selectedMolecule;

        private MoleculeComponent moleculeComponent1;
        private MoleculeComponent moleculeComponent2;
        private MoleculeComponent moleculeComponent3;
        private MoleculeComponent moleculeComponent4;

        private ObservableCollection<SubstanceType> substanceTypes;
        private SubstanceType selectedSubstanceType;

        public delegate void PropertyChangedDelegate(SubstanceType substanceType);
        public event PropertyChangedDelegate SelectionChanged;

        public MoleculeComponent MoleculeComponent1
        {
            get => this.moleculeComponent1;
            set
            {
                if (this.moleculeComponent1 != value)
                {
                    this.moleculeComponent1 = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public MoleculeComponent MoleculeComponent2
        {
            get => this.moleculeComponent2;
            set
            {
                if (this.moleculeComponent2 != value)
                {
                    this.moleculeComponent2 = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public MoleculeComponent MoleculeComponent3
        {
            get => this.moleculeComponent3;
            set
            {
                if (this.moleculeComponent3 != value)
                {
                    this.moleculeComponent3 = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public MoleculeComponent MoleculeComponent4
        {
            get => this.moleculeComponent4;
            set
            {
                if (this.moleculeComponent4 != value)
                {
                    this.moleculeComponent4 = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<Molecule> Molecules
        {
            get => this.molecules;
            set
            {
                if (this.molecules != value)
                {
                    this.molecules = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public Molecule SelectedMolecule
        {
            get => this.selectedMolecule;
            set
            {
                if (this.selectedMolecule != value)
                {
                    this.selectedMolecule = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<SubstanceType> SubstanceTypes
        {
            get => this.substanceTypes;
            set
            {
                if (this.substanceTypes != value)
                {
                    this.substanceTypes = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public SubstanceType SelectedSubstanceType
        {
            get => this.selectedSubstanceType;
            set
            {
                if (this.selectedSubstanceType != value)
                {
                    this.selectedSubstanceType = value;
                    this.OnPropertyChanged();
                    this.UpdateMolecules();

                    if (this.SelectionChanged != null)
                        this.SelectionChanged.Invoke(this.selectedSubstanceType);
                }
            }
        }

        public AtomPropertiesViewModel()
        {
            this.moleculeBuild = new MoleculeBuild();
            this.Molecules = new ObservableCollection<Molecule>();
            this.SubstanceTypes = new ObservableCollection<SubstanceType>();
            this.valancesProvider = new ValancesProvider();
            this.valancesDistributionHelper = new ValancesDistributionHelper();
            this.substanceTypeHelper = new SubstanceTypeHelper();
            this.SubstanceTypes.CollectionChanged += (sender, e) => this.OnPropertyChanged(nameof(this.SubstanceTypes));
            this.Molecules.CollectionChanged += (sender, e) => this.OnPropertyChanged(nameof(this.Molecules));
        }

        private MoleculeComponent CreateMolecule(Atom atom)
            => (atom == null) ? null : new MoleculeComponent()
            {
                Atom = atom,
                Valances = this.valancesProvider.GetValances(atom)
            };

        private void UpdatePossibleSubstanceTypes()
        {
            this.SubstanceTypes = this.substanceTypeHelper
                .GetSubstanceTypes(new List<MoleculeComponent>
                {
                    this.MoleculeComponent1,
                    this.MoleculeComponent2,
                    this.MoleculeComponent3,
                    this.MoleculeComponent4
                });

            if (!this.SubstanceTypes.Contains(this.SelectedSubstanceType))
                this.SelectedSubstanceType = this.SubstanceTypes.FirstOrDefault();
        }

        private void SetPrefferedValents()
        {
            this.valancesDistributionHelper.SetPrefferedValences(new List<MoleculeComponent>
            {
                this.MoleculeComponent1,
                this.MoleculeComponent2,
                this.MoleculeComponent3,
                this.MoleculeComponent4
            });
        }

        public void UpdateAtomProperties()
        {
            var viewModelLocator = Application.Current.Resources["ViewModelLocator"] as ViewModelLocator;
            var chemicalsViewModel = viewModelLocator?.ChemicalsViewModel;

            var atoms = chemicalsViewModel.SelectedAtoms;

            for (int i = 1; i <= 4; i++)
            {
                MoleculeComponent moleculeComponent = null;
                
                if ( i - 1 < atoms.Count)
                    moleculeComponent = this.CreateMolecule(atoms[i - 1]);

                switch (i)
                {
                    case 1:
                        this.MoleculeComponent1 = moleculeComponent;
                        break;
                    case 2:
                        this.MoleculeComponent2 = moleculeComponent;
                        break;
                    case 3:
                        this.MoleculeComponent3 = moleculeComponent;
                        break;
                    case 4:
                        this.MoleculeComponent4 = moleculeComponent;
                        break;
                }
            }

            this.SetPrefferedValents();
            this.UpdatePossibleSubstanceTypes();
            this.UpdateMolecules();

            new List<MoleculeComponent>
            {
                this.MoleculeComponent1,
                this.MoleculeComponent2,
                this.MoleculeComponent3,
                this.MoleculeComponent4
            }.Where(component => component != null).ToList()
            .ForEach(component => component.ModelChanged += this.UpdateMolecules);
        }

        public void UpdateMolecules()
        {
            this.Molecules = this.moleculeBuild.Build(this.SelectedSubstanceType, new List<MoleculeComponent>()
            {
                this.MoleculeComponent1,
                this.MoleculeComponent2,
                this.MoleculeComponent3,
                this.MoleculeComponent4
            });

            this.SelectedMolecule = this.Molecules.FirstOrDefault();
        }
    }
}