﻿using Chemistry.DataAccess.DTOs;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Chemistry.DataAccess.Reader
{
    public sealed class AtomCsvReader : IDatabaseReader<int, AtomDto>
    {
        private const string DataSource = @"../../Data/elements.csv";

        public Dictionary<int, AtomDto> Read()
        {
            var atoms = new Dictionary<int, AtomDto>();

            string[] chemicalElementRecords = File.ReadAllLines(AtomCsvReader.DataSource);

            chemicalElementRecords.ToList().ForEach(record =>
            {

            });
            //string sql = $"SELECT * FROM {TableName.ChemicalElement}";
            //using (var connection = new SqlConnection(Constant.ConnectionString))
            //{
            //    connection.Open();

            //    if (connection.State == System.Data.ConnectionState.Open)
            //        using (SqlCommand command = connection.CreateCommand())
            //        {
            //            command.CommandText = sql;
            //            using (SqlDataReader reader = command.ExecuteReader())
            //            {
            //                while (reader.Read())
            //                {
            //                    atoms.Add(reader.GetInt32(0), new AtomDto()
            //                    {
            //                        KeyChemicalElement = reader.GetInt32(0),
            //                        ShortName = reader.GetString(1),
            //                        TextId = reader.GetInt32(2),
            //                        Weight = reader.GetDouble(3),
            //                        Electroneutrolity = reader.GetDouble(4)
            //                    });
            //                }
            //            }
            //        }
            //}

            return atoms;
        }
    }
}