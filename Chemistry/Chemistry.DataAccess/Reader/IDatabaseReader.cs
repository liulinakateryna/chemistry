﻿using System.Collections.Generic;

namespace Chemistry.DataAccess.Reader
{
    public interface IDatabaseReader<TKey, TValue>
    {
        Dictionary<TKey, TValue> Read();
    }
}