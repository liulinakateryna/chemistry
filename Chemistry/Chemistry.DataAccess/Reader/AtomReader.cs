﻿using Chemistry.DataAccess.DTOs;
using Chemistry.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Chemistry.DataAccess.Reader
{
    public sealed class AtomReader : IDatabaseReader<int, AtomDto>
    {
        public Dictionary<int, AtomDto> Read()
        {
            var atoms = new Dictionary<int, AtomDto>();

            string sql = $"SELECT * FROM {TableName.ChemicalElement}";
            using (var connection = new SqlConnection(Constant.ConnectionString)) 
            { 
                connection.Open();

                if (connection.State == System.Data.ConnectionState.Open)
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = sql;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                atoms.Add(reader.GetInt32(0), new AtomDto()
                                {
                                    KeyChemicalElement = reader.GetInt32(0),
                                    ShortName = reader.GetString(1).Trim(),
                                    TextId = reader.GetInt32(2),
                                    Weight = reader.GetDouble(3),
                                    Electroneutrolity = reader.IsDBNull(4) ? new double?() : reader.GetDouble(4)
                                });
                            }
                        }
                    }
            }

            return atoms;
        }
    }
}