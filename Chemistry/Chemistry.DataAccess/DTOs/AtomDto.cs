﻿namespace Chemistry.DataAccess.DTOs
{
    public sealed class AtomDto
    {
        public double? Electroneutrolity
        {
            get;
            set;
        }

        public int KeyChemicalElement
        {
            get;
            set;
        }

        public string ShortName
        {
            get;
            set;
        }

        public int TextId
        {
            get;
            set;
        }

        public double Weight
        {
            get;
            set;
        }
    }
}